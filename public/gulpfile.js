/* Require modules
-------------------------------------------------------------------- */

var gulp   = require('gulp'),
    stylus = require('gulp-stylus'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    prefix = require('gulp-autoprefixer');

/* Backend Styles task
-------------------------------------------------------------------- */

gulp.task('BackendStyles', function() {
    gulp.src('backend/stylus/main.styl')
        .pipe(stylus({
            use: ['nib']
        }))
        //.pipe(prefix("last 1 version", "> 1%", "ie 8", { cascade: true }))
        .on('error', notify.onError("Error: <%= error.message %>")) // Если есть ошибки, выводим и продолжаем
        .pipe(gulp.dest('backend/css/')) // записываем css
});

/* Backend AngularApp task
-------------------------------------------------------------------- */

gulp.task('BackendAngularApp', function() {
    gulp.src(['backend/js/app/**/*.js'])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest('backend/js'))
});

/* Backend AngularVendor task
-------------------------------------------------------------------- */

gulp.task('BackendAngularVendor', function() {
   
    gulp.src(['backend/js/vendor/angular/**/*.js'/*, '!backend/js/vendor/angular/angular-core.min.js'*/])
        .pipe(concat('angular-modules.min.js'))
        .pipe(uglify())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest('backend/js/vendor'));

});

/* Frontend styles task
-------------------------------------------------------------------- */

gulp.task('FrontendStyles', function() {
   
    gulp.src(['css/*.css', '!css/main.min.css'])
        .pipe(concat('main.min.css'))
        .pipe(prefix("last 1 version", "> 1%", "ie 8", { cascade: true }))
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest('css/'));

});


/* Watch task
-------------------------------------------------------------------- */

gulp.task('watch', ['BackendAngularApp', 'BackendAngularVendor', 'BackendStyles'], function() {

    gulp.watch('backend/stylus/**/*.styl', ['BackendStyles']);

    gulp.watch('backend/js/app/**/*.js', ['BackendAngularApp']);

    gulp.watch('backend/js/vendor/angular/**/*.js', ['BackendAngularVendor']);

    //gulp.watch('css/*.css', ['FrontendStyles']);

});