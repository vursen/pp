'use strict';

app.classy.controller({
    name: 'BlogPostsEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'BlogPosts', 'BlogCategories', 'Layouts', 'CKPlugins', 'flash'],

    init: function() {

        if (this.$routeParams.postId) {
            this.$.post = this.BlogPosts.get({ id: this.$routeParams.postId });
        } else {
            this.$.post = new this.BlogPosts();
        }

        this.$.langs      = this.config.langs;
        this.$.layouts    = this.Layouts.query();
        this.$.categories = this.BlogCategories.query();

    },

    save: function() {

        var _this = this;
        
        this.$.post.$save(function() {

            _this.flash('Запись сохранена.');

        });

    },

    plugin: function(name) {

        //console.log(name, this.CKPlugins);
        return this.CKPlugins[name]();

    }

});