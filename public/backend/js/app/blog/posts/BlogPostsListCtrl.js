'use strict';

app.classy.controller({

    name: 'BlogPostsListCtrl',

    inject: ['$scope', '$routeParams', 'config', 'BlogPosts', 'BlogCategories', 'flash', 'ngTableParams', '$filter'],

    init: function() {

        this.BlogPosts.query(function (posts) {

            this.$.posts = posts;

            this.$.tableParams = new this.ngTableParams({
                page: 1,
                count: 5
            }, {
                counts: [],
                total: posts.length,
                getData: function($defer, params) {
                    $defer.resolve(posts.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

            this.$.tableParams.paginationTemplate = this.$filter('backendAssetsUrl')('views/partials/pagination.html');

        }.bind(this));

        this.$.categories = this.BlogCategories.query();
        
    },

    delete: function ($index, post) {

        var _this = this;
        
        if (!confirm('Вы действительно хотите удалить данную запись?')) {
            return false;
        }

        this.BlogPosts.delete({}, { 'id': post.id }, function(response) {

            _this.$.posts.splice($index, 1);
            _this.flash('Запись удалена.');

        });

    }, 

});