'use strict';

app.factory('BlogPosts', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/blog/posts/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);