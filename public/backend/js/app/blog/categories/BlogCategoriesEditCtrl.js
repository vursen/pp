'use strict';

app.classy.controller({
    name: 'BlogCategoriesEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'BlogCategories', 'Layouts', 'flash'],

    init: function() {

        if (this.$routeParams.categoryId) {
            this.$.category = this.BlogCategories.get({ id: this.$routeParams.categoryId });
        } else {
            this.$.category = new this.BlogCategories();
        }

        this.$.langs   = this.config.langs;
        this.$.layouts = this.Layouts.query();

    },

    save: function() {

        var _this = this;

        this.$.category.$save(function() {

            _this.flash('Категория сохранена.');

        });

    },

});