'use strict';

app.factory('BlogCategories', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/blog/categories/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);