'use strict';

app.classy.controller({

    name: 'BlogCategoriesListCtrl',

    inject: ['$scope', '$routeParams', 'config', 'BlogCategories', 'flash'],

    init: function() {

        this.$.categories = this.BlogCategories.query();

    },

    delete: function ($index, category) {

        var _this = this;
        
        if (!confirm('Вы действительно хотите удалить данную категорию?')) {
            return false;
        }

        this.BlogCategories.delete({}, {'id': category.id}, function(response) {

            _this.$.categories.splice($index, 1);
            _this.flash('Категория удалена.');

        });

    }, 

});