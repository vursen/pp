'use strict';

app.classy.controller({
    name: 'SettingsCtrl',

    inject: ['$scope', '$routeParams', 'config', 'Settings', 'flash'],

    init: function() {

        this.$.settings = this.Settings.query();

    },

    save: function() {

        var _this = this;

        this.$.settings.$save(function() {

            _this.flash('Настройки успешно обновлены.');

        });

    }


});