'use strict';

app.factory('Settings', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/settings',
        {},
        {
            'query': { isArray: false }
        }
    );
}]);