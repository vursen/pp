'use strict';

app.classy.controller({

    name: 'StoreProductsListCtrl',

    inject: ['$scope', '$routeParams', 'config', 'StoreProducts', 'StoreCategories', 'flash', 'ngTableParams', '$filter'],

    init: function() {

        this.StoreProducts.query(function (products) {

            this.$.products = products;

            this.$.tableParams = new this.ngTableParams({
                page: 1,
                count: 5
            }, {
                counts: [],
                total: products.length,
                getData: function($defer, params) {
                    $defer.resolve(products.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

            this.$.tableParams.paginationTemplate = this.$filter('backendAssetsUrl')('views/partials/pagination.html');

        }.bind(this));

        this.$.categories = this.StoreCategories.query();
        
    },

    delete: function ($index, product) {

        var _this = this;
        
        if (!confirm('Вы действительно хотите удалить данный продукт?')) {
            return false;
        }

        this.StoreProducts.delete({}, { 'id': product.id }, function(response) {

            _this.$.products.splice($index, 1);
            _this.flash('Продукт удален.');

        });

    }, 

});