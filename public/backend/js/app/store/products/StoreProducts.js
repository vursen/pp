'use strict';

app.factory('StoreProducts', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/store/products/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);