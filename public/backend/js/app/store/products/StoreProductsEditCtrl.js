'use strict';

app.classy.controller({

    name: 'StoreProductsEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'StoreProducts', 'StoreCategories', 'flash'],

    init: function() {

        if (this.$routeParams.productId) {
            this.$.product = this.StoreProducts.get({ id: this.$routeParams.productId });
        } else {
            this.$.product = new this.StoreProducts({
                images: []
            });
        }

        this.$.langs      = this.config.langs;
        this.$.categories = this.StoreCategories.query();
        
    },

    save: function() {

        var _this = this;
        
        this.$.product.$save(function() {

            _this.flash('Продукт сохранен.');

        });

    },

    addImage: function(newImageUrl) {

        if (this.$.product.images.indexOf(newImageUrl) == -1) {

            this.$.product.images.push(newImageUrl);

        } else {

            this.flash('error', 'Изображение уже добавлено.');

        }

        this.$.$apply();

    },

    removeImage: function($index) {

        this.$.product.images.splice($index, 1);

    }

});