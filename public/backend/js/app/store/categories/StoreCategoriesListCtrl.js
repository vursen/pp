'use strict';

app.classy.controller({

    name: 'StoreCategoriesListCtrl',

    inject: ['$scope', '$routeParams', 'config', 'StoreCategories', 'flash'],

    init: function() {

        this.$.categories = this.StoreCategories.query();

    },

    delete: function ($index, category) {

        var _this = this;
        
        if (!confirm('Вы действительно хотите удалить данную категорию?')) {
            return false;
        }

        this.StoreCategories.delete({}, {'id': category.id}, function(response) {

            _this.$.categories.splice($index, 1);
            _this.flash('Категория удалена.');

        });

    }, 

});