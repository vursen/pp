'use strict';

app.factory('StoreCategories', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/store/categories/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);