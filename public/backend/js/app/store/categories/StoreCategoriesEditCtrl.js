'use strict';

app.classy.controller({
    name: 'StoreCategoriesEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'StoreCategories', 'flash'],

    init: function() {

        if (this.$routeParams.categoryId) {
            this.$.category = this.StoreCategories.get({ id: this.$routeParams.categoryId });
        } else {
            this.$.category = new this.StoreCategories();
        }

        this.$.langs = this.config.langs;

    },

    save: function() {

        var _this = this;
        
        this.$.category.$save(function() {

            _this.flash('Категория сохранена.');

        });

    },

    plugin: function(name) {

        //console.log(name, this.CKPlugins);
        return this.CKPlugins[name]();

    }

});