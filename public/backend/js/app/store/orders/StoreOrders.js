'use strict';

app.factory('StoreOrders', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/store/orders/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);