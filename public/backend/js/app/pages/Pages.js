'use strict';

app.factory('Pages', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/pages/:id',
        { id: '@id' },
        {
            'query': { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);