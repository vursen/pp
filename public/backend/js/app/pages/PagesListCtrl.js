'use strict';

app.classy.controller({

    name: 'PagesListCtrl',

    inject: ['$scope', 'config', 'Pages', 'flash'],

    init: function() {

        this.$.pages = this.Pages.query();
        
    },

    delete: function ($index, page) {

        if (!confirm('Вы действительно хотите удалить данную страницу?')) {
            return false;
        }

        var _this = this;

        this.Pages.delete({}, {'id': page.id}, function(response) {

            _this.$.pages.splice($index, 1);
            _this.flash('Страница удалена.');

        });

    }, 

});