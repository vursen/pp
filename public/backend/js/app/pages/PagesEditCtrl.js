'use strict';

app.classy.controller({
    name: 'PagesEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'Pages', 'Layouts', 'flash'],

    init: function() {

        if (this.$routeParams.pageId) {
            this.$.page = this.Pages.get({ id: this.$routeParams.pageId });
        } else {
            this.$.page = new this.Pages();
        }
        this.$.langs   = this.config.langs;
        this.$.layouts = this.Layouts.query();

    },

    save: function() {

        var _this = this;

        this.$.page.$save(function() {

            _this.flash('Страница сохранена.');

        });

    },

});