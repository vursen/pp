'use strict';

app.service('CKPlugins', ['config', 'FileManager', function (appConfig, FileManager) {

    $.fn.inlineStyle = function (prop) {
        return this.prop("style")[$.camelCase(prop)];
    };

    var _self = this;

    this.config = {
        default_color: '#fff',
        colors: ['000000', 'ffffff', '4a4a4a', 'b32525','e78787','265f8c', '5c9da9','1c6224','4ba047','e79918','fce97c','C0C0C0','dadada', 'transparent'],
        galleries: {
            classic: {
                title: 'Фото-классик',
                gallery_height: true,
                gallery_width: true,
                image_width: true,
                image_title: true,
                image_desc: false,
            },
            mini: {
                title: 'Фото-мини',
                gallery_height: true,
                gallery_width: true,
                image_width: false,
                image_title: true,
                image_desc: false,
            },
            sweetthumbs: {
                title: 'Фото-sweet',
                gallery_height: true,
                gallery_width: true,
                image_width: true,
                image_title: true,
                image_desc: false,
            },
            phototext: {
                title: 'Фото+Текст',
                gallery_height: false,
                gallery_width: true,
                image_width: true,
                image_border: true,
                image_title: true,
                image_desc: true,
                image_crop: true
            }
        }
    }

    this.ckeditor = function () {
        return CKEDITOR.instances[Object.keys(CKEDITOR.instances)[0]];
    }

    this.helpers = {
        rgb2hex: function(rgb) {
            if (!rgb) { return false; }
            if (rgb.match(/transparent/)) { return 'transparent'; }
            if (rgb.match(/^#([0-9A-Za-z]{6})$/)) { return rgb; }

            rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);

            function hex(x) { return ("0" + parseInt(x).toString(16)).slice(-2); }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        },

        isValue: function(val) {
            return (val && val != 'initial' && val != '0px');
        },

        isNumber: function(val) {
            return $.isNumeric(val);
        },

        urlToRelative: function(url) {
            return url.replace(/^(?:\/\/|[^\/]+)*/, "");
        },

        unit: function(val, type) {
            if (_self.helpers.isNumber(val)) {
                return val.toString() + type.toString()
            } else {
                return val;
            }
        },

        getSelectionElement: function(options) {
            var options         = options || null;
            var founded_element = false;

            if(_self.ckeditor().getSelection() != null) {
                var element = _self.ckeditor().getSelection().getStartElement();

                if (options) {
                    while(!founded_element) {
                        if (element.is('body')) break;
                        
                        if (options.tagName && element.is(options.tagName)) {
                            founded_element = true;
                            break;
                        }
                        if (options.className && element.hasAttribute('class') == true && element.hasClass(options.className) == true) {    
                            founded_element = true;
                            break;
                        }
                        if (options.data && element.data(options.data)) {   
                            founded_element = true;
                            break;  
                        }
                        element = element.getParent();
                    }
                } else {
                    founded_element = true;
                }
            }
            if(!founded_element){
                return false; 
            }
            return element;
        },

        insertElement: function(element, p) {
            _self.ckeditor().insertElement(element);

            if (p) { _self.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>')); }

            _self.ckeditor().getSelection().selectElement(element);
        },

        modal: function(options){
            
            var fancyboxOptions = {
                closeBtn: false, 
                minWidth: '400px',
                maxWidth: '700px',
                autoSize: true,
                padding: 5,
                openSpeed: 0,
                closeSpeed: 0
            };

            var $template = $(
                '<div class="b-modal">' + 
                    '<div class="b-modal__bottom">' +
                        '<a class="b-modal__ok btn--md btn--success">OK</a>&nbsp;<a class="b-modal__cancel btn--md btn--default">Отмена</a>' +
                    '</div>' +
                '</div>'
            );

            $template.prepend(options.template);

            var $fields = [];
            $template.find('[data-field]').each(function() {
                $fields[$(this).data('field')] = $(this);
            });

            $template.find('.b-modal__cancel').click(function() {
                $.fancybox.close();
            });

            $template.find('.b-modal__ok').click(function() {
                if (options.save && options.save($template, $fields) !== false) {
                    $.fancybox.close();
                }

                _self.ckeditor().fire('change');
            });

            $template.find('[data-filemanager]').each(function() {
                $(this).attr({
                    placeholder: 'Нажмите сюда, чтобы выбрать файл.',
                    readonly: 'readonly'
                }).click(function() {
                    FileManager.open({
                        field: $(this)
                    });
                })
            });

            $template.find('[data-colorpicker]').colorPicker({ 
                pickerDefault: _self.config.default_color, 
                colors: _self.config.colors, 
                transparency: true
            });

            $template.find('[data-dragsort]').each(function() {
                var $this = $(this);
                $this.dragsort({ 
                    dragSelector: $this.data('dragsort'), 
                    dragBetween: false
                });
            });

            if (options.init) {
                options.init($template, $fields);
            }

            $.fancybox($template, fancyboxOptions);
            
        },
    }
    
    /* Изображениями
    -------------------------------------------------------------------------- */

    this.Image = function() { 

        var is_new_element = false;
        var element        = this.helpers.getSelectionElement({tagName : 'img'});

        if (!element) {
            is_new_element = true;
            element        = CKEDITOR.dom.element.createFromHtml('<img />');   
        }

        var $element = $(element.$);
        
        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Изображение</h2>' +
                '<label class="form-label">*URL: <input data-field="url" data-filemanager class="form-input"/></label>' +
                '<label class="form-label">Alt: <textarea data-field="alt" class="form-textarea"></textarea></label>' +
                '<label class="form-label">Ширина: <input data-field="width" class="form-input width--10"/></label>' +
            '</div>';

        /* Рамка
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Рамка</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Толщина <input data-field="border_width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Цвет <input data-field="border_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Расположение
        --------------------------------------------------------------------- */

        template +=
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Расположение</h3>' + 
                '<label class="form-label">' +
                    '<select class="form-select" data-field="position">' +
                        '<option value="none">По умолчанию</option>' +
                        '<option value="left">Слева</option>' +
                        '<option value="right">Справа</option>' +
                        '<option value="center">По центру</option>' +
                    '</select>' + 
                '</label>' + 
            '</div>';

        /* Отступы
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Внешние отступы</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Сверху<input data-field="margin_top" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Справа<input data-field="margin_right" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Снизу<input data-field="margin_bottom" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Слева<input data-field="margin_left" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Эффекты
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Эффекты</h3>' + 
                '<label class="form-label form-group">' +
                    '<select class="form-select" data-field="effects">' +
                        '<option value="">Без эффектов</option>' +
                        '<option value="expando">Expando</option>' +
                        '<option value="fancybox">Fancybox</option>' +
                        '<option value="fancybox_auto">Fancybox Auto</option>' +
                        '<option value="fancybox_iframe">Fancybox Iframe</option>' +
                    '</select>' + 
                '</label>' +
            '</div>';

        /*

        w_content += '<h3>Эффекты</h3>';

        w_content += '<img alt=" " src="' + appConfig.backendAssetsUrl + '/img/exsando.jpg"/><input type="radio" name="effect" id="image-effect-expando"/><b>Expando</b> <br />увеличение изображения при подводе мышки до исходного размера (рекомедуемая ширина импортируемого изображения для данного эффекта - от 300 до 500px)<div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';
        
        w_content += '<img alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy.jpg"/><input type="radio" name="effect" id="image-effect-fancybox" /><b>Fancybox</b><br />при клике мышкой увеличенное изображение откроется на затемненнои фоне. Стрелки перехода на другое изображение отсутствуют<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';    

        w_content += '<img alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy_auto.jpg"/><input type="radio" name="effect" id="image-effect-fancybox-auto" /><b>Fancybox Auto</b><br />при клике мышкой увеличенное изображение откроется на затемненнои фоне. Присутствуют стрелки перехода на другое изображение<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';
        
        w_content += '<img alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy_iframe.jpg"/><input type="radio" name="effect" id="image-effect-fancybox-frame"/><b>Fancybox Iframe</b><br />при клике мышкой откроется стороннее изображение (JPG, SWF, HTML или TXT) на затемненном фоне. <br>' + 
                     'Ссылка: <input name="img_fancy_link" style="width:300px;" value=""><br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';

        w_content += '<img alt=" " src="' + appConfig.backendAssetsUrl + '/img/adaptiv_img.jpg"/><input type="checkbox" name="responsive-image"/><b>Responsive Image</b><br />размер картинки по ширине адаптируется размеру ширины поля страницы. Данная функция особо актуальна для "резиновых" по ширине сайтов.<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';

        w_content += '<input type="radio" name="effect" id="image-effect-none" style=""/> <b>Эффекты не используются</b> <br />';
        
        */
        
        this.helpers.modal({
            template: template,

            init: function($modal, $fields) {

                $fields.url.val($element.attr('src') || '');
                $fields.alt.val($element.attr('alt') || '');
                $fields.width.val(element.getStyle('width') || '');

                /* Рамка
                ---------------------------------------------------------------- */

                if (_self.helpers.isValue(element.getStyle('border-width'))) {
                    $fields.border_width.val(element.getStyle('border-width'));
                    $fields.border_color.val(_self.helpers.rgb2hex(element.getStyle('border-color'))).change();
                }

                /* Расположение
                ---------------------------------------------------------------- */

                if (['right', 'left'].indexOf(element.getStyle('float')) !== -1) {
                    $fields.position.val(element.getStyle('float'));
                } else if (element.getStyle('margin-left') == 'auto' && element.getStyle('margin-right') == 'auto') {
                    $fields.position.val('center');
                }

                $fields.position.change(function(e) {
                    if ($(this).val() == 'center') {
                        $fields.margin_left.attr('disabled', 'disabled');
                        $fields.margin_right.attr('disabled', 'disabled');
                    } else {
                        $fields.margin_left.removeAttr('disabled');
                        $fields.margin_right.removeAttr('disabled');
                    }
                }).change();

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    $fields['margin_' + item].val(element.getStyle('margin-' + item) || '');
                });

               /* var is_checked = false;
                $modal.find('input[name="effect"]').each(function(){
                    if($element.hasClass($(this).attr('id')) == true){
                        
                        if($(this).attr('id') == 'image-effect-fancybox-frame' && !isEmpty(element.data('fancy-link'))){
                            $modal.find('input[name="img_fancy_link"]').val(element.data('fancy-link'));
                        }
                        
                        $(this).attr('checked', 'checked');
                        is_checked = true;
                        
                    }
                });

                if ($element.hasClass(responsive_class) == true) {
                    $modal.find('input[name="responsive-image"]').attr('checked', 'checked');
                }

                if(!is_checked){
                    $modal.find('#image-effect-none').attr('checked', 'checked');
                }*/
            },

            save: function($modal, $fields) {

                /* Валидация
                ---------------------------------------------------------------- */

                if (!_self.helpers.isValue($fields.url.val())) { alert('Вы должны выбрать изображение!'); return false; }
                
                element.setAttributes({
                    'data-cke-saved-src': $fields.url.val(),
                    'src': $fields.url.val(),
                    'alt': $fields.alt.val().replace(/[\r\n]/g, ' ')
                });

                element.setStyle('display', 'block', true);

                if (_self.helpers.isValue($fields.width.val())) {
                    element.setStyle('width', _self.helpers.unit($fields.width.val(), 'px'), true);
                } else {
                    element.removeStyle('width');
                }

                /*element.setAttribute('src', $fields.url.val());
                element.setAttribute('alt', $fields.alt.val().replace(/[\r\n]/g, ' '));*/

                /* Рамка
                ---------------------------------------------------------------- */
                            
                if (_self.helpers.isValue($fields.border_width.val())) {
                    element.setStyles({
                        'border-width': _self.helpers.unit($fields.border_width.val(), 'px'),
                        'border-style': 'solid',
                        'border-color': $fields.border_color.val()
                    }, true);
                } else {
                    element.removeStyle('border-width'); 
                    element.removeStyle('border-color'); 
                    element.removeStyle('border-style'); 
                }

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    if (_self.helpers.isValue($fields['margin_' + item].val())) {
                        element.setStyle('margin-' + item, _self.helpers.unit($fields['margin_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle('margin-' + item);
                    }
                });

                /* Расположение
                ---------------------------------------------------------------- */

                // Обязательно после отступов, чтобы переписать margin-left, margin-right

                if (['right', 'left'].indexOf($fields.position.val()) !== -1) {
                    element.setStyle('float', $fields.position.val(), true);
                } else if ($fields.position.val() == 'center') {
                    element.removeStyle('float');
                    element.setStyles({
                        'margin-left': 'auto',
                        'margin-right': 'auto'
                    }, true);
                } else {
                    element.removeStyle('float');
                }
                
                /*$modal.find('[name="effect"]').each(function(){
                    if($(this).is(':checked')){

                        if($(this).attr('id') != 'image-effect-none'){
                            element.addClass($(this).attr('id'));
                        }

                        if($(this).attr('id') == 'image-effect-fancybox-frame'){
                            element.data('fancy-link', $modal.find('input[name="img_fancy_link"]').val());
                        } else {
                            element.data('fancy-link', ''); 
                        }

                    } else {
                        element.removeClass($(this).attr('id'));
                    }
                });

                if ($modal.find('[name="responsive-image"]').is(':checked')) {

                    element.addClass(responsive_class);

                } else {

                    element.removeClass(responsive_class);

                }*/
                
                
                if (is_new_element) { _self.helpers.insertElement(element, true); }
            }
        }); 

    },
    
    /* Iframe
    -------------------------------------------------------------------------- */

    this.Iframe = function() {

        var element  = CKEDITOR.dom.element.createFromHtml('<iframe></iframe>'); 

        var template = 
            '<div class="b-modal__section">' +
                '<h2 class="b-modal__section__heading heading--md">Iframe</h2>' + 
                '<label class="form-label">*Ссылка: <input data-field="url" class="form-input"></label>' +
                '<label class="form-label">Ширина: <input data-field="width" class="form-input"></label>' +
                '<label class="form-label">Высота: <input data-field="height" class="form-input"></label>' +
            '</div>';
        
        this.helpers.modal({
            template: template,

            save: function($modal, $fields) {

                /* Валидация
                ---------------------------------------------------------------- */

                if (!$fields.url.val()) { alert('Вы должны указать ссылку.'); return false; }

                element.setAttributes({
                    'src': $fields.url.val(),
                    'width': (_self.helpers.isValue($fields.width.val()) ? _self.helpers.unit($fields.width.val(), 'px') : '100%'),
                    'frameborder': 0,
                    'scrolling': 'no'
                });

                if (_self.helpers.isValue($fields.height.val())) {
                    element.setAttribute('height', _self.helpers.unit($fields.height.val(), 'px'), true);
                } else {
                    element.setAttribute('onload', 'frame_load(this);');    
                }

                _self.helpers.insertElement(element, true);

            }
        });

    },
    
    /* Div
    -------------------------------------------------------------------------- */

    this.Div = function() {

        var is_new_element = false;
        var element        = this.helpers.getSelectionElement({className : 'ck-div'});

        if (!element) {
            is_new_element = true;
            element        = CKEDITOR.dom.element.createFromHtml('<div class="ck-div"><p class="ck-div__content">Контент</p></div>'); 
        }

        var $element = $(element.$);
        
        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Окно</h2>' +
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Ширина <input data-field="width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Высота <input data-field="height" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Фон <input data-field="background_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Рамка
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Рамка</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Толщина <input data-field="border_width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Цвет <input data-field="border_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label"><input type="checkbox" data-field="border_top" class="form-checkbox" /> Сверху</label>' + 
                    '<label class="form-group__item width--10 form-label"><input type="checkbox" data-field="border_right" class="form-checkbox" /> Справа</label>' + 
                    '<label class="form-group__item width--10 form-label"><input type="checkbox" data-field="border_bottom" class="form-checkbox" /> Снизу</label>' + 
                    '<label class="form-group__item width--10 form-label"><input type="checkbox" data-field="border_left" class="form-checkbox" /> Слева</label>' + 
                '</div>' + 
            '</div>';

        /* Расположение
        --------------------------------------------------------------------- */

        template +=
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Расположение</h3>' + 
                '<label class="form-label">' +
                    '<select class="form-select" data-field="position">' + 
                        '<option value="none">По умолчанию</option>' +
                        '<option value="left">Слева</option>' +
                        '<option value="right">Справа</option>' +
                        '<option value="center">По центру</option>' +
                    '</select>' + 
                '</label>' + 
            '</div>';

        /* Внешние отступы
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Внешние отступы</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Сверху<input data-field="margin_top" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Справа<input data-field="margin_right" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Снизу<input data-field="margin_bottom" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Слева<input data-field="margin_left" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Внутренние отступы
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Внутренние отступы</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Сверху<input data-field="padding_top" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Справа<input data-field="padding_right" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Снизу<input data-field="padding_bottom" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Слева<input data-field="padding_left" class="form-input" /></label>' + 
                '</div>' +
            '</div>';
        
        this.helpers.modal({
            template: template,

            init: function($modal, $fields) {

                $fields.width.val(element.getStyle('width') || '');
                $fields.height.val(element.getStyle('height') || '');

                if (_self.helpers.isValue(element.getStyle('background-color'))) {
                    $fields.background_color.val(_self.helpers.rgb2hex(element.getStyle('background-color'))).change();
                }

                /* Рамка
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    var border_width = element.getStyle('border-' + item + '-width'),
                        border_color = element.getStyle('border-' + item + '-color');

                    if (!$fields.border_width.val() && _self.helpers.isValue(border_width)) {
                        $fields.border_width.val($element.inlineStyle('border-' + item + '-width'));
                    }

                    if ($fields.border_color.val() == _self.config.default_color && _self.helpers.isValue(border_color)) {
                        $fields.border_color.val(_self.helpers.rgb2hex(border_color)).change();
                    }
                        
                    if (_self.helpers.isValue(border_width)) {
                        $fields['border_' + item].attr('checked', 'checked');
                    }
                });

                /* Расположение
                ---------------------------------------------------------------- */

                if (['right', 'left'].indexOf(element.getStyle('float')) !== -1) {
                    $fields.position.val(element.getStyle('float'));
                } else if (element.getStyle('margin-left') == 'auto' && element.getStyle('margin-right') == 'auto') {
                    $fields.position.val('center');
                } else {
                    $fields.position.val('none');
                }

                $fields.position.change(function(e) {
                    if ($(this).val() == 'center') {
                        $fields.margin_left.attr('disabled', 'disabled');
                        $fields.margin_right.attr('disabled', 'disabled');
                    } else {
                        $fields.margin_left.removeAttr('disabled');
                        $fields.margin_right.removeAttr('disabled');
                    }
                }).change();

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    $fields['margin_' + item].val(element.getStyle('margin-' + item) || '');
                    $fields['padding_' + item].val(element.getStyle('padding-' + item) || '');
                });

            },

            save: function($modal, $fields) {

                /* Размеры
                ---------------------------------------------------------------- */

                if (_self.helpers.isValue($fields.width.val())) {
                    element.setStyle('width', _self.helpers.unit($fields.width.val(), 'px'), true);
                } else {
                    element.removeStyle('width');
                }

                if (_self.helpers.isValue($fields.height.val())) {
                    element.setStyle('height', _self.helpers.unit($fields.height.val(), 'px'), true);
                } else {
                    element.removeStyle('height');
                }

                /* Фон
                ---------------------------------------------------------------- */

                element.setStyle('background-color', $fields.background_color.val(), true);

                /* Рамка
                ---------------------------------------------------------------- */
                            
                if (_self.helpers.isValue($fields.border_width.val())) {
                    element.setStyles({
                        'border-width': _self.helpers.unit($fields.border_width.val(), 'px'),
                        'border-style': 'solid',
                        'border-color': $fields.border_color.val()
                    }, true);

                    $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                        if (!$fields['border_' + item].is(':checked')) {
                            element.setStyle('border-' + item, 'none');
                        }
                    });
                } else {
                    element.removeStyle('border-width'); 
                    element.removeStyle('border-color'); 
                    element.removeStyle('border-style');
                }

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    if (_self.helpers.isValue($fields['margin_' + item].val())) {
                        element.setStyle('margin-' + item, _self.helpers.unit($fields['margin_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle('margin-' + item);
                    }

                    if (_self.helpers.isValue($fields['padding_' + item].val())) {
                        element.setStyle('padding-' + item, _self.helpers.unit($fields['padding_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle('padding-' + item);
                    }
                });

                /* Расположение
                ---------------------------------------------------------------- */

                // Обязательно после отступов, чтобы переписать margin-left, margin-right

                if (['right', 'left'].indexOf($fields.position.val()) !== -1) {
                    element.setStyle('float', $fields.position.val());
                } else if ($fields.position.val() == 'center') {
                    element.removeStyle('float');
                    element.setStyles({
                        'margin-left': 'auto',
                        'margin-right': 'auto'
                    }, true);
                } else {
                    element.removeStyle('float');
                }

                if (is_new_element) { _self.helpers.insertElement(element, true); }

            }
        });

    },
            
    /* Audio
    -------------------------------------------------------------------------- */
    
    this.Audio = function() {
    
        var is_new_element = false;
        var element        = this.helpers.getSelectionElement({className : 'ck-audio'});

        if (!element) {
            is_new_element = true;
            element        = CKEDITOR.dom.element.createFromHtml('<div class="ck-audio"></div>');   
        }

        var template = 
            '<div class="b-modal__section">' +
                '<h2 class="b-modal__section__heading heading--md">Аудио</h2>' + 
                '<label class="form-label">*Путь к файлу: <input data-field="url" class="form-input" data-filemanager></label>' +
            '</div>';
        
        this.helpers.modal({
            template: template,

            init: function($modal, $fields) {

                $fields.url.val(element.data('url') || '');

            },

            save: function($modal, $fields) {

                /* Валидация
                ---------------------------------------------------------------- */

                if (!_self.helpers.isValue($fields.url.val())) { alert('Вы должны указать путь к файлу.'); return false; }

                element.data('url', $fields.url.val());
                element.setText($fields.url.val());

                if (is_new_element) { _self.helpers.insertElement(element, true); }

            }
        });

    },
    
    /* Кнопки
    -------------------------------------------------------------------------- */
    
    this.Button = function(){
    
        var is_new_element = false;
        var element = this.helpers.getSelectionElement({className : 'ck-btn'});

        if (!element) {
            is_new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<a href="#" class="ck-btn">Кнопка</a>');  
        }

        var $element = $(element.$);
        
        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Кнопка</h2>' +
                '<label class="form-label">*Ссылка: <input data-field="url" class="form-input"/></label>' +
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Ширина <input data-field="width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Высота <input data-field="height" class="form-input" /></label>' + 
                    '<label class="form-group__item width--20 form-label">Цвет текста <input data-field="text_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Изображение по умолчанию
        --------------------------------------------------------------------- */

        template +=
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">*Состояние по умолчанию</h3>' + 
                '<label class="form-label">*Изображение: <input data-field="state_default_img" data-filemanager class="form-input"/></label>' +
                '<label class="form-label">Прозрачность: <input data-field="state_default_opacity" class="form-input width--10"/></label>' +
            '</div>';

        /* Изображение при наведении
        --------------------------------------------------------------------- */

        template +=
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Состояние при наведении</h3>' + 
                '<label class="form-label">Изображение: <input data-field="state_hover_img" data-filemanager class="form-input"/></label>' +
                '<label class="form-label">Прозрачность: <input data-field="state_hover_opacity" class="form-input width--10"/></label>' +
            '</div>';

        /* Рамка
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Рамка</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Толщина <input data-field="border_width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Цвет <input data-field="border_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Расположение
        --------------------------------------------------------------------- */

        template +=
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Расположение</h3>' + 
                '<label class="form-label">' +
                    '<select class="form-select" data-field="position">' + 
                        '<option value="none">По умолчанию</option>' +
                        '<option value="left">Слева</option>' +
                        '<option value="right">Справа</option>' +
                        '<option value="center">По центру</option>' +
                    '</select>' + 
                '</label>' + 
            '</div>';

        /* Внешние отступы
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Внешние отступы</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Сверху<input data-field="margin_top" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Справа<input data-field="margin_right" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Снизу<input data-field="margin_bottom" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Слева<input data-field="margin_left" class="form-input" /></label>' + 
                '</div>' +
            '</div>';

        /* Внутренние отступы
        --------------------------------------------------------------------- */

        template += 
            '<div class="b-modal__section">' + 
                '<h3 class="b-modal__section__heading heading--sm">Внутренние отступы</h3>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Сверху<input data-field="padding_top" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Справа<input data-field="padding_right" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Снизу<input data-field="padding_bottom" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Слева<input data-field="padding_left" class="form-input" /></label>' + 
                '</div>' +
            '</div>';
       
        this.helpers.modal({
            template: template,

            init: function($modal, $fields) {

                $fields.url.val($element.attr('data-cke-saved-href') || '');
                $fields.width.val(element.getStyle('width') || '');
                $fields.height.val(element.getStyle('height') || '');
                $fields.text_color.val(_self.helpers.rgb2hex(element.getStyle('color'))).change();

                /* Состояние по умолчанию
                ---------------------------------------------------------------- */

                $fields.state_default_img.val(element.data('state-default-img') || '');
                $fields.state_default_opacity.val(element.data('state-default-opacity') || '');

                /* Состояние при наведении
                ---------------------------------------------------------------- */

                $fields.state_hover_img.val(element.data('state-hover-img') || '');
                $fields.state_hover_opacity.val(element.data('state-hover-opacity') || '');

                /* Расположение
                ---------------------------------------------------------------- */

                if (['right', 'left'].indexOf(element.getStyle('float')) !== -1) {
                    $fields.position.val(element.getStyle('float'));
                } else if (element.getStyle('margin-left') == 'auto' && element.getStyle('margin-right') == 'auto') {
                    $fields.position.val('center');
                } else {
                    $fields.position.val('none');
                }

                $fields.position.change(function(e) {
                    if ($(this).val() == 'center') {
                        $fields.margin_left.attr('disabled', 'disabled');
                        $fields.margin_right.attr('disabled', 'disabled');
                    } else {
                        $fields.margin_left.removeAttr('disabled');
                        $fields.margin_right.removeAttr('disabled');
                    }
                }).change();

                /* Рамка
                ---------------------------------------------------------------- */

                if (_self.helpers.isValue(element.getStyle('border-width'))) {
                    $fields.border_width.val(element.getStyle('border-width'));
                    $fields.border_color.val(_self.helpers.rgb2hex(element.getStyle('border-color'))).change();
                }

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    $fields['margin_' + item].val(element.getStyle('margin-' + item) || '');
                    $fields['padding_' + item].val(element.getStyle('padding-' + item) || '');
                });
            },

            save: function($modal, $fields) {

                /* Валидация
                ---------------------------------------------------------------- */

                if (!_self.helpers.isValue($fields.url.val())) { alert('Вы должны указать ссылку.'); return false; }

                /*if (!_self.helpers.isValue($fields.state_hover_img.val()) || !_self.helpers.isValue($fields.state_hover_opacity.val())) { 
                    alert('Вы должны заполнить все поля для состояния при наведении.'); return false; 
                }*/

                /*if (!_self.helpers.isValue($fields.state_default_img.val())) { 
                    alert('Вы должны указать фоновое изображение для состояния по умолчанию.'); return false; 
                }*/

                element.setStyles({
                    'background': 'url(' + $fields.state_default_img.val() + ') no-repeat',
                    'display': '',
                    'color': $fields.text_color.val()
                }, true);
                
                element.setAttributes({
                    'data-cke-saved-href': $fields.url.val(),
                    'href': $fields.url.val(),
                    'data-state-default-img': $fields.state_default_img.val(),
                    'data-state-default-opacity': $fields.state_default_opacity.val() || 100,
                    'data-state-hover-img': $fields.state_hover_img.val() || '',
                    'data-state-hover-opacity': $fields.state_hover_opacity.val() || 100
                });

                if (_self.helpers.isValue($fields.width.val())) {
                    element.setStyle('width', _self.helpers.unit($fields.width.val(), 'px'), true);
                } else {
                    element.removeStyle('width');
                }

                if (_self.helpers.isValue($fields.height.val())) {
                    element.setStyle('height', _self.helpers.unit($fields.height.val(), 'px'), true);
                } else {
                    element.removeStyle('height');
                }

                /* Отступы
                ---------------------------------------------------------------- */

                $.each(['top', 'right', 'bottom', 'left'], function(key, item) {
                    if (_self.helpers.isValue($fields['margin_' + item].val())) {
                        element.setStyle('margin-' + item, _self.helpers.unit($fields['margin_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle('margin-' + item);
                    }

                    if (_self.helpers.isValue($fields['padding_' + item].val())) {
                        element.setStyle('padding-' + item, _self.helpers.unit($fields['padding_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle('padding-' + item);
                    }
                });

                /* Расположение
                ---------------------------------------------------------------- */

                // Обязательно после отступов, чтобы переписать margin-left, margin-right

                if (['right', 'left'].indexOf($fields.position.val()) !== -1) {
                    element.setStyle('float', $fields.position.val());
                } else if ($fields.position.val() == 'center') {
                    element.removeStyle('float');
                    element.setStyles({
                        'margin-left': 'auto',
                        'margin-right': 'auto',
                        'display': 'block'
                    }, true);
                } else {
                    element.removeStyle('float');
                }

                /* Рамка
                ---------------------------------------------------------------- */
                            
                if (_self.helpers.isValue($fields.border_width.val())) {
                    element.setStyles({
                        'border-width': _self.helpers.unit($fields.border_width.val(), 'px'),
                        'border-style': 'solid',
                        'border-color': $fields.border_color.val()
                    }, true);
                } else {
                    element.removeStyle('border-width'); 
                    element.removeStyle('border-color'); 
                    element.removeStyle('border-style'); 
                }

                if (is_new_element) { _self.helpers.insertElement(element, true); }

            }
        });
        /*, function(){
        
            var $modal = $('.b-modal-window');
            
            //Путь
            
            if(!isEmpty(element.getAttribute('data-cke-saved-href'))){
                $modal.find('input[name="button_url"]').val(element.getAttribute('data-cke-saved-href'));
            }
            
            //Открыть в iframe
            
            if(element.data('fancy-iframe') == 'true'){
                $modal.find('input[name="button_fancy_iframe"]').attr('checked', 'checked');
            }
            
            //Ширина
            
            if(!isEmpty(element.getFirst().getStyle('width'))){
                $modal.find('input[name="button_width"]').val(element.getFirst().getStyle('width'));
            }
            
            //Высота
            
            if(!isEmpty(element.getFirst().getStyle('height'))){
                $modal.find('input[name="button_height"]').val(element.getFirst().getStyle('height'));
            }
            
            //Отступ
            
            if(element.getStyle('text-align') == 'center'){
                $modal.find('input[name="button_position"][value="center"]').attr('checked', 'checked');
            }else if(element.getFirst().getStyle('float') == 'left'){   
                $modal.find('input[name="button_position"][value="left"]').attr('checked', 'checked');
                $modal.find('input[name="button_position_left"]').val(element.getFirst().getStyle('margin-right')); 
            }else if(element.getFirst().getStyle('float') == 'right'){
                $modal.find('input[name="button_position"][value="right"]').attr('checked', 'checked');
                $modal.find('input[name="button_position_right"]').val(element.getFirst().getStyle('margin-left')); 
            } else {
                $modal.find('input[name="button_position"][value="none"]').attr('checked', 'checked');
            }
            
            //Путь к изображению 1
            
            if(!isEmpty(element.getFirst().data('img1'))){
                $modal.find('input[name="button_img1_path"]').val(element.getFirst().data('img1'));
            }
            
            //Прозрачность изображения 1
            
            if(!isEmpty(element.getFirst().data('img1-opacity'))){
                $modal.find('input[name="button_img1_opacity"]').val(element.getFirst().data('img1-opacity'));
            }
            
            //Путь к изображению 2
            
            if(!isEmpty(element.getFirst().data('img2'))){
                $modal.find('input[name="button_img2_path"]').val(element.getFirst().data('img2'));
            }
            
            //Прозрачность изображения 2
            
            if(!isEmpty(element.getFirst().data('img2-opacity'))){
                $modal.find('input[name="button_img2_opacity"]').val(element.getFirst().data('img2-opacity'));
            }
            
        }, function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Обработка поля путь к изображению 1

            var button_img1_path = $modal.find('input[name="button_img1_path"]').val();
            if(button_img1_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны выбрать путь для первого изображения!'); return false; }
            element.getFirst().data('img1', button_img1_path);
            element.getFirst().setAttribute('data-cke-saved-src', button_img1_path);
            element.getFirst().setAttribute('src', button_img1_path);
            
            //Обработка поля прозрачность изображения 1
            
            var button_img1_opacity = $modal.find('input[name="button_img1_opacity"]').val();
            if(isEmpty(button_img1_opacity)){ alert('Вы должны установить прозрачность для первого изображения!'); return false; }
            element.getFirst().data('img1-opacity', $modal.find('input[name="button_img1_opacity"]').val());
            
            //Обработка поля путь к изображению 2
            
            var button_img2_path = $modal.find('input[name="button_img2_path"]').val();
            if(button_img2_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны выбрать путь для второго изображения!'); return false; }
            element.getFirst().data('img2', $modal.find('input[name="button_img2_path"]').val());
            
            //Обработка поля прозрачность изображения 2
            
            var button_img2_opacity = $modal.find('input[name="button_img2_opacity"]').val();
            if(isEmpty(button_img2_opacity)){ alert('Вы должны установить прозрачность для второго изображения!'); return false; }
            element.getFirst().data('img2-opacity', $modal.find('input[name="button_img2_opacity"]').val());
            
            //Обработка поля ширина
                
            var button_width = $modal.find('input[name="button_width"]').val();
            if(!isEmpty(button_width)){
                element.getFirst().setStyle('width', (isNumeric(button_width)) ? button_width + 'px' : button_width, true);
            } else {
                element.getFirst().setStyle('width', 'auto', true); 
            }
            
            //Обработка поля высота
            
            var button_height = $modal.find('input[name="button_height"]').val();
            if(!isEmpty(button_height)){
                element.getFirst().setStyle('height', (isNumeric(button_height)) ? button_height + 'px' : button_height, true);
            } else {
                element.getFirst().setStyle('height', 'auto', true);
            }
            
            //Установка ссылки
            
            element.setAttribute('data-cke-saved-href', $modal.find('input[name="button_url"]').val());
            
            //Установка fancy-iframe
            
            element.data('fancy-iframe',($modal.find('input[name="button_fancy_iframe"]').is(':checked')) ? true : false);  
          
            //Обработка обтекания
            
            //element.setStyle('float', $modal.find('select[name="button_float"]').val());
            
            //Обработка отступа
              
            var button_position = $modal.find('input[name="button_position"]:checked').val();
            if(!isEmpty(button_position)){
                    
                element.removeStyle('text-align');
                element.getFirst().removeStyle('float');
                element.getFirst().removeStyle('margin');
                
                if(button_position == 'center'){
                    
                    element.getFirst().setStyle('float', 'none');
                    element.getFirst().setStyle('margin', '0px');   
                    element.setStyle('text-align', 'center', true);
                    element.setStyle('display', 'block', true);
                    
                }else if(button_position == 'right'){
                    
                    var button_position_right = $modal.find('input[name="button_position_right"]').val();
                    element.getFirst().setStyle('float', 'right');
                    element.getFirst().setStyle('margin-left', (isNumeric(button_position_right)) ? button_position_right + 'px' : button_position_right, true);
                    element.getFirst().setStyle('margin-right', '15px', true);
                    
                }else if(button_position == 'left'){
                    
                    var button_position_left = $modal.find('input[name="button_position_left"]').val();
                    element.getFirst().setStyle('float', 'left');
                    element.getFirst().setStyle('margin-right', (isNumeric(button_position_left)) ? button_position_left + 'px' : button_position_left, true);
                    element.getFirst().setStyle('margin-left', '15px', true);
                    
                }       
                
            }   

        });*/
    },
    
    /* Столбцы
    -------------------------------------------------------------------------- */

    this.Columns = function() {

        var element = CKEDITOR.dom.element.createFromHtml('<div class="ck-columns clearfix"></div>'); 

        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Столбцы</h2>' +
                '<label class="form-label">Кол-во столбцов <input data-field="count" class="form-input" /></label>' + 
            '</div>';
            
        this.helpers.modal({
            template: template,

            save: function($modal, $fields) {

                /* Валидация
                ---------------------------------------------------------------- */

                if (!_self.helpers.isValue($fields.count.val())) { alert('Вы должны указать кол-во столбцов.'); return false; }
            
                var width = _self.helpers.unit(Math.round(100 /  $fields.count.val()), '%');
                
                for (var i = 0; i < $fields.count.val(); i++) {
                    var col = CKEDITOR.dom.element.createFromHtml(
                        '<div class="ck-columns__item" style="width:' + width + ';">' +
                            '<div class="ck-columns__item__content">' + 
                                '<p>Содержимое</p><a class="ck-columns__item__btn">&nbsp;</a>' +
                            '</div>' +
                        '</div>'
                    );

                    col.appendTo(element);
                }

                _self.helpers.insertElement(element, true);

            }
        });
        
    },
    
    /* Спойлер
    -------------------------------------------------------------------------- */
    
    this.Spoiler = function() {

        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Спойлер</h2>' +
                '<label class="form-label">Заголовок <textarea data-field="title" class="form-textarea"></textarea></label>' + 
                '<label class="form-label">Содержимое <textarea data-field="content" class="form-textarea"></textarea></label>' + 
            '</div>';      
        
        this.helpers.modal({
            template: template,

            save: function($modal, $fields) {
            
                var element = CKEDITOR.dom.element.createFromHtml(
                    '<div class="ck-spoiler">' +
                        '<a href="#" class="ck-spoiler__title">' + $fields.title.val() +'</a>' + 
                        '<div class="ck-spoiler__content">'+ $fields.content.val() + '</div>' + 
                    '</div>'
                );
                
                _self.helpers.insertElement(element, true);

            }
        });
        
    },
    
    /* Вставка страницы
    -------------------------------------------------------------------------- */
    
    this.IncludePage = function() {
    
        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Вставка страницы</h2>' +
                '<label class="form-label">Название <input data-field="name" class="form-input" /></label>' + 
            '</div>';      
        
        this.helpers.modal({
            template: template,

            save: function($modal, $fields) {
                
                /* Валидация
                ---------------------------------------------------------------- */

                if (!_self.helpers.isValue($fields.name.val())) { alert('Вы должны указать название страницы.'); return false; }

                var element = CKEDITOR.dom.element.createFromHtml('<div class="ck-include-page">@page(' + $fields.name.val() + ')</div>');
                
                _self.helpers.insertElement(element, true);

            }
        });
            
    },
    
    /* Галереи
    -------------------------------------------------------------------------- */
    
    this.Gallery = function() {
        
        var is_new_element = false;
        var element        = this.helpers.getSelectionElement({data : 'ck-gallery'});

        if (!element) {
            is_new_element = true;
            element        = CKEDITOR.dom.element.createFromHtml('<div data-ck-gallery=""></div>'); 
        }

        var $element = $(element.$);

        var template =
            '<div class="b-modal__section b-gallery__settings">' + 
                '<h2 class="b-modal__section__heading heading--md">Галерея</h2>' +
                '<label class="form-label"><select data-field="type" class="form-select"></select></label>' + 
                '<div class="form-group">' +
                    '<label class="form-group__item form-label width--40">Ширина галереи <input data-field="gallery_width" class="form-input width--10" /></label>' + 
                    '<label class="form-group__item form-label width--40">Высота галереи<input data-field="gallery_height" class="form-input width--10" /></label>' + 
                '</div>' +
                '<label class="form-label">Ширина изображений <input data-field="image_width" class="form-input width--10" /></label>' + 
                '<label class="form-label"><input data-field="image_crop" type="checkbox" class="form-checkbox" /> Обрезать</label>' + 
            '</div>'

        template +=
            '<div class="b-modal__section b-gallery__borders">' + 
                '<h2 class="b-modal__section__heading heading--sm">Рамка для изображений</h2>' +
                '<div class="form-group">' +
                    '<label class="form-group__item width--10 form-label">Толщина <input data-field="image_border_width" class="form-input" /></label>' + 
                    '<label class="form-group__item width--10 form-label">Цвет <input data-field="image_border_color" data-colorpicker maxlength="5" class="form-input" /></label>' + 
                '</div>' +  
            '</div>';

        template +=
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--sm">Изображения <a class="link b-gallery__add-item">добавить</a></h2>' +
                //'<div class="b-gallery__items-wrapper">' +
                    '<ul class="b-gallery__items" data-dragsort="li"></ul>' +
                //'</div>' +
            '</div>'; 
        
        this.helpers.modal({
            template: template,

            init: function($modal, $fields) {

                var _this   = this;
                var gallery = _self.config.galleries[element.data('ck-gallery') || 'classic'];

                $fields.type.empty();

                $.each(_self.config.galleries, function(key, item) {
                    $fields.type.append($('<option></option>').val(key).text(item.title));
                });

                $fields.type.val(element.data('ck-gallery') || 'classic');

                if (gallery.gallery_width) { 
                    $fields.gallery_width.val(element.getStyle('width') || '');
                } else {
                    $fields.gallery_width.parents('label').hide(); 
                }

                if (gallery.gallery_height) { 
                    $fields.gallery_height.val(element.getStyle('height') || '');
                } else {
                    $fields.gallery_height.parents('label').hide();
                } 

                if (gallery.image_width) { 
                    $fields.image_width.val(element.data('image-width') || '');
                } else {
                    $fields.image_width.parents('label').hide(); 
                }

                if (gallery.image_crop) { 
                    $fields.image_crop.attr('checked', element.data('image-crop') ? 'checked' : '');
                } else {
                    $fields.image_crop.parents('label').hide(); 
                }

                if (gallery.image_border) {
                    var firstImage = element.findOne('img');

                    $fields.image_border_width.val(firstImage && firstImage.getStyle('border-width') || '');   
                    $fields.image_border_color.val(firstImage && _self.helpers.rgb2hex(firstImage.getStyle('border-color')) || '').change();   
                } else {
                    $modal.find('.b-gallery__borders').hide();
                }

                $fields.type.change(function() {
                    $modal.find('.b-modal__ok').click();
                    _self.Gallery();
                });

                var createGalleryImage = function(src, title, desc) {

                    var $image = $(
                        '<li class="b-gallery__item">' + 
                            '<a class="b-gallery__item__remove link link--danger">x</a>' +
                            '<div class="b-gallery__item__image"><img /></div>' + 
                            '<label class="form-label"><textarea class="b-gallery__item__title form-textarea" placeholder="Название"></textarea></label>' + 
                            '<label class="form-label"><textarea class="b-gallery__item__desc form-textarea" placeholder="Описание"></textarea></label>' + 
                        '</li>'
                    );

                    if (gallery.image_title) {
                        $image.find('.b-gallery__item__title').val(title || '');
                    } else {
                        $image.find('.b-gallery__item__title').parents('label').hide();
                    }

                    if (gallery.image_desc) {
                        $image.find('.b-gallery__item__desc').val(desc || '');
                    } else {
                        $image.find('.b-gallery__item__desc').parents('label').hide();
                    }

                    $image.find('img').attr('src', src);

                    $image.find('.b-gallery__item__remove').click(function(e) {
                        $(this).parents('.b-gallery__item').remove();
                        $.fancybox.update();

                        e.preventDefault();
                    });

                    return $image;

                }

                $modal.find('.b-gallery__add-item').click(function(){
                    FileManager.open({ 
                        callback: function(file) {
                            $modal
                                .find('.b-gallery__items')
                                .append(createGalleryImage(file.url));
                        }
                    });
                });

                $element.find('img').each(function(){
                    $modal
                        .find('.b-gallery__items')
                        .append(createGalleryImage(this.src, $(this).attr('data-title') || '', $(this).attr('data-desc') || ''));
                });

            },

            save: function($modal, $fields) {
                
                var gallery  = _self.config.galleries[$fields.type.val()];
                gallery.name = $fields.type.val();

                $element
                    .empty()
                    .removeAttr('style')
                    .removeAttr('class')
                    .attr('data-ck-gallery', gallery.name)
                    .attr('data-ck-gallery-id', gallery.name + new Date().getTime())
                    .addClass('ck-gallery-' + gallery.name);
                
                $.each(['width', 'height'], function(key, item) {
                    if (gallery['gallery_' + item] && _self.helpers.isValue($fields['gallery_' + item].val())) {
                        element.setStyle(item, _self.helpers.unit($fields['gallery_' + item].val(), 'px'), true);
                    } else {
                        element.removeStyle(item);
                    }
                });           
                
                if (gallery.image_width && _self.helpers.isValue($fields.image_width.val())) {       
                    $element.attr('data-image-width', _self.helpers.unit($fields.image_width.val(), 'px'));
                } else {
                    $element.removeAttr('data-image-width');
                }

                if ($fields.image_crop.is(':checked') && _self.helpers.isValue($fields.image_width.val()) && gallery.image_crop) {
                    $element.attr('data-image-crop', true);
                } else {
                    $element.removeAttr('data-image-crop');
                }

                $modal.find('.b-gallery__item').each(function() {

                    var $this       = $(this),
                        image_src   = _self.helpers.urlToRelative($this.find('img').attr('src')),
                        image_title = $this.find('.b-gallery__item__title').val(),
                        image_desc  = $this.find('.b-gallery__item__desc').val(),
                        $image      = $('<img />').attr('src', image_src);
                    
                    if (gallery.image_desc && image_desc) { $image.attr('data-desc', image_desc); }

                    if (gallery.image_title && image_title) { $image.attr('data-title', image_title); }

                    $element.append($image);
                    
                });

                /* Рамка
                ---------------------------------------------------------------- */
                            
                if (_self.helpers.isValue($fields.image_border_width.val()) && gallery.image_border) {
                    $element.find('img').css('border', [_self.helpers.unit($fields.image_border_width.val(), 'px'), 'solid', $fields.image_border_color.val()].join(' '));
                } else {
                    $element.find('img').css('border', '')
                }
            
                if (is_new_element) { _self.helpers.insertElement(element, true); }
                
            }
        });

    },

    
    /* Плагин для создания спойлера "подробнее"
    -------------------------------------------------------------------------- */
    
    this.SpoilerMore = function(){

        _self.helpers.insertElement(CKEDITOR.dom.element.createFromHtml('<div class="ck-spoiler-more">Контент</div>'), true);
        
    },

    /* Плагин для создания кнопки 
    -------------------------------------------------------------------------- */
    
    this.BlogMore = function(){
        
        this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>###more###</p>'));
        
    },

    /* Плагин для вставки youtube-видео
    -------------------------------------------------------------------------- */

    this.Youtube = function(){
    
        this.ckeditor().openDialog('youtube');
        return false;
        
    },
     
    /* Разделитель
    -------------------------------------------------------------------------- */
     
    this.Delimiter = function(){
        
        var template =
            '<div class="b-modal__section">' + 
                '<h2 class="b-modal__section__heading heading--md">Разделитель</h2>' +
                '<label class="form-label">' + 
                    '<select data-field="type" class="form-select">' + 
                        '<option value="">Пустой</option>' +
                        '<option value="bordered">В виде линии</option>' +
                    '</select>' +
                '</label>' + 
            '</div>';      
        
        this.helpers.modal({
            template: template,

            save: function($modal, $fields) {
            
                var element = CKEDITOR.dom.element.createFromHtml('<div class="line"></div>');

                if ($fields.type.val()) {
                    element.addClass(['line', $fields.type.val()].join('--'));
                }
                
                _self.helpers.insertElement(element, true);

            }
        });

    },
    
    /* Плагин фичи
    -------------------------------------------------------------------------- */

    this.Features = function(){
        
        var element = this.helpers.getSelectionElement(); //Получение ссылки на выделенный элемент.
        if(!element){ alert('Вы должны выделить элемент!'); return false; }
        
        var $element = $(element.$); //jquery объект

        var w_content = '<div class="b-modal-window"><h2>Фичи</h2>';
        
        w_content += '<div class="features">';
        
        /* Список фич
        ---------------------------------------------------------------------- */

        w_content += '<select name="quotes" style="width: 100%;">' + 
                        '<option>Не выбрано</option>' +
                        '<option data-class="quotes">Цитаты слева</option>' +
                        '<option data-class="quotes_right">Цитаты справа</option>' +
                     '</select>';

        /* ------------------------------------------------------------------- */
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        
        this.helpers.modal(w_content, function(){
        
            var $modal = $('.b-modal-window');

            $modal.find('select option').each(function(){
                
                var $this = $(this);

                if($element.hasClass($(this).data('class')) == true){
                    this.selected = true;
                }
                
            });
            
            
        }, function(){
            
            var $modal = $('.b-modal-window');

            $modal.find('select option').each(function(){
                
                if($(this).attr('selected')) {
                    $element.addClass($(this).data('class'));
                } else {
                    $element.removeClass($(this).data('class'));
                }
                
            });
            
        });
        
    }
    
}]);