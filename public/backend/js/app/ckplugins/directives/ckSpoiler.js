app.directive('ckSpoiler', function () {
    return {
        require: '',
        restrict: 'A',
        transclude: true,
        template: '<div class="tabs">' +
                    '<ul class="tabs__menu">' +
                        '<li ng-repeat="item in tabsItems" ng-class="{ active: item.selected }" class="tabs__menu__item">' + 
                            '<a href="" ng-click="select(item)">[[ item.title ]]</a>' +
                        '</li>' + 
                    '</ul>' +
                    '<div class="tabs__content" ng-transclude></div>' +
                  '</div>',

        link: function($scope) {
            console.log($scope);
        }
    };
})