app.directive('ckGallery', ['$filter', function($filter) {
    return {
        require: '',
        restrict: 'AEC',
        transclude: true,
        templateUrl: $filter('backendAssetsUrl')('/views/ckplugins/gallery.html'),
        link: function($scope, $elm, $attr, $model) {
            
        },

        controller: function() {

        }
    };
}]);