'use strict';

app.classy.controller({

    name: 'LayoutsListCtrl',

    inject: ['$scope', 'config', 'Layouts', 'flash'],

    init: function() {

        this.$.layouts = this.Layouts.query();
        
    },

    delete: function ($index, layout) {

        if (!confirm('Вы действительно хотите удалить данный шаблон?')) {
            return false;
        }

        var _this = this;

        this.Layouts.delete({}, { 'id': layout.id }, function(response) {

            _this.$.layouts.splice($index, 1);
            _this.flash('Шаблон удален.');

        });

    }, 

});