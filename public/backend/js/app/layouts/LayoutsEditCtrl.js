'use strict';

app.classy.controller({
    name: 'LayoutsEditCtrl',

    inject: ['$scope', '$routeParams', 'config', 'Layouts', 'flash'],

    init: function() {

        if (this.$routeParams.layoutId) {
            this.$.layout = this.Layouts.get({ id: this.$routeParams.layoutId });
        } else {
            this.$.layout = new this.Layouts();
        }

    },

    save: function() {

        var _this = this;

        this.$.layout.$save(function() {

            _this.flash('Шаблон сохранен.');

        });

    },

});