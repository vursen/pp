'use strict';

app.factory('Layouts', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/layouts/:id',
        { id: '@id' },
        {
            'query' : { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);