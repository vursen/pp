'use strict';

var app = angular.module('app', ['classy', 'ngResource', 'ngRoute', 'ngAnimate', 'ngTagsInput', 'flash', 'ui.sortable', 'ngTable']);

app.config(['$routeProvider', '$interpolateProvider', '$httpProvider', 'config', function ($routeProvider, $interpolateProvider, $httpProvider, config) {

    $httpProvider.interceptors.push('httpRequestInterceptor');
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $routeProvider

        /* Главная
        ------------------------------------------------------------------- */

        .when('/', { 
            templateUrl: config.backendAssetsUrl + '/views/index.html', 
            controller: 'IndexCtrl'
        })

        /* Настройки
        ------------------------------------------------------------------- */

        .when('/settings', { 
            templateUrl: config.backendAssetsUrl + '/views/settings/index.html', 
            controller: 'SettingsCtrl'
        })

        /* Шаблоны
        ------------------------------------------------------------------- */

        .when('/layouts', { 
            templateUrl: config.backendAssetsUrl + '/views/layouts/list.html', 
            controller: 'LayoutsListCtrl'
        })
        .when('/layouts/edit/:layoutId', { 
            templateUrl: config.backendAssetsUrl + '/views/layouts/edit.html', 
            controller: 'LayoutsEditCtrl' 
        })
        .when('/layouts/create', { 
            templateUrl: config.backendAssetsUrl + '/views/layouts/edit.html', 
            controller: 'LayoutsEditCtrl' 
        })

        /* Страницы
        ------------------------------------------------------------------- */

        .when('/pages', { 
            templateUrl: config.backendAssetsUrl + '/views/pages/list.html', 
            controller: 'PagesListCtrl'
        })
        .when('/pages/edit/:pageId', { 
            templateUrl: config.backendAssetsUrl + '/views/pages/edit.html', 
            controller: 'PagesEditCtrl' 
        })
        .when('/pages/create', { 
            templateUrl: config.backendAssetsUrl + '/views/pages/edit.html', 
            controller: 'PagesEditCtrl' 
        })

        /* Блог
        ------------------------------------------------------------------- */

        .when('/blog', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/index.html', 
            controller: 'BlogIndexCtrl' 
        })

        /* Блог / Записи
        ------------------------------------------------------------------- */

        /*.when('/blog/posts', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/posts/list.html', 
            controller: 'BlogPostsListCtrl' 
        })*/
        .when('/blog/posts/edit/:postId', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/posts/edit.html', 
            controller: 'BlogPostsEditCtrl' 
        })
        .when('/blog/posts/create', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/posts/edit.html', 
            controller: 'BlogPostsEditCtrl' 
        })

        /* Блог / Категории
        ------------------------------------------------------------------- */

        /*.when('/blog/categories', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/categories/list.html', 
            controller: 'BlogCategoriesListCtrl' 
        })*/
        .when('/blog/categories/edit/:categoryId', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/categories/edit.html', 
            controller: 'BlogCategoriesEditCtrl' 
        })
        .when('/blog/categories/:categoryId/posts', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/posts/list.html', 
            controller: 'BlogPostsListCtrl' 
        })
        .when('/blog/categories/create', { 
            templateUrl: config.backendAssetsUrl + '/views/blog/categories/edit.html', 
            controller: 'BlogCategoriesEditCtrl' 
        })

        /* Магазин
        ------------------------------------------------------------------- */

        .when('/store', { 
            templateUrl: config.backendAssetsUrl + '/views/store/index.html', 
            controller: 'StoreIndexCtrl' 
        })

        /* Блог / Продукты
        ------------------------------------------------------------------- */

        /*.when('/store/products', { 
            templateUrl: config.backendAssetsUrl + '/views/store/products/list.html', 
            controller: 'StoreProductsListCtrl' 
        })*/
        .when('/store/products/edit/:productId', { 
            templateUrl: config.backendAssetsUrl + '/views/store/products/edit.html', 
            controller: 'StoreProductsEditCtrl' 
        })
        .when('/store/products/create', { 
            templateUrl: config.backendAssetsUrl + '/views/store/products/edit.html', 
            controller: 'StoreProductsEditCtrl' 
        })

        /* Блог / Категории
        ------------------------------------------------------------------- */

        /*.when('/store/categories', { 
            templateUrl: config.backendAssetsUrl + '/views/store/categories/list.html', 
            controller: 'StoreCategoriesListCtrl' 
        })*/
        .when('/store/categories/edit/:categoryId', { 
            templateUrl: config.backendAssetsUrl + '/views/store/categories/edit.html', 
            controller: 'StoreCategoriesEditCtrl' 
        })
        .when('/store/categories/create', { 
            templateUrl: config.backendAssetsUrl + '/views/store/categories/edit.html', 
            controller: 'StoreCategoriesEditCtrl' 
        })

        /* Блог / Заказы
        ------------------------------------------------------------------- */

        /*.when('/store/orders', { 
            templateUrl: config.backendAssetsUrl + '/views/store/orders/list.html', 
            controller: 'StoreOrderssListCtrl' 
        })*/
        .when('/store/orders/edit/:orderId', { 
            templateUrl: config.backendAssetsUrl + '/views/store/orders/edit.html', 
            controller: 'StoreOrdersEditCtrl' 
        })
        .when('/store/orders/create', { 
            templateUrl: config.backendAssetsUrl + '/views/store/orders/edit.html', 
            controller: 'StoreOrdersEditCtrl' 
        })
        
        .otherwise({redirectTo: '/'});

}]);

app.run(['config', '$window', function (appConfig, $window) {

    CKEDITOR.editorConfig = function(config) {

        //config.toolbar                 = 'Full';
        config.filebrowserWindowWidth  = '700';
        config.filebrowserWindowHeight = '500';
        config.enterMode               = CKEDITOR.ENTER_P;
        config.shiftEnterMode          = CKEDITOR.ENTER_BR;
        config.contentsCss             = [
            appConfig.backendAssetsUrl + "/../css/reset.css",
            appConfig.backendAssetsUrl + "/../css/base.css",
            appConfig.backendAssetsUrl + "/../css/ckplugins.css",
            appConfig.backendAssetsUrl + "/css/ckeditor.css",
            appConfig.backendAssetsUrl + "/js/vendor/ckeditor/contents.css"
        ];
        config.height         = 500;
        config.extraPlugins   = 'onchange';

        config.indentOffset   = 20;
        config.fullPage       = false;
        config.allowedContent = true;

        config.removeButtons = 'Format,Font,tliyoutube';

        config.filebrowserBrowseUrl      = appConfig.backendUrl + '/elfinder/';
        config.filebrowserImageBrowseUrl = appConfig.backendUrl + '/elfinder/';
        config.filebrowserFlashBrowseUrl = appConfig.backendUrl + '/elfinder/';
        config.filebrowserUploadUrl      = appConfig.backendUrl + '/elfinder/';
        config.filebrowserImageUploadUrl = appConfig.backendUrl + '/elfinder/';
        config.filebrowserFlashUploadUrl = appConfig.backendUrl + '/elfinder/';
    }; 

}]);

app.factory('httpRequestInterceptor', ['flash', '$q', '$window', 'config', function (flash, $q, $window, config) {
    return {
        responseError: function(rejection) {

            if (rejection.status == '401') {
                $window.location.href = config.backendUrl + '/auth';
            } else {
                flash('error', (rejection.data.error ? rejection.data.error : 'Неизвестная ошибка.'));
            }
            
            return $q.reject(rejection);

        }
     };
}]);