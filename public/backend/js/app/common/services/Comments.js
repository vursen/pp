'use strict';

app.factory('Comments', ['$resource', 'config', function ($resource, config) {
    return $resource(
        config.backendUrl + '/comments/:id',
        { id: '@id' },
        {
            'query' : { isArray: true },
            'delete': { method: 'POST', params: { '_method' : 'delete' }}
        }
    );
}]);