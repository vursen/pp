'use strict';

app.service('FileManager', ['config', '$window', function (config, $window) {

    this.open = function (params) {

        var ELFinder = $window.open(config.backendUrl + '/elfinder/', '', 'width=800, height=420, scrollbars=0');
        $(ELFinder).load(function() {
            
            if (params) {

                ELFinder.window.fileCallback = function(file) {

                    if (params.field) {
                        params.field.val(file.url);
                    }
                    
                    if (params.callback) {
                        params.callback(file);
                    }
                    
                };

            }

        });

    }

}]);