'use strict';

app.service('CKEditor', ['config', 'FileManager', function (appConfig, FileManager) {

    this.ckeditor = function () {
        return CKEDITOR.instances[Object.keys(CKEDITOR.instances)[0]];
    }

    var _self = this;

    this.colorpicker = ["000000", "ffffff", "4a4a4a", "b32525","e78787","265f8c", "5c9da9","1c6224","4ba047","e79918","fce97c","C0C0C0","dadada", "transparent"];
    this.defaultcolor = '000000';

    function rgb2hex(rgb) {
        if (rgb.match(/transparent/)) { return 'transparent'; }
        if (rgb.match(/^#([0-9A-Za-z]{6})$/)) { return rgb; }

        rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);

        function hex(x) { return ("0" + parseInt(x).toString(16)).slice(-2); }

        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    var isEmpty = function(object){
        return $.isEmptyObject(object);
    }
                
    var isNumeric = function(val){
        return $.isNumeric(val);  
    }

    var urlToRelative = function(url) {
        return url.replace(/^(?:\/\/|[^\/]+)*/, "");
    }

    this.getSelectionElement = function(options){
        
        var options         = options || null;
        var founded_element = false;

        if(this.ckeditor().getSelection() != null) {
            
            var element = this.ckeditor().getSelection().getStartElement();

            if (options) {

                while(!founded_element) {
                    
                    if (element.is('body')) break;
                    
                    if (options.tagName && element.is(options.tagName)) {
                        founded_element = true;
                        break;
                    }
                    if (options.className && element.hasAttribute('class') == true && element.hasClass(options.className) == true) {    
                        founded_element = true;
                        break;
                    }
                    if (options.data && element.data(options.data)) {   
                        founded_element = true;
                        break;  
                    }
                    
                    element = element.getParent();
                }

            } else {

                founded_element = true;

            }

        }

        if(!founded_element){
            
            return false;
            
        }
        
        return element;
            
    },

    this.showWindow = function(html, onOpen, onClose, options){
        
        var options = $.extend({beforeShow      : onOpen,
                                closeBtn        : false, 
                                maxWidth        : '70%',
                                minWidth        : 350,
                                autoSize        : true,
                                padding         : 5,
                                //width         : 'auto',
                                openSpeed       : 0,
                                closeSpeed  : 0}, options);

        var $html = $(html);

        $html.find('.modal-cancel').click(function () {
            $.fancybox.close();
        });

        $html.find('.modal-ok').click(function () {
            if (onClose() != false) {
                $.fancybox.close();
            }
            _self.ckeditor().fire('change');
        });

        $html.find('[data-filemanager]').click(function() {
            FileManager.open({
                field: $(this)
            });
        });

        $html.find('[data-colorpicker]').colorPicker({ 
            pickerDefault: _self.defaultcolor, 
            colors: _self.colorpicker, 
            transparency: true
        });

        $.fancybox($html, options);
        
    },
    
    /* Плагин для работы с изображениями
    -------------------------------------------------------------------------- */

    this.Image = function() { 

        var responsive_class = 'res_img';

        var new_element = false;
        var element = this.getSelectionElement({tagName : 'img'}); //Получение ссылки на выделенный элемент.
        if(!element){
            new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<img />');   
        }

        var $element = $(element.$)
        //-------------------Подготовка html-модального окна-------------------//
        
        var w_content = '<div class="b-modal-window"><h2>Изображение</h2>';

        w_content += '*Путь: <input name="img_path" data-filemanager="true" value="' + ((isEmpty(element.getAttribute('src'))) ? 'Нажмите сюда, чтобы выбрать изображение.' : element.getAttribute('src')) + '"  readonly="readonly"  /><br>';

        w_content += 'Alt: <br><textarea name="img_alt" style="height: 50px;">' + ((isEmpty(element.getAttribute('alt'))) ? '' : element.getAttribute('alt')) + '</textarea><br>';

        w_content += 'Ширина изображения: <input name="img_width" style="width:55px;" value="' + ((isEmpty(element.getStyle('width'))) ? '' : element.getStyle('width')) + '"/><br>';


        /**
         * Рамка
         */

        w_content += '<h3>Рамка</h3>';
        w_content += 'Цвет: <input name="img_border_color" data-colorpicker value="' + ((isEmpty(element.getStyle('border-color'))) ? this.defaultcolor : rgb2hex(element.getStyle('border-color'))) + '" maxlength="5" /><br> ';
        w_content += 'Толщина: <input name="img_border" style="width:55px;" value="' + ((isEmpty(element.getStyle('border-width'))) ? '' : element.getStyle('border-width')) + '"/> ';


        /**
         * Отступ
         */

        w_content += '<h3>Отступ</h3>';
        
        if(element.getStyle('float') == 'left'){
            w_content += '<input type="radio" name="img_position" value="left" checked />Лево <input name="img_position_left" style="width:55px;" value="' + element.getStyle('margin-left') + '">';
        } else {
            w_content += '<input type="radio" name="img_position" value="left" />Лево <input name="img_position_left" style="width:55px;">';    
        }
        if(element.getStyle('float') == 'right'){
            w_content += '<input type="radio" name="img_position" value="right" checked />Право <input name="img_position_right" style="width:55px;" value="' + element.getStyle('margin-right') + '">';
        } else {
            w_content += '<input type="radio" name="img_position" value="right" />Право <input name="img_position_right" style="width:55px;">'; 
        }
        if(element.getStyle('margin') == '0px auto'){
            w_content += '<input type="radio" name="img_position" value="center" checked />Центр';
        } else {
            w_content += '<input type="radio" name="img_position" value="center" />Центр';  
        }

        /**
         * Эффекты
         */

        w_content += '<h3>Эффекты</h3>';

        w_content += '<img style="border: 0px solid ; width: 50px; height: 50px;float:left;margin:5px 10px 0 0;" alt=" " src="' + appConfig.backendAssetsUrl + '/img/exsando.jpg"/><input type="radio" name="effect" id="image-effect-expando"/><b>Expando</b> <br />увеличение изображения при подводе мышки до исходного размера (рекомедуемая ширина импортируемого изображения для данного эффекта - от 300 до 500px)<div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';
        
        w_content += '<img style="border: 0px solid ; width: 50px; height: 50px;float:left;margin:5px 10px 0 0;" alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy.jpg"/><input type="radio" name="effect" id="image-effect-fancybox" /><b>Fancybox</b><br />при клике мышкой увеличенное изображение откроется на затемненнои фоне. Стрелки перехода на другое изображение отсутствуют<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';    

        w_content += '<img style="border: 0px solid ; width: 50px; height: 50px;float:left;margin:5px 10px 0 0;" alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy_auto.jpg"/><input type="radio" name="effect" id="image-effect-fancybox-auto" /><b>Fancybox Auto</b><br />при клике мышкой увеличенное изображение откроется на затемненнои фоне. Присутствуют стрелки перехода на другое изображение<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';
        
        w_content += '<img style="border: 0px solid ; width: 50px; height: 50px;float:left;margin:5px 10px 0 0;" alt=" " src="' + appConfig.backendAssetsUrl + '/img/fancy_iframe.jpg"/><input type="radio" name="effect" id="image-effect-fancybox-frame"/><b>Fancybox Iframe</b><br />при клике мышкой откроется стороннее изображение (JPG, SWF, HTML или TXT) на затемненном фоне. <br>' + 
                     'Ссылка: <input name="img_fancy_link" style="width:300px;" value=""><br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';

        w_content += '<img style="border: 0px solid ; width: 50px; height: 50px;float:left;margin:5px 10px 0 0;" alt=" " src="' + appConfig.backendAssetsUrl + '/img/adaptiv_img.jpg"/><input type="checkbox" name="responsive-image"/><b>Responsive Image</b><br />размер картинки по ширине адаптируется размеру ширины поля страницы. Данная функция особо актуальна для "резиновых" по ширине сайтов.<br /><div class="clear" style="border-bottom:1px solid #dbdcdd;width:100%;height:3px;"></div>';

        w_content += '<input type="radio" name="effect" id="image-effect-none" style=""/> <b>Эффекты не используются</b> <br />';
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';

        /**
         * Открываем модальное окно
         */
        
        var _this = this;
        
        this.showWindow(w_content, function(){
                
                var $modal      = $('.b-modal-window');
                
                var is_checked = false;
                $modal.find('input[name="effect"]').each(function(){
                    
                    if($element.hasClass($(this).attr('id')) == true){
                        
                        if($(this).attr('id') == 'image-effect-fancybox-frame' && !isEmpty(element.data('fancy-link'))){
                            $modal.find('input[name="img_fancy_link"]').val(element.data('fancy-link'));
                        }
                        
                        $(this).attr('checked', 'checked');
                        is_checked = true;
                        
                    }
                    
                });

                if ($element.hasClass(responsive_class) == true) {

                    $modal.find('input[name="responsive-image"]').attr('checked', 'checked');

                }


                if(!is_checked){
                    $modal.find('#image-effect-none').attr('checked', 'checked');
                }
                
            
            }, function(){
                
                var $modal = $('.b-modal-window');
                
                //Обработка поля путь к изображению
                
                var img_path = $modal.find('input[name="img_path"]').val();
                if(img_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны выбрать изображение!'); return false; }
                
                element.setAttribute('data-cke-saved-src', img_path);
                element.setAttribute('src', img_path);
                
                //Обработка поля alt
                
                var img_alt = $modal.find('[name="img_alt"]').val();
                element.setAttribute('alt', img_alt.replace(/[\r\n]/g, ' '));
                
                //Обработка рамки
                
                var img_border = $modal.find('[name="img_border"]').val();
                if(!isEmpty(img_border)){
                    element.setStyle('border-width', (isNumeric(img_border)) ? img_border + 'px' : img_border, true);
                    element.setStyle('border-style', 'solid', true);    
                } else {
                    element.setStyle('border-width', '', true); 
                }
                
                var img_border_color = $modal.find('[name="img_border_color"]').val();
                if(!isEmpty(img_border)){
                    element.setStyle('border-color', img_border_color, true);
                } else {
                    element.setStyle('border-color', '', true); 
                }
                
                //Обработка эффектов
                
                $modal.find('[name="effect"]').each(function(){
                    if($(this).is(':checked')){

                        if($(this).attr('id') != 'image-effect-none'){
                            element.addClass($(this).attr('id'));
                        }

                        if($(this).attr('id') == 'image-effect-fancybox-frame'){
                            element.data('fancy-link', $modal.find('input[name="img_fancy_link"]').val());
                        } else {
                            element.data('fancy-link', ''); 
                        }

                    } else {
                        element.removeClass($(this).attr('id'));
                    }
                });

                if ($modal.find('[name="responsive-image"]').is(':checked')) {

                    element.addClass(responsive_class);

                } else {

                    element.removeClass(responsive_class);

                }

                //Обработка поля ширина
                
                var img_width = $modal.find('[name="img_width"]').val();
                if(!isEmpty(img_width)){
                    element.setStyle('width', (isNumeric(img_width)) ? img_width + 'px' : img_width, true);
                } else {
                    element.setStyle('width', '', true);    
                }
                
                //Обработка отступа
                
                var img_position = $modal.find('[name="img_position"]:checked').val();
                if(!isEmpty(img_position)){
                    
                    element.removeStyle('float');
                    element.removeStyle('margin');
                    if(img_position == 'center'){
                        
                        element.setStyle('float', 'none');  
                        element.setStyle('margin', '0px auto', true);
                        
                    }else if(img_position == 'right'){
                        
                        var img_position_right = $modal.find('[name="img_position_right"]').val();
                        element.setStyle('float', 'right');
                        element.setStyle('margin-right', (isNumeric(img_position_right)) ? img_position_right + 'px' : img_position_right, true);
                        element.setStyle('margin-left', '15px', true);
                        
                    }else if(img_position == 'left'){
                        
                        var img_position_left = $modal.find('[name="img_position_left"]').val();
                        element.setStyle('float', 'left');
                        element.setStyle('margin-left', (isNumeric(img_position_left)) ? img_position_left + 'px' : img_position_left, true);
                        element.setStyle('margin-right', '15px', true);
                        
                    } else {
                        element.removeStyle('margin');
                        element.removeStyle('float');
                    }
                    
                    
                }
                
                //Обработка ссылки fancybox frame

                if(new_element){
                    _this.ckeditor().insertElement(element);
                    _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
                }
                
        });

                
    },
    
    /* Плагин для создания iframe
    -------------------------------------------------------------------------- */

    this.Iframe = function(){
    
        var element = CKEDITOR.dom.element.createFromHtml('<iframe></iframe>'); 
        var w_content = '<div class="b-modal-window"><h2>Окно-iframe</h2>';
        
        //var frame_link = (isEmpty(element.getAttribute('src'))) ? '' : element.getAttribute('src');
        w_content += '*Ссылка: <input name="link" style="width:300px;"><br>';
        
        //var frame_width = (isEmpty(element.getStyle('width'))) ? '' : element.getStyle('width');
        w_content += 'Ширина: <input name="width" style="width:55px;" ><br>';
        
        //var frame_height = (isEmpty(element.getStyle('height'))) ? '' : element.getStyle('height');
        w_content += 'Высота: <input name="height" style="width:55px;"><br>';

        //w_content += '<input name="video" type="checkbox"> Видео<br>'
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, null , function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Обработка поля ссылка

            var src = $modal.find('input[name="link"]').val();
            if(isEmpty(src)){   alert('Вы должны заполнить поле "Ссылка"!'); return false; }
            element.setAttribute('src', src);
            
            //Обработка поля ширина
            
            var width = $modal.find('[name="width"]').val();
            if(!isEmpty(width)){
                element.setAttribute('width', (isNumeric(width)) ? width + 'px' : width, true);
            } else {
                element.setAttribute('width', '100%', true);    
            }
            
            //Обработка поля высота
            
            var height = $modal.find('[name="height"]').val();
            if(!isEmpty(height)){
                element.setAttribute('height', (isNumeric(height)) ? height + 'px' : height, true);
            } else {
                element.setAttribute('onload', 'frame_load(this);');    
            }

            /*if($modal.find('[name="video"]').is(':checked')){
                element.addClass('video');
            }*/
        
            element.setAttribute('frameborder', '0');
            element.setAttribute('scrolling', 'no');
            
            _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
        
                
        });
    },
    
    /* Плагин для создания div
    -------------------------------------------------------------------------- */

    this.Div = function(){
        
        var new_element = false;
        var element = this.getSelectionElement({className : 'div-window'}); //Получение ссылки на выделенный элемент.
        if(!element){
            new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<div class="div-window"><span class="div-window-content">Контент</span></div>'); 
        }
        
        //-------------------Подготовка html-модального окна-------------------//

        var w_content = '<div class="b-modal-window"><h2>Окно</h2>';
        
        //Ширина
        var div_width = (isEmpty(element.getStyle('width'))) ? '' : element.getStyle('width');
        w_content += 'Ширина: <input name="div_width" style="width:55px;" value="' + div_width + '"/><br>';
        
        //Высота
        var div_height = (isEmpty(element.getStyle('height'))) ? '' : element.getStyle('height');
        w_content += 'Высота: <input name="div_height" style="width:55px;" value="' + div_height + '"/><br>';
        
        //Цвет фона
        var div_background_color = (isEmpty(element.getStyle('background-color'))) ? '#ffffff' : rgb2hex(element.getStyle('background-color'));
        w_content += '<h3>Фон</h3>' +
                     'Цвет: <input data-colorpicker name="div_background_color" style="width:62px;" value="' + div_background_color + '" maxlength="5" />';  
                     
        //Рамка
        var div_border_right    = element.getStyle('border-right');
        var div_border_left     = element.getStyle('border-left');
        var div_border_top      = element.getStyle('border-top');
        var div_border_bottom   = element.getStyle('border-bottom');
        
        if(!isEmpty(element.getStyle('border-width'))){
            var div_border_width = element.getStyle('border-width');        
        }else if(!isEmpty(element.getStyle('border-right-width')) && !isEmpty(div_border_right)){       
            var div_border_width = element.getStyle('border-right-width');      
        }else if(!isEmpty(element.getStyle('border-left-width')) && !isEmpty(div_border_left)){ 
            var div_border_width = element.getStyle('border-left-width');   
        }else if(!isEmpty(element.getStyle('border-top-width')) && !isEmpty(div_border_top)){
            var div_border_width = element.getStyle('border-top-width');    
        }else if(!isEmpty(element.getStyle('border-bottom-width')) && !isEmpty(div_border_bottom)){
            var div_border_width = element.getStyle('border-bottom-width'); 
        } else {
            var div_border_width = '';
        }
        
        if(!isEmpty(element.getStyle('border-color'))){
            var div_border_color = rgb2hex(element.getStyle('border-color'));
        }else if(!isEmpty(element.getStyle('border-right-color')) && !isEmpty(div_border_right)){
            var div_border_color = rgb2hex(element.getStyle('border-right-color'));
        }else if(!isEmpty(element.getStyle('border-left-color')) && !isEmpty(div_border_left)){
            var div_border_color = rgb2hex(element.getStyle('border-left-color'));
        }else if(!isEmpty(element.getStyle('border-top-color')) && !isEmpty(div_border_top)){
            var div_border_color = rgb2hex(element.getStyle('border-top-color'));   
        }else if(!isEmpty(element.getStyle('border-bottom-color')) && !isEmpty(div_border_bottom)){
            var div_border_color = rgb2hex(element.getStyle('border-bottom-color'));
        } else {    
            var div_border_color = '';      
        }
        
        w_content += '<h3>Рамка</h3>' +
                     'Толщина: <input name="div_border_width" style="width:50px;" value="' + div_border_width + '" maxlength="5" />'; 
    
        if((!isEmpty(div_border_right) && isEmpty(element.getStyle('border-width'))) || (!isEmpty(element.getStyle('border-width')))){
            w_content += '<input type="checkbox" name="div_border_right" checked> Справа&nbsp;';
        } else {
            w_content += '<input type="checkbox" name="div_border_right"> Справа&nbsp;';
        }
        if((!isEmpty(div_border_left) && isEmpty(element.getStyle('border-width'))) || (!isEmpty(element.getStyle('border-width')))){
            w_content += '<input type="checkbox" name="div_border_left" checked> Слева&nbsp;';
        } else {
            w_content += '<input type="checkbox" name="div_border_left"> Слева&nbsp;';
        }
        if((!isEmpty(div_border_top) && isEmpty(element.getStyle('border-width'))) || (!isEmpty(element.getStyle('border-width')))){
            w_content += '<input type="checkbox" name="div_border_top" checked> Сверху&nbsp;';
        } else {
            w_content += '<input type="checkbox" name="div_border_top"> Сверху&nbsp;';
        }
        if((!isEmpty(div_border_bottom) && isEmpty(element.getStyle('border-width'))) || (!isEmpty(element.getStyle('border-width')))){
            w_content += '<input type="checkbox" name="div_border_bottom" checked> Снизу&nbsp;';
        } else {
            w_content += '<input type="checkbox" name="div_border_bottom"> Снизу&nbsp;';
        }
        w_content += '<br/>Цвет: <input data-colorpicker name="div_border_color" style="width:62px;" value="' + div_border_color + '" maxlenght="7" /><br>';
        
        //Внутренний отступ
        w_content += '<h3>Внутренний отступ</h3>' +
                     'Слева: <input name="padding-left" value="' + element.getStyle('padding-left') + '" style="width: 40px;">' + 
                     'Справа: <input name="padding-right" value="' + element.getStyle('padding-right') + '" style="width: 40px;">' + 
                     'Сверху: <input name="padding-top" value="' + element.getStyle('padding-top') + '" style="width: 40px;">' +
                     'Снизу: <input name="padding-bottom" value="' + element.getStyle('padding-bottom') + '" style="width: 40px;">';
        
        //Внешний отступ
        w_content += '<h3>Внешний отсуп</h3>' +
                     'Слева: <input name="margin-left" value="' + element.getStyle('margin-left') + '" style="width: 40px;">' + 
                     'Справа: <input name="margin-right" value="' + element.getStyle('margin-right') + '" style="width: 40px;">' + 
                     'Сверху: <input name="margin-top" value="' + element.getStyle('margin-top') + '" style="width: 40px;">' +
                     'Снизу: <input name="margin-bottom" value="' + element.getStyle('margin-bottom') + '" style="width: 40px;">';
        
        //Обтекание 
        var div_float = element.getStyle('float');           
        w_content += '<h3>Обтекание</h3>' + 
                     '<select name="div_float">'; 
        
        w_content += (isEmpty(div_float) || div_float == 'none') ? '<option value="none" selected>Не используется</option>' : '<option value="none">Не используется</option>';
        w_content += (div_float == 'right') ? '<option value="right" selected>Слева</option>' : '<option value="right">Слева</option>';
        w_content += (div_float == 'left') ? '<option value="left" selected>Право</option>' : '<option value="left">Право</option>';
        w_content += '</select>';
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, {}, function(){
            

                var $modal = $('.b-modal-window');
                
                //Обработка обтекания
                
                element.removeClass('div-window-right'); element.removeClass('div-window-left'); element.removeClass('div-window-none');
                element.setStyle('float', $modal.find('select[name="div_float"]').val());
                element.addClass('div-window-' + $modal.find('select[name="div_float"]').val());
                
                //Обработка поля ширина
                
                var div_width = $modal.find('input[name="div_width"]').val();
                if(!isEmpty(div_width)){
                    element.setStyle('width', (isNumeric(div_width)) ? div_width + 'px' : div_width, true);
                } else {
                    element.setStyle('width', 'auto', true);    
                }
                
                //Обработка поля высота
                
                var div_height = $modal.find('input[name="div_height"]').val();
                if(!isEmpty(div_height)){
                    element.setStyle('height', (isNumeric(div_height)) ? div_height + 'px' : div_height, true);
                } else {
                    element.setStyle('height', 'auto', true);
                }
            
                //Обработка цвета фона
                
                element.setStyle('background-color', $modal.find('input[name="div_background_color"]').val());
                
                //Обработка рамки
                
                var div_border_color = $modal.find('input[name="div_border_color"]').val();
                var div_border_width = $modal.find('input[name="div_border_width"]').val();
                
                if(!isEmpty(div_border_color) && !isEmpty(div_border_width)){
                    if($modal.find('input[name="div_border_right"]').is(':checked')){
                        element.setStyle('border-right', parseFloat(div_border_width) + 'px solid ' + div_border_color);
                    }
                    if($modal.find('input[name="div_border_left"]').is(':checked')){
                        element.setStyle('border-left', parseFloat(div_border_width) + 'px solid ' + div_border_color);
                    }
                    if($modal.find('input[name="div_border_top"]').is(':checked')){
                        element.setStyle('border-top', parseFloat(div_border_width) + 'px solid ' + div_border_color);
                    }
                    if($modal.find('input[name="div_border_bottom"]').is(':checked')){
                        element.setStyle('border-bottom', parseFloat(div_border_width) + 'px solid ' + div_border_color);
                    }
                }
            
                //Обработка внешнего отступа
                
                $modal.find('input[name^="margin"]').each(function(){
                    element.setStyle($(this).attr('name'), (isNumeric($(this).val())) ? $(this).val() + 'px' : $(this).val(), true);
                });
                
                //Обработка внутреннего отступа
                
                $modal.find('input[name^="padding"]').each(function(){
                    element.setStyle($(this).attr('name'), (isNumeric($(this).val())) ? $(this).val() + 'px' : $(this).val(), true);
                });
                
                if(new_element){
                    _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
                    _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p><p>&nbsp;</p>'));
                }
            
                
        });
    },
            
    /* Плагин для вставки audio
    -------------------------------------------------------------------------- */
    
    this.Audio = function(){
    
        var new_element = false;
        var element = this.getSelectionElement({className : 'audiopleer'}); //Получение ссылки на выделенный элемент.
        if(!element){
            new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<div class="audiopleer">Аудио</div>');   
        }
            
        var w_content = '<div class="b-modal-window"><h2>Аудио</h2>';
        
        //Путь к файлу
        
        var audio_path = (isEmpty(element.data('audio-url'))) ? 'Нажмите сюда, чтобы выбрать изображение.' : element.data('audio-url');
        w_content += '*Путь к файлу: <input name="audio_path" value="' + audio_path + '" readonly="readonly" data-filemanager /><br>';
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';

        var _this = this;
        this.showWindow(w_content, null , function(){
    
            var $modal = $('.b-modal-window');
            
            //Обработка поля путь к файлу

            var audio_path = $modal.find('input[name="audio_path"]').val();
            if(audio_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны заполнить поле "Путь к файлу"!'); return false; }
            element.data('audio-url', audio_path);
            
            if(new_element){
                _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
                _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
            }
                
        });
    },
    
    /* Плагин для создания кнопок
    -------------------------------------------------------------------------- */
    
    this.Button = function(){
    
        var new_element = false;
        var element = this.getSelectionElement({className : 'hover-button'}); //Получение ссылки на выделенный элемент.
        if(!element){
            new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<a class="hover-button"><img data-img1="" data-img2="" src="" /></a>');  
        }
            
        var w_content = '<div class="b-modal-window"><h2>Кнопка</h2>' + 
                        'Ссылка: <input name="button_url" style="width:400px;"/><br>' + 
                        '<input type="checkbox" name="button_fancy_iframe"/>Открыть в iframe<br>' + 
                        'Ширина: <input name="button_width" style="width:55px;" /><br>' + 
                        'Высота: <input name="button_height" style="width:55px;" /><br>';
                        
        w_content += '<h3>Отступ</h3>' +
                     '<input type="radio" name="button_position" value="center" />Центр ' + 
                     '<input type="radio" name="button_position" value="left" />Лево <input name="button_position_right" style="width:55px;">' + 
                     '<input type="radio" name="button_position" value="right" />Право <input name="button_position_left" style="width:55px;">' + 
                     '<input type="radio" name="button_position" value="none"/>Нет отступа';
                     
        w_content += '<h3>*Изображение 1</h3>' + 
                     'Путь: <input name="button_img1_path" style="width:400px; cursor:pointer;" value="Нажмите сюда, чтобы выбрать изображение." data-filemanager readonly="readonly"  /><br>' + 
                     'Прозрачность: <input name="button_img1_opacity" style="width:40px;"/>%' + 
                     
                     '<h3>*Изображение 2</h3>' +
                     'Путь: <input name="button_img2_path" style="width:400px; cursor:pointer;" value="Нажмите сюда, чтобы выбрать изображение." data-filemanager readonly="readonly"  /><br>' + 
                     'Прозрачность: <input name="button_img2_opacity" style="width:40px;"/>%';
        
    
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';

        var _this = this;
        this.showWindow(w_content, function(){
        
            var $modal = $('.b-modal-window');
            
            //Путь
            
            if(!isEmpty(element.getAttribute('data-cke-saved-href'))){
                $modal.find('input[name="button_url"]').val(element.getAttribute('data-cke-saved-href'));
            }
            
            //Открыть в iframe
            
            if(element.data('fancy-iframe') == 'true'){
                $modal.find('input[name="button_fancy_iframe"]').attr('checked', 'checked');
            }
            
            //Ширина
            
            if(!isEmpty(element.getFirst().getStyle('width'))){
                $modal.find('input[name="button_width"]').val(element.getFirst().getStyle('width'));
            }
            
            //Высота
            
            if(!isEmpty(element.getFirst().getStyle('height'))){
                $modal.find('input[name="button_height"]').val(element.getFirst().getStyle('height'));
            }
            
            //Отступ
            
            if(element.getStyle('text-align') == 'center'){
                $modal.find('input[name="button_position"][value="center"]').attr('checked', 'checked');
            }else if(element.getFirst().getStyle('float') == 'left'){   
                $modal.find('input[name="button_position"][value="left"]').attr('checked', 'checked');
                $modal.find('input[name="button_position_left"]').val(element.getFirst().getStyle('margin-right')); 
            }else if(element.getFirst().getStyle('float') == 'right'){
                $modal.find('input[name="button_position"][value="right"]').attr('checked', 'checked');
                $modal.find('input[name="button_position_right"]').val(element.getFirst().getStyle('margin-left')); 
            } else {
                $modal.find('input[name="button_position"][value="none"]').attr('checked', 'checked');
            }
            
            //Путь к изображению 1
            
            if(!isEmpty(element.getFirst().data('img1'))){
                $modal.find('input[name="button_img1_path"]').val(element.getFirst().data('img1'));
            }
            
            //Прозрачность изображения 1
            
            if(!isEmpty(element.getFirst().data('img1-opacity'))){
                $modal.find('input[name="button_img1_opacity"]').val(element.getFirst().data('img1-opacity'));
            }
            
            //Путь к изображению 2
            
            if(!isEmpty(element.getFirst().data('img2'))){
                $modal.find('input[name="button_img2_path"]').val(element.getFirst().data('img2'));
            }
            
            //Прозрачность изображения 2
            
            if(!isEmpty(element.getFirst().data('img2-opacity'))){
                $modal.find('input[name="button_img2_opacity"]').val(element.getFirst().data('img2-opacity'));
            }
            
        }, function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Обработка поля путь к изображению 1

            var button_img1_path = $modal.find('input[name="button_img1_path"]').val();
            if(button_img1_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны выбрать путь для первого изображения!'); return false; }
            element.getFirst().data('img1', button_img1_path);
            element.getFirst().setAttribute('data-cke-saved-src', button_img1_path);
            element.getFirst().setAttribute('src', button_img1_path);
            
            //Обработка поля прозрачность изображения 1
            
            var button_img1_opacity = $modal.find('input[name="button_img1_opacity"]').val();
            if(isEmpty(button_img1_opacity)){ alert('Вы должны установить прозрачность для первого изображения!'); return false; }
            element.getFirst().data('img1-opacity', $modal.find('input[name="button_img1_opacity"]').val());
            
            //Обработка поля путь к изображению 2
            
            var button_img2_path = $modal.find('input[name="button_img2_path"]').val();
            if(button_img2_path.indexOf('Нажмите сюда, чтобы выбрать изображение.') != (-1)){ alert('Вы должны выбрать путь для второго изображения!'); return false; }
            element.getFirst().data('img2', $modal.find('input[name="button_img2_path"]').val());
            
            //Обработка поля прозрачность изображения 2
            
            var button_img2_opacity = $modal.find('input[name="button_img2_opacity"]').val();
            if(isEmpty(button_img2_opacity)){ alert('Вы должны установить прозрачность для второго изображения!'); return false; }
            element.getFirst().data('img2-opacity', $modal.find('input[name="button_img2_opacity"]').val());
            
            //Обработка поля ширина
                
            var button_width = $modal.find('input[name="button_width"]').val();
            if(!isEmpty(button_width)){
                element.getFirst().setStyle('width', (isNumeric(button_width)) ? button_width + 'px' : button_width, true);
            } else {
                element.getFirst().setStyle('width', 'auto', true); 
            }
            
            //Обработка поля высота
            
            var button_height = $modal.find('input[name="button_height"]').val();
            if(!isEmpty(button_height)){
                element.getFirst().setStyle('height', (isNumeric(button_height)) ? button_height + 'px' : button_height, true);
            } else {
                element.getFirst().setStyle('height', 'auto', true);
            }
            
            //Установка ссылки
            
            element.setAttribute('data-cke-saved-href', $modal.find('input[name="button_url"]').val());
            
            //Установка fancy-iframe
            
            element.data('fancy-iframe',($modal.find('input[name="button_fancy_iframe"]').is(':checked')) ? true : false);  
            
            //Обработка внешнего отступа
                
            /*$modal.find('input[name^="margin"]').each(function(){
                element.setStyle($(this).attr('name'), (isNumeric($(this).val())) ? $(this).val() + 'px' : $(this).val(), true);
            });*/
          
            //Обработка обтекания
            
            //element.setStyle('float', $modal.find('select[name="button_float"]').val());
            
            //Обработка отступа
              
            var button_position = $modal.find('input[name="button_position"]:checked').val();
            if(!isEmpty(button_position)){
                    
                element.removeStyle('text-align');
                element.getFirst().removeStyle('float');
                element.getFirst().removeStyle('margin');
                
                if(button_position == 'center'){
                    
                    element.getFirst().setStyle('float', 'none');
                    element.getFirst().setStyle('margin', '0px');   
                    element.setStyle('text-align', 'center', true);
                    element.setStyle('display', 'block', true);
                    
                }else if(button_position == 'right'){
                    
                    var button_position_right = $modal.find('input[name="button_position_right"]').val();
                    element.getFirst().setStyle('float', 'right');
                    element.getFirst().setStyle('margin-left', (isNumeric(button_position_right)) ? button_position_right + 'px' : button_position_right, true);
                    element.getFirst().setStyle('margin-right', '15px', true);
                    
                }else if(button_position == 'left'){
                    
                    var button_position_left = $modal.find('input[name="button_position_left"]').val();
                    element.getFirst().setStyle('float', 'left');
                    element.getFirst().setStyle('margin-right', (isNumeric(button_position_left)) ? button_position_left + 'px' : button_position_left, true);
                    element.getFirst().setStyle('margin-left', '15px', true);
                    
                }       
                
            }   
            
            if(new_element){
                _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
                _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
            }
                
        });
    },
    
    /* Плагин для создания столбцов
    -------------------------------------------------------------------------- */

    
    this.Columns = function(){
    
        var w_content = '<div class="b-modal-window"><h2>Столбцы</h2>';
        
        //Количество столбцов
        w_content += 'Количество столбцов: <input name="column_count" style="width:55px;"/>';
            
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, null , function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Создание кода столбцов
            
            if(isEmpty($modal.find('input[name="column_count"]').val())){ alert('Вы должны заполнить поле "Количество столбцов"!'); return false; }
            
            var html = '<div class="columns">';
            var column_count = (Math.round(100 /  $modal.find('input[name="column_count"]').val())).toString() + "%";
            
            for(var i = 0; i < $modal.find('input[name="column_count"]').val(); i++){
                html += '<div class="column" style="width:' + column_count + ';"><div class="column-body"><p>Содержимое</p><span class="column-delimiter">&nbsp;</span><a class="column-button">&nbsp;</a></div></div>';
            }
            html += '<div class="columns-clear"></div></div>';
            
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml(html));
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp</p>'));
            
        });
        
    },
    
    /* Плагин для создания спойлера
    -------------------------------------------------------------------------- */
    
    this.Toggle = function(){
        
        var w_content = '<div class="b-modal-window"><h2>Спойлер</h2>';
        
        w_content += 'Заголовок: <br /><textarea name="toggle_title" style="width:400px; height:50px;"></textarea><br />' +
                     'Содержимое: <br /><textarea name="toggle_content" style="width:400px;"></textarea><br />';
            
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, null , function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Создание кода спойлера
            
            var html = '<div class="toggle"><div class="toggle-title">'+ $modal.find('textarea[name="toggle_title"]').val() +'</div><div class="toggle-content">'+ $modal.find('textarea[name="toggle_content"]').val() +'</div></div>';
            
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml(html));
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp</p>'));
            
        });
        
    },
    
    /* Плагин для вставки страницы
    -------------------------------------------------------------------------- */
    
    this.IncludePage = function(){
    
        var w_content = '<div class="b-modal-window"><h2>Вставка страницы</h2>';
        
        w_content += '*Название: <input name="name" style="width:300px;"/><br />';
            
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, null , function(){
            
            
            var $modal = $('.b-modal-window');
            
            //Создание кода спойлера
            
            if(isEmpty($modal.find('input[name="name"]').val())){ alert('Вы должны заполнить поле "Название"!'); return false; }
            
            var html = '<div class="include_page">@page(' + $modal.find('input[name="name"]').val() + ')</div>';
            
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml(html));
            
        });         
            
    },
    
    /* Плагин для создания галерей
    -------------------------------------------------------------------------- */
    
    this.Gallery = function(){
        
        
        var ppGalleries = {
            
            photo_gallery: {
                name: 'gallery-classic',
                class: 'b-gallery-classic',
                title: 'Фото-классик',
                gallery_height: true,
                gallery_width: true,
                image_width: true,
                image_title: {
                    type: 'textarea',
                },
                image_desc: false,
            },
            photo_mini: {
                
                name: 'gallery-mini',
                class: 'b-gallery-mini',
                title: 'Фото-мини',
                gallery_height: true,
                gallery_width: true,
                image_width: false,
                image_title: {
                    type: 'textarea',
                },
                image_desc: false,
                
            },
            sweet_thumbnails: {
                name: 'gallery-sweetthumbs',
                title: 'Фото-sweet',
                class: 'b-gallery-sweetthumbs',
                gallery_height: true,
                gallery_width: true,
                image_width: true,
                image_title: {
                    type: 'textarea',
                },
                image_desc: false,
            },
            gallery_photo_text: {
                name: 'gallery-phototext',
                title: 'Фото+Текст',
                class: 'b-gallery-phototext',
                gallery_height: false,
                gallery_width: true,
                image_width: true,
                image_border: true,
                image_title: {
                    type: 'textarea',
                },
                image_desc: true,
                image_crop: true
            },
            
                
        }
        
        //----------------------------------------------------------------------------//

        var new_element = false;
        var element = this.getSelectionElement({data : 'gallery'}); //Получение ссылки на выделенный элемент.
        if(!element){
            new_element = true;
            element = CKEDITOR.dom.element.createFromHtml('<div data-gallery="" data-gallery-id="" ></div>'); 
        }
        
        //----------------------------------------------------------------------------//
        
        var $element        = $(element.$),
            $w_content      = $('<div class="b-modal-window gallery">' +
                                '<h2>Галерея</h2>' +
                                '<div class="gallery__settings">' +
                                    '<label class="form__label">Тип галереи: <select name="gallery_type" class="form__select"></select></label>' +
                                '</div>' +
                                '<h3>Изображения <a class="gallery__add-item">Добавить</a></h3>' + 
                                '<ul class="gallery__items"></ul>' +
                                '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>');
        

        var current_gallery = (isEmpty(ppGalleries[$element.attr('data-gallery')])) ? ppGalleries['photo_gallery'] : ppGalleries[$element.attr('data-gallery')];  
    
        var gallery_is_empty = (!$element.find('img').length) ? true : false;
        
        //----------------------------------------------------------------------------//
                                
        $.each(ppGalleries, function(gallery_name, gallery_params){
            $w_content.find('[name="gallery_type"]').append('<option value="' + gallery_name + '">' + gallery_params.title + '</option>')
        });
        $w_content.find('[name="gallery_type"]').val(current_gallery.name);
        
        //----------------------------------------------------------------------------//
            
        if(current_gallery.gallery_width){
            $w_content.find('.gallery__settings').append('<label class="form__label">Ширина галереи: <input name="gallery_width" class="form__input"/></label>'); 
        }
        if(current_gallery.gallery_height){
            $w_content.find('.gallery__settings').append('<label class="form__label">Высота галереи: <input name="gallery_height" class="form__input" /></label>');    
        }
        if(current_gallery.image_width){
            $w_content.find('.gallery__settings').append('<label class="form__label">Ширина изображения: <input name="image_width" class="form__input" /></label>');   
        }
        if(current_gallery.image_crop){
            $w_content.find('.gallery__settings').append('<label class="form__label"><input type="checkbox" name="image_crop"/> Обрезать</label>');    
        }
        if(current_gallery.image_border){
            $w_content.find('.gallery__settings')
                .append('<label class="form__label">Рамка: <input name="image_border" style="width:30px;" /> Цвет: <input name="image_border_color" data-colorpicker style="width:62px;" maxlenght="7" /></label>');
                
        }

        var _this = this;
        
        this.showWindow($w_content, function(){
        
            var $modal = $('.b-modal-window');

            var addGalleryImage = function(src, title, desc) {

                var $image = $('<li><a>удалить</a><div><img /></div></li>');

                if(current_gallery.image_title) {
                    if(current_gallery.image_title.type == 'textarea') {
                        $image.append('<textarea name="title" placeholder="Название"></textarea>');
                    } else {
                        $image.append('<input type="text" placeholder="Название" name="title" />'); 
                    }    
                }

                if(current_gallery.image_desc) {
                    $image.append('<textarea name="desc" placeholder="Описание"></textarea>');  
                }

                $image
                    .find('img')
                        .attr('src', src)
                    .end()
                    .find('[name="title"]')
                        .val(title)
                    .end()
                    .find('[name="desc"]')
                        .val(desc)
                    .end()
                    .find('a')
                        .click(function() {
                            $(this).parent('li').remove();
                            $.fancybox.update();
                        });
                $('.gallery__items').append($image);

                $.fancybox.update();

            }
            
            if(!isEmpty(element.getAttribute('img-width')) && current_gallery.image_width){
                $modal.find('[name="image_width"]').val(element.getAttribute('img-width'));
            }

            if(!isEmpty(element.getAttribute('img-crop')) && element.getAttribute('img-crop') == "true" && current_gallery.image_crop){
                $modal.find('[name="image_crop"]').attr('checked', 'checked');
            }

            if(!isEmpty(element.getStyle('height')) && current_gallery.gallery_height){
                $modal.find('[name="gallery_height"]').val(element.getStyle('height'));
            }

            if(!isEmpty(element.getStyle('width')) && current_gallery.gallery_width){
                $modal.find('[name="gallery_width"]').val(element.getStyle('width'));
            }
            
            
            if(current_gallery.image_border){
            
                if(!gallery_is_empty && !isEmpty(element.getFirst().getStyle('border-width'))){
                    
                    $modal.find('[name="image_border"]').val(element.getFirst().getStyle('border-width'));
                    $modal.find('[name="image_border_color"]').val(rgb2hex(element.getFirst().getStyle('border-color')));   
                
                }            
            }

            //Событие смены типа галереи
            $('[name="gallery_type"]').change(function() {
                
                $modal.find('.modal-ok').click();
                _this.Gallery();

            });
            
            $element.find('img').each(function(){

                var title = (!isEmpty($(this).attr('data-gallery-img-title'))) ? $(this).attr('data-gallery-img-title') : '';
                var desc = (!isEmpty($(this).attr('data-gallery-img-desc'))) ? $(this).attr('data-gallery-img-desc') : '';
                
                addGalleryImage(this.src, title, desc);
                
            });
            
            
            $modal.find('.gallery__add-item').click(function(){
                
                FileManager.open({ 
                    callback: function(file) {
                        addGalleryImage(file.url, '', '');
                    }
                });
                
            });

            $('.gallery__items').dragsort({ dragSelector: "li", dragBetween: false});
            
            
        }, function(){
            

            var $modal = $('.b-modal-window');

            $element.empty().removeAttr('style');
            
            //Установка типа галереи

            var gallery_type = $modal.find('[name="gallery_type"]').val(),
                current_gallery = ppGalleries[gallery_type];
                
            $element.attr('data-gallery', gallery_type).removeAttr('class').addClass(current_gallery.class);
            
            //Обработка ширины и высоты
            
            var gallery_width = $modal.find('[name="gallery_width"]').val();
            if(current_gallery.gallery_width && !isEmpty(gallery_width)){
                $element.css('width', gallery_width);
            } else {
                $element.css('width', '');  
            }
            
            var gallery_height = $modal.find('[name="gallery_height"]').val();
            if(current_gallery.gallery_height && !isEmpty(gallery_height)){
                $element.css('height', gallery_height);
            } else {
                $element.css('height', '');
            }

            //Наполнение галереи изображениями
            
            $modal.find('.gallery__items img').each(function(){

                var image_title = $(this).parent().parent().find('[name="title"]').val();
                var image_desc = $(this).parent().parent().find('[name="desc"]').val();
                image_title = (current_gallery.image_title && !isEmpty(image_title)) ? image_title : '';
                image_desc = (current_gallery.image_desc && !isEmpty(image_desc)) ? image_desc : '';    
                    
                $element.append('<img src="' + urlToRelative(this.src) + '" data-gallery-img-title="' + image_title + '" data-gallery-img-desc="' + image_desc + '" />');
                
            });
            
            //Установка ширины изображений
            
            var image_width = $modal.find('input[name="image_width"]').val();
            if (current_gallery.image_width && image_width) {       
                $element.attr('img-width', image_width);
            } else {
                $element.removeAttr('img-width');
            }

            if ($modal.find('[name="image_crop"]').is(':checked') && image_width) {
                $element.attr('img-crop', true);
            } else {
                $element.removeAttr('img-crop');
            }
            
            
            //Установка рамки
            
            var image_border = $modal.find('input[name="image_border"]').val();
            if(current_gallery.image_border && !isEmpty(image_border)){ 
            
                var image_border_color  = $modal.find('input[name="image_border_color"]').val();
                var image_border        = (isNumeric(image_border)) ? image_border + 'px' : image_border;
                $element.find('img').css('border', image_border + ' solid ' + image_border_color);
                
            } else {
                $element.find('img').css('border', '');
            }
            
            //Установка уникального индефикатора галереи
            $element.attr('data-gallery-id', gallery_type + new Date().getTime());
            
            if(new_element){
                _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
                _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
                _this.ckeditor().getSelection().selectElement(element);
            }
            
            
        }, { minWidth : 'auto'});
        
    },

    
    /* Плагин для создания спойлера "подробнее"
    -------------------------------------------------------------------------- */
    
    this.ButtonMore = function(){
    
        this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<div class="spoiler">Контент</div>'));
        this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp</p>'));
        return false;
        
    },

    /* Плагин для создания кнопки 
    -------------------------------------------------------------------------- */
    
    this.BlogMore = function(){
    
        this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>###more###</p>'));
        return false;
        
    },

    /* Плагин для вставки youtube-видео
    -------------------------------------------------------------------------- */

    this.Youtube = function(){
    
        this.ckeditor().openDialog('youtube');
        return false;
        
    },
     
    /* Плагин для создания разделителя на странице
    -------------------------------------------------------------------------- */
     
    this.Delimiter = function(){
        
        var element = CKEDITOR.dom.element.createFromHtml('<div></div>');   

        var w_content = '<div class="b-modal-window"><h2>Разделитель</h2>';
      
        //Выбор типа разделителя
        w_content += '<select name="type_line" style="width:100%;">' +
                     '<option value="empty_line">Пустой разделитель</option>' + 
                     '<option value="border_line">Разделитель в виде линии</option></select>';
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, null , function(){
            

            var $modal = $('.b-modal-window');
            
            //Обработка типа разделителя
            if($modal.find('select[name="type_line"]').val() == 'empty_line'){
                
                element.addClass('line');   
                
            }else if($modal.find('select[name="type_line"]').val() == 'border_line'){
                
                element.addClass('line-border');
                
            }
            
            _this.ckeditor().insertElement(element);//Вставка созданного элемента в CKEDITOR
            _this.ckeditor().insertElement(CKEDITOR.dom.element.createFromHtml('<p>&nbsp;</p>'));
            
        });
        
    },
    
    /* Плагин фичи
    -------------------------------------------------------------------------- */

    this.Features = function(){
        
        var element = this.getSelectionElement(); //Получение ссылки на выделенный элемент.
        if(!element){ alert('Вы должны выделить элемент!'); return false; }
        
        var $element = $(element.$); //jquery объект

        var w_content = '<div class="b-modal-window"><h2>Фичи</h2>';
        
        w_content += '<div class="features">';
        
        /* Список фич
        ---------------------------------------------------------------------- */

        w_content += '<select name="quotes" style="width: 100%;">' + 
                        '<option>Не выбрано</option>' +
                        '<option data-class="quotes">Цитаты слева</option>' +
                        '<option data-class="quotes_right">Цитаты справа</option>' +
                     '</select>';

        /* ------------------------------------------------------------------- */
        
        w_content += '<div class="b-modal-window-footer"><a class="modal-ok btn--md btn--success">OK</a>&nbsp;<a class="modal-cancel btn--md btn--default">Отмена</a></div></div>';
        
        var _this = this;
        this.showWindow(w_content, function(){
        
            var $modal = $('.b-modal-window');

            $modal.find('select option').each(function(){
                
                var $this = $(this);

                if($element.hasClass($(this).data('class')) == true){
                    this.selected = true;
                }
                
            });
            
            
        }, function(){
            
            if(CKPlugins.w_cancel) return true;

            var $modal = $('.b-modal-window');

            $modal.find('select option').each(function(){
                
                if($(this).attr('selected')) {
                    $element.addClass($(this).data('class'));
                } else {
                    $element.removeClass($(this).data('class'));
                }
                
            });
            
        });
        
    }
    
}]);