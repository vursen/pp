'use strict';

app.classy.controller({
    name: 'CKPluginsCtrl',

    inject: ['$scope', 'flash', 'CKPlugins'],

    init: function() {

        

    },

    plugin: function(name) {

        return this.CKPlugins[name]();

    }
});