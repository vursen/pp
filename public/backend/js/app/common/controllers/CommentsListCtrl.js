'use strict';

app.classy.controller({
    name: 'CommentsListCtrl',

    inject: ['$scope', '$routeParams', 'config', 'Comments', 'flash'],

    init: function() {

        /*if (this.$routeParams.postId) {

            this.$.comments = this.Comments.query({ post_id: this.$routeParams.postId });

        } else if (this.$routeParams.page_id) {

            this.$.comments = this.Comments.query({ page_id: this.$routeParams.pageId });

        } else if (this.$routeParams.product_id) {

            this.$.comments = this.Comments.query({ product_id: this.$routeParams.productId });
            
        }*/

    },

    setComments: function(comments) {

        this.$.comments = comments;

    },

    delete: function($index, comment) {

        var _this = this;
        
        if (!confirm('Вы действительно хотите удалить данный комментарий?')) {
            return false;
        }

        this.Comments.delete({}, { 'id': comment.id }, function(response) {

            _this.$.comments.splice($index, 1);
            _this.flash('Комментарий удален.');

        });

    }
});