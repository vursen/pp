'use strict';

app.classy.controller({

    name: 'MainCtrl',

    inject: ['$scope', '$location', 'config'],

    init: function() {
        
        
    },

    itemIsActive: function(path) {

        if (path == '/') {

            return (this.$location.path() == '/') ? 'active' : '';

        } else {

            return (this.$location.path().substr(0, path.length) == path) ? 'active' : '';

        }

    }

});