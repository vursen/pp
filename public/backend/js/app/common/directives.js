app.directive('ckEditor', [function () {
    return {
        require: '?ngModel',
        restrict: 'AEC',
        scope: {

        },
        link: function (scope, elm, attr, ngModel) {
            
            var ck = CKEDITOR.replace(elm[0]);

            if (!ngModel) return;

            ck.on('instanceReady', function() {
                /*scope.$watch(attr.ngModel, function(newValue, oldValue) {
                    if (oldValue == undefined) {
                        ck.setData(newValue);
                    }
                });*/

                ngModel.$render = function (value) {
                    if (ngModel.$viewValue != undefined) {
                        ck.setData(ngModel.$viewValue);
                    }
                };
                
            });

            ck.on('pasteState', updateModel);
            ck.on('change', updateModel);

            function updateModel() {
                scope.$apply(function() {
                    ngModel.$setViewValue(ck.getData());
                });
            }

            /*ck.on('change', updateModel);
            ck.on('blur', updateModel);*/

            elm.bind('$destroy', function () {
                ck.destroy(false);
            });   

        }
    };
}]);

app.directive('fileManager', ['FileManager', function (FileManager) {
    return {
        restrict: 'AEC',
        require: '?ngModel',
        link: function($scope, $elm, $attr, $model) {

            $elm.on('click', function() {

                if ($attr.ngModel) {

                    FileManager.open({
                        callback: function(file) {
                            $model.$setViewValue(file.url);
                            $model.$render();
                        }
                    });

                } else {

                    FileManager.open();

                }
                
            });

        }
    };
}]);


app.directive('stickyPanel', function () {
    return {
        restrict: 'AEC',
        link: function($scope, $elm, $attr, $model) {
           /* $elm.pin({
                  containerSelector: ".b-content",
                  activeClass: 'fixed'
            });*/
        }
    };
});

/*
app
.directive('tabs', function () {
    return {
        require: '',
        restrict: 'A',
        transclude: true,
        template: '<div class="tabs">' +
                    '<ul class="tabs__menu">' +
                        '<li ng-repeat="item in tabsItems" ng-class="{ active: item.selected }" class="tabs__menu__item">' + 
                            '<a href="" ng-click="select(item)">[[ item.title ]]</a>' +
                        '</li>' + 
                    '</ul>' +
                    '<div class="tabs__content" ng-transclude></div>' +
                  '</div>',

        link: function($scope) {
            console.log($scope);
        }
    };
})
.directive('tabsItem', function() {
    return {
        require: '',
        restrict: 'A',
        transclude: true,
        template: '<div class="tabs__item" ng-show="selected" ng-transclude></div>',
        link: function($scope, $element, $attrs) {
            console.log($scope);
        }
    };
});*/