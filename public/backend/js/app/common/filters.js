app.filter('lang', ['config', function (config) {

    return function(lang) {
        return config.langs[lang];
    };

}]);

app.filter('frontendUrl', ['config', function (config) {
    return function(url, lang) {
        
        var baseUrl = config.frontendUrl.replace(new RegExp('(http://[^/]+)'), '');

        if (lang) {
            return [baseUrl, lang, url].join('/');
        } else {
            return [baseUrl, url].join('/');
        }

    };
}]);

app.filter('backendAssetsUrl', ['config', function (config) {
    return function(url) {

        return [config.backendAssetsUrl.replace(new RegExp('(http://[^/]+)'), ''), url].join('/');

    };
}]);