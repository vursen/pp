$(document).ready(function() {

    $('.content').fitVids();

    /* AJAX форма
    ---------------------------------------------------- */

    $('.js-ajax-form').ajaxForm({

        dataType: 'json',
        success: function(response) {

            if (response.message) {
                alert(response.message);
            }
            if (response.redirect) {
                document.location.href = response.redirect;
            }
            if (response.reload) {
                document.location.reload();
            }

        }

    });

    $('.back-link').click(function() {
        document.history.back();
        return false;
    });
        
    
    /* Подсветка меню
    ---------------------------------------------------- */

    /*var path = document.location.pathname;
    $('.pp-menu').find('a').each(function(){
        
        var re = new RegExp('^' + $(this).attr('href') + '$');
        if(re.test(path)){
            
            $(this).addClass('current');
            
        }else if(re.test(path.replace(/^\//, '')) ){
            
            $(this).addClass('current'); 
            
        }
        
    });*/
    
    

    /* Цитаты
    ---------------------------------------------------- */

    $bMainQuote = $('.b-main__quote p:not(:empty)');
    $bMainQuote
            .parent()
            .empty()
            .append($bMainQuote.get(Math.floor(Math.random() * $bMainQuote.length)));

    /* Счетчик
    ---------------------------------------------------- */

    $('.countdown').countdown({
        until: new Date(2014, 12 - 1, 6),
        format: 'd'
    });

    /* Ajax json
    ---------------------------------------------------- */

    $.oldAjax = $.ajax;
 
    $.ajax = function () {
        var args = Array.prototype.slice.call(arguments);
 
        var jqxhr = $.oldAjax.apply(this, args);
 
        jqxhr.fail(function () {
            if (/application\/json/.test(jqxhr.getResponseHeader('Content-Type'))) {
                try {
                    var errors = JSON.parse(jqxhr.responseText);
                    if (errors.error)
                        alert(errors.error);
                } catch(e) {

                }
            }
        });
 
        return jqxhr;
    };
    
});

/* Автоопределение высоты для iframe
------------------------------------------------------- */

function frame_load(frame){
    
    if(frame.height == '' || frame.height == 'auto'){

        var h = $(frame).contents().find('html').prop('scrollHeight');
        //alert($(frame).contents().find('html').prop('scrollHeight'));
        if(h == 0 || h == undefined){
            h = $(frame).contents().find('body').prop('scrollHeight');  
        }
        frame.height = Math.round(h);
    
    }
}


/* Эффект expando
   Expando Image Script ©2008 John Davenport Scheuer
------------------------------------------------------- */

document.images&&!function(){var t,e=/Apple/.test(navigator.vendor),i=e?10:20,m=e?10:5,s=function(e){if(e=e||window.event,s.r.test(e.className)||(e=e.target||e.srcElement||null),e&&s.r.test(e.className)){var n=s,o=function(e){return e[0]*t+e[1]+"px"},u=function(){t=(1-Math.cos(n.ims[r].jump/i*Math.PI))/2,e.style.width=o(n.ims[r].w),e.style.height=o(n.ims[r].h),n.ims[r].d&&i>n.ims[r].jump?(++n.ims[r].jump,n.ims[r].timer=setTimeout(u,m)):!n.ims[r].d&&n.ims[r].jump>0&&(--n.ims[r].jump,n.ims[r].timer=setTimeout(u,m))},d=document.images,r=d.length-1;for(r;r>-1&&d[r]!=e;--r);if(r+=e.src,!n.ims[r])return e.title="",n.ims[r]={im:new Image,jump:0},n.ims[r].im.onload=function(){n.ims[r].w=[n.ims[r].im.width-e.width,e.width],n.ims[r].h=[n.ims[r].im.height-e.height,e.height],n(e)},void(n.ims[r].im.src=e.src);n.ims[r].timer&&clearTimeout(n.ims[r].timer),n.ims[r].d=!n.ims[r].d,u()}};s.ims={},s.r=new RegExp("\\bexpando\\b"),document.addEventListener?(document.addEventListener("mouseover",s,!1),document.addEventListener("mouseout",s,!1)):document.attachEvent&&(document.attachEvent("onmouseover",s),document.attachEvent("onmouseout",s))}();