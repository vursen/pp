$(document).ready(function() {
    
    /* Большое меню
    ---------------------------------------------------- */

    var $topMenuBig = $('.b-top-menu__big');
    $topMenuBig.find('li').on('mouseenter', function() {

        var $subMenu = $(this).find('ul');

        if (!$subMenu.is(':visible'))
            $subMenu.slideDown(300); // Открываем

    });

    $topMenuBig.find('li').on('mouseleave', function() {

        var $subMenu = $(this).find('ul');

        if ($subMenu.is(':visible'))
            $subMenu.slideUp(300); // Закрываем

    });

    /* Маленькое меню
    ---------------------------------------------------- */

    var $topMenuSmall = $('.b-top-menu__small').html($topMenuBig.html()).children('ul');
    $topMenuSmall.find('li').click(function(e) {

        $topMenuSmall.find('li ul').slideUp(300); // Скрываем все подменю

        $subMenu = $(this).find('ul');

        if (!$subMenu.is(':visible')) { // Если подменю не открыто

            $subMenu.slideDown(300); // Открываем

        }

        e.preventDefault();

    });

    /* Кнопка свернуть/развернуть
    ---------------------------------------------------- */

   $('.b-top-menu__pull').click(function(e) {

        if (!$topMenuSmall.is(':visible')) { // Если меню не открыто

            $topMenuSmall.slideDown(300, function() {

                $topMenuSmall.show();

            });

        } else {

            $topMenuSmall.slideUp(300, function() {

                $topMenuSmall.hide();

            });

        }

    });

});