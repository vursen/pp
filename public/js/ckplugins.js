$(document).ready(function() {

    /* Спойлер далее
    ------------------------------------------------------------------------- */
    
    $('.ck-spoiler-more').each(function() {

        var $spoiler = $(this);
        $spoiler
            .wrapInner('<div class="ck-spoiler-more__content"></div>')
            .find('.ck-spoiler-more__content')
            .after('<div class="ck-spoiler-more__toggle open">Далее</div>');

        $spoiler.find('.ck-spoiler-more__toggle').toggle(function() {

            $spoiler.find('.ck-spoiler-more__content').slideDown();
            $(this)
                .removeClass('open')
                .addClass('close')
                .text('Скрыть'); 

        }, function() {

            $spoiler.find('.ck-spoiler-more__content').slideUp();
            $(this)
                .removeClass('close')
                .addClass('open')
                .text('Далее');

        });
    });

    /* Спойлер
    ------------------------------------------------------------------------- */
    
    $('.ck-spoiler').click(function() {
        
        $(this).find('.ck-spoiler__content').toggle('slow');
        
    });

    /* Аудиоплеер
    ------------------------------------------------------------------------- */

    $('.audiopleer').each(function(){
        
        var audio_url = $(this).data('audio-url');

        $(this).html('<object type="application/x-shockwave-flash" data="/audio_player.swf" width="450" height="33">' + 
                     '<param name="movie" value="/audio_player.swf">' +
                     '<param name="FlashVars" value="mp3=' + audio_url +'&amp;width=450&amp;height=33&amp;loop=0&amp;autoplay=0&amp;volume=30&amp;showstop=1&amp;showinfo=0&amp;showvolume=1&amp;showslider=1&amp;showloading=always&amp;buttonwidth=35&amp;sliderwidth=15&amp;sliderheight=15&amp;volumewidth=70&amp;volumeheight=25&amp;loadingcolor=FFFFFF&amp;bgcolor1=7F7F7F&amp;bgcolor2=000000&amp;slidercolor1=00CCFF&amp;slidercolor2=0000FF&amp;sliderovercolor=FF6820&amp;buttoncolor=00FF00&amp;buttonovercolor=FFFFFF&amp;textcolor=FFFFFF">' +
                     '<param name="wmode" value="transparent">' +
                     '</object>');
        
    });    
    
    /* Галерея "Фото-классик"
    ------------------------------------------------------------------------- */

    $('.ck-gallery-classic').each(function() {
        var $gallery = $(this);

        $gallery.find('img').each(function() {
            var $this = $(this);
            $this.attr('alt', $this.data('title'));
        });

        $gallery.galleria();
    });
    
    /* Галерея "Фото-мини"
    ------------------------------------------------------------------------- */
    
    $('.ck-gallery-mini').each(function() {
        var $gallery   = $(this),
            galleryId  = $gallery.data('ck-gallery-id');

        $gallery.find('img').each(function() {
            
            var $this = $(this),
                src   = $(this).attr('src'),
                title = $(this).data('title') || '';

            $this.attr('alt', title).wrapAll('<a href="' + src + '" rel="' + galleryId + '" title="' + title + '"></a>');

            $this.attr('src', '/image/?size=135x135&path='+ src).bind('load.thumb', function() {
                $(this).unbind('load.thumb').thumbs();
            });
            
        });

        $gallery.find('a').fancybox();
    });
    
    /* Галерея "Фото+Текст"
    ------------------------------------------------------------------------- */

    $('.ck-gallery-phototext').each(function() {

        var $gallery   = $(this),
            galleryId  = $gallery.data('ck-gallery-id'),
            imageWidth = $gallery.data('image-width'),
            imageCrop  = $gallery.data('image-crop');

        $gallery.find('img').each(function() {

            var $this = $(this),
                src   = $(this).attr('src'),
                title = $(this).data('title') || '',
                desc  = $(this).data('desc') || '';

            if (imageWidth) {
                imageCrop ? $this.attr('src', '/image/?size=' + [parseInt(imageWidth), parseInt(imageWidth)].join('x') + '&path=' + src) : $this.width(imageWidth);
            }

            $this
                .attr('alt', title)
                .wrapAll('<a href="' + src + '" rel="' + galleryId + '" title="' + title + '"></a>')
                .parent('a')
                .wrapAll('<div class="ck-gallery-phototext__item"></div>')
                .parent('.ck-gallery-phototext__item')
                .append('<div class="ck-gallery-phototext__item__title">' + title + '</div>')
                .append('<div class="ck-gallery-phototext__item__desc">' + desc.toString().replace(/\r\n|\r|\n/g,'<br />') + '</div>');
        });

        $gallery.find('a').fancybox();

    });


    /* Колонки
    ------------------------------------------------------------------------- */

    $(window).resize(function(){

        $('.ck-columns').each(function(){
            
            $(this)
                .css('height', 'auto')
                .find('.ck-columns__item')
                    .css('height', 'auto')
                    .find('.ck-columns__item__content')
                        .css('height', 'auto');

            var max_height = $(this).height() + 30;

            $(this).find('.ck-columns__item__content').each(function(){
                $(this).height(max_height);
            });
            
        });
        
    }).resize();

    /* Кнопки
    ------------------------------------------------------------------------- */
    
    $('.ck-btn').each(function() {
        var $btn = $(this);

        $btn.fadeTo(0, $btn.data('state-default-opacity') / 100);

        $btn.hover(function() {   
            
            if ($btn.data('state-hover-img')) {
                $btn.css('background-image', 'url(' + $btn.data('state-hover-img') + ')');
            }

            $btn.fadeTo(50, $btn.data('state-hover-opacity') / 100);
        
        }, function() {
            
            if ($btn.data('state-default-img')) {
                $btn.css('background-image', 'url(' + $btn.data('state-default-img') + ')');
            }

            $btn.fadeTo(50, $btn.data('state-default-opacity') / 100);
            
        });
    });
    
    /* Фото-эффекты
    ------------------------------------------------------------------------- */
    
    $('img').each(function(){

        if($(this).hasClass('image-effect-magnify')){
            
            $(this).addClass("magnify").css('cursor', 'pointer');
                
        }
        if($(this).hasClass('image-effect-expando')){
            
            $(this).addClass("expando").css('cursor', 'pointer');
            
        }
        if($(this).hasClass('image-effect-fancybox')){
            
            $(this).wrapAll('<a href="' + this.src + '" title="' + this.alt + '"></a>')
                   .css('cursor', 'pointer')
                   .parent('a')
                   .attr('data-fancy-link', true);
            
        }
        if($(this).hasClass('image-effect-fancybox-auto')){
            
            $(this).wrapAll('<a href="' + this.src + '" rel="f_gallery" title="' + this.alt + '"></a>')
                   .css('cursor', 'pointer')
                   .parent('a')
                   .attr('data-fancy-link', true);
            
        }
        if($(this).hasClass('image-effect-fancybox-frame')){
            
            $(this).wrapAll('<a href="' + $(this).attr('data-fancy-link') + '" ></a>')
                   .css('cursor', 'pointer')
                   .parent('a')
                   .attr('data-fancy-iframe', true);
        }
            
    });

    $('a[data-fancy-iframe=true]').fancybox({type: 'iframe'});
    $('a[data-fancy-link=true]').fancybox();

});