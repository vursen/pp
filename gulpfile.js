/* Require modules
----------------------------------------------------------------------------------------- */

var gulp   = require('gulp');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var prefix = require('gulp-autoprefixer');
var nib    = require('nib');

/* Backend Styles task
----------------------------------------------------------------------------------------- */

gulp.task('BackendStyles', function() {
    gulp.src('public/backend/stylus/main.styl')
        .pipe(stylus({
            use: [nib()]
        }))
        //.pipe(prefix("last 1 version", "> 1%", "ie 8", { cascade: true }))
        .on('error', console.log)
        .pipe(gulp.dest('public/backend/css/'))
});

/* Backend AngularApp task
----------------------------------------------------------------------------------------- */

gulp.task('BackendAngularApp', function() {
    gulp.src('public/backend/js/app/**/*.js')
        .pipe(concat('app.min.js'))
        .on('error', console.log)
        .pipe(gulp.dest('public/backend/js'))
});

/* Backend AngularVendor task
----------------------------------------------------------------------------------------- */

gulp.task('BackendAngularVendor', function() {
   
    gulp.src('public/backend/js/vendor/angular/**/*.js'/*, '!backend/js/vendor/angular/angular-core.min.js'*/)
        .pipe(concat('angular-modules.min.js'))
        .pipe(uglify())
        .on('error', console.log)
        .pipe(gulp.dest('public/backend/js/vendor'));

});

/* Frontend styles task
----------------------------------------------------------------------------------------- */

gulp.task('FrontendStyles', function() {
   
    gulp.src(['public/css/*.css', '!public/css/main.min.css'])
        .pipe(concat('main.min.css'))
        .pipe(prefix("last 1 version", "> 1%", "ie 8", { cascade: true }))
        .on('error', console.log)
        .pipe(gulp.dest('public/css/'));

});

/* Watch task
----------------------------------------------------------------------------------------- */

gulp.task('watch', ['BackendAngularApp', 'BackendAngularVendor', 'BackendStyles'], function() {

    gulp.watch('public/backend/stylus/**/*.styl', ['BackendStyles']);

    gulp.watch('public/backend/js/app/**/*.js', ['BackendAngularApp']);

    gulp.watch('public/backend/js/vendor/angular/**/*.js', ['BackendAngularVendor']);

    //gulp.watch('css/*.css', ['FrontendStyles']);

});