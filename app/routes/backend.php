<?php
/*
|--------------------------------------------------------------------------
| Backend
|--------------------------------------------------------------------------
*/

Route::group(array(
    'prefix'    => Setting::get('backend.path'), 
    'before'    => 'admin.auth',
), function() {

    Route::get('/', array('as' => 'backend.index', function() 
    { 

        return View::make('backend.index');

    }));

    /*
    |---------------------------------------------------------------------
    | Блог
    |---------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'blog'), function() 
    {

        /* Список категорий
        --------------------------------------------------------------- */

        Route::get('/categories', array(
            'as'   => 'backend.blog.categories', 
            'uses' => 'Backend\Blog\CategoriesController@index'
        ));

        /* Редактирование категории
        --------------------------------------------------------------- */

        Route::get('/categories/{categoryId}', array(
            'as'   => 'backend.blog.categories.edit', 
            'uses' => 'Backend\Blog\CategoriesController@edit'
        ));

        /* Сохранение категории
        --------------------------------------------------------------- */

        Route::post('/categories/{categoryId?}', array(
            'as'   => 'backend.blog.categories.save',
            'uses' => 'Backend\Blog\CategoriesController@save'
        ));
        
        /* Удаление категории
        --------------------------------------------------------------- */

        Route::delete('/categories/{categoryId}', array(
            'as'   => 'backend.blog.categories.delete',
            'uses' => 'Backend\Blog\CategoriesController@delete'
        ));

        /* Список записей
        --------------------------------------------------------------- */

        Route::get('/posts', array(
            'as' => 'backend.blog.posts', 
            'uses' => 'Backend\Blog\PostsController@index'
        ));

        /* Редактирование записи
        --------------------------------------------------------------- */

        Route::get('/posts/{postId}', array(
            'as'   => 'backend.blog.posts.edit', 
            'uses' => 'Backend\Blog\PostsController@edit'
        ));

        /* Сохранение записи
        ------------------------------------------------------------------ */

        Route::post('/posts/{postId?}', array(
            'as'   => 'backend.blog.posts.save', 
            'uses' => 'Backend\Blog\PostsController@save'
        ));

        /* Удаление записи
        --------------------------------------------------------------- */

        Route::delete('/posts/{postId}', array(
            'as'   => 'backend.blog.posts.delete', 
            'uses' => 'Backend\Blog\PostsController@delete'
        ));

    });

    /*
    |---------------------------------------------------------------------
    | Магазин
    |---------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'store'), function() 
    {

        /* Список категорий
        --------------------------------------------------------------- */

        Route::get('/categories', array(
            'as'   => 'backend.store.categories', 
            'uses' => 'Backend\Store\CategoriesController@index'
        ));

        /* Редактирование категории
        --------------------------------------------------------------- */

        Route::get('/categories/{categoryId}', array(
            'as'   => 'backend.store.categories.edit', 
            'uses' => 'Backend\Store\CategoriesController@edit'
        ));

        /* Сохранение категории
        --------------------------------------------------------------- */

        Route::post('/categories/{categoryId?}', array(
            'as'   => 'backend.store.categories.save',
            'uses' => 'Backend\Store\CategoriesController@save'
        ));
        
        /* Удаление категории
        --------------------------------------------------------------- */

        Route::delete('/categories/{categoryId}', array(
            'as'   => 'backend.store.categories.delete',
            'uses' => 'Backend\Store\CategoriesController@delete'
        ));

        /* Список продуктов
        --------------------------------------------------------------- */

        Route::get('/products', array(
            'as' => 'backend.store.products', 
            'uses' => 'Backend\Store\ProductsController@index'
        ));

        /* Редактирование продукта
        --------------------------------------------------------------- */

        Route::get('/products/{productId}', array(
            'as'   => 'backend.store.products.edit', 
            'uses' => 'Backend\Store\ProductsController@edit'
        ));

        /* Сохранение продукта
        ------------------------------------------------------------------ */

        Route::post('/products/{productId?}', array(
            'as'   => 'backend.store.products.save', 
            'uses' => 'Backend\Store\ProductsController@save'
        ));

        /* Удаление продукта
        --------------------------------------------------------------- */

        Route::delete('/products/{productId}', array(
            'as'   => 'backend.store.products.delete', 
            'uses' => 'Backend\Store\ProductsController@delete'
        ));

    });

    /*
    |---------------------------------------------------------------------
    | Комментарии
    |---------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'comments'), function() 
    {

        Route::get('/', array(
            'as'   => 'backend.comments',
            'uses' => 'Backend\CommentsController@index'
        ));

        Route::delete('/{commentId}', array(
            'as'   => 'backend.comments.delete',
            'uses' => 'Backend\CommentsController@delete'
        ));

    });
    
    /*
    |---------------------------------------------------------------------
    | Форма обратной связи
    |---------------------------------------------------------------------
    */

    /*Route::get('/forms', array(
        'as'     => 'backend.forms',
        function() { return View::make('backend.forms.index'); }
    ));*/

    /*
    |---------------------------------------------------------------------
    | Настройки
    |---------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'settings'), function() 
    {

        /* Форма редактирования настроек
        --------------------------------------------------------------- */

        Route::get('/', array(
            'as' => 'backend.settings', 
            'uses' => 'Backend\SettingsController@index'
        ));

        /* Сохранение настроек
        --------------------------------------------------------------- */

        Route::post('/', array(
            'as'   => 'backend.settings.save',
            'uses' => 'Backend\SettingsController@save'
        ));

    });

    /*
    |--------------------------------------------------------------------
    | Страницы
    |--------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'pages'), function() 
    {

        /* Список страниц
        --------------------------------------------------------------- */

        Route::get('/', array(
            'as' => 'backend.pages', 
            'uses' => 'Backend\PagesController@index'
        ));

        /* Редактирование страницы
        --------------------------------------------------------------- */

        Route::get('/{pageId}', array(
            'as'   => 'backend.pages.edit', 
            'uses' => 'Backend\PagesController@edit'
        ));

        /* Сохранение страницы
        --------------------------------------------------------------- */

        Route::post('/{pageId?}', array(
            'as'   => 'backend.pages.save', 
            'uses' => 'Backend\PagesController@save'
        ));

        /* Удаление страницы
        --------------------------------------------------------------- */

        Route::delete('/{pageId}', array(
            'as'   => 'backend.pages.delete',
            'uses' => 'Backend\PagesController@delete'
        ));

        /* Форма создания страницы
        --------------------------------------------------------------- */

        Route::get('/create', array(
            'as'   => 'backend.pages.create', 
            'uses' => 'Backend\PagesController@create'
        ));

    });

    /*
    |---------------------------------------------------------------------
    | Резервные копии
    |---------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'backups'), function() 
    {

        /* Список бэкапов
        --------------------------------------------------------------- */

        Route::get('/', array(
            'as'   => 'backend.backups', 
            'uses' => 'Backend\BackupsController@index'
        ));

        /* Создание бэкапа
        --------------------------------------------------------------- */

        Route::get('/create', array(
            'as'   => 'backend.backups.create', 
            'uses' => 'Backend\BackupsController@create'
        ));

    });

    /*
    |--------------------------------------------------------------------
    | Шаблоны
    |--------------------------------------------------------------------
    */

    Route::group(array('prefix' => 'layouts'), function() 
    {

        /* Список шаблонов
        --------------------------------------------------------------- */

        Route::get('/', array(
            'as'   => 'backend.layouts', 
            'uses' => 'Backend\LayoutsController@index'
        ));

        /* Редактирование шаблона
        --------------------------------------------------------------- */

        Route::get('/{layoutId}', array(
            'as'   => 'backend.layouts.edit', 
            'uses' => 'Backend\LayoutsController@edit'
        ));

        /* Сохранение шаблона
        --------------------------------------------------------------- */

        Route::post('/{layoutId?}', array(
            'as'   => 'backend.layouts.save', 
            'uses' => 'Backend\LayoutsController@save'
        ));

        /* Удаление шаблона
        --------------------------------------------------------------- */

        Route::delete('/{layoutId}', array(
            'as'   => 'backend.layouts.delete', 
            'uses' => 'Backend\LayoutsController@delete'
        ));

    });

    /*
    |---------------------------------------------------------------------
    | ELFinder
    |---------------------------------------------------------------------
    */

    Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
    
    Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');

});

/*
|-------------------------------------------------------------------------
| Авторизация
|-------------------------------------------------------------------------
*/

Route::group(array(
    'prefix'    => Setting::get('backend.path') . '/auth',
    'namespace' => 'Backend'
), function() {

    /* Форма входа в систему
    ------------------------------------------------------------------ */

    Route::get('/', array(
        'as'   => 'backend.auth.login',
        'uses' => 'AuthController@index'
    ));

    /* Проверка данных для входа в систему
    ------------------------------------------------------------------ */

    Route::post('/login', array(
        'as'   => 'backend.auth.login.post',
        'uses' => 'AuthController@login'
    ));

    /* Выход из системы
    ------------------------------------------------------------------ */

    Route::get('/logout', array(
        'as'   => 'backend.auth.logout',
        'uses' => 'AuthController@logout'
    ));

});