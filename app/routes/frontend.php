<?php
/*
|-------------------------------------------------------------------------
| Frontend
|-------------------------------------------------------------------------
*/

Route::group(array('namespace' => 'Frontend'), function() use ($language) {

    Route::get('/image', array(
        'as'   => 'frontend.image', 
        'uses' => 'ImageController@getImage'
    ));

    Route::group(array('prefix' => $language), function() 
    {

        /*
        |---------------------------------------------------------------------
        | Страницы
        |---------------------------------------------------------------------
        */

        /* Главная страница 
        ------------------------------------------------------------------- */

        Route::get('/', array(
            'as'   => 'frontend.index', 
            'uses' => 'PagesController@index'
        ));

        /* Просмотр страницы
        ------------------------------------------------------------------- */

        Route::get('/{slug}.html', array(
            'as'   => 'frontend.pages.view', 
            'uses' => 'PagesController@view'
        ));

        /*
        |---------------------------------------------------------------------
        | Блог
        |---------------------------------------------------------------------
        */

        Route::group(array('prefix' => 'blog'), function() 
        {

            /* Список записей 
            --------------------------------------------------------------- */

            Route::get('/', array(
                'as'   => 'frontend.blog', 
                'uses' => 'Blog\PostsController@index'
            ));

            /* Список записей по категории
            --------------------------------------------------------------- */

            Route::get('/categories/{slug}', array(
                'as'   => 'frontend.blog.categories.view', 
                'uses' => 'Blog\PostsController@viewCategory'
            ));

            /* Список записей по тегу
            --------------------------------------------------------------- */

            Route::get('/tags/{slug}', array(
                'as'   => 'frontend.blog.tags.view', 
                'uses' => 'Blog\PostsController@viewTag'
            ));

            /* Просмотр записи 
            --------------------------------------------------------------- */

            Route::get('/posts/{slug}', array(
                'as'   => 'frontend.blog.posts.view', 
                'uses' => 'Blog\PostsController@viewPost'
            ));

            /* Поиск записи
            --------------------------------------------------------------- */

            Route::any('/search', array(
                'as'   => 'frontend.blog.search',
                'uses' => 'Blog\PostsController@search'
            ));

        });

        /*
        |---------------------------------------------------------------------
        | Магазин
        |---------------------------------------------------------------------
        */

        Route::group(array('prefix' => 'store'), function() 
        {

            /* Список продуктов 
            --------------------------------------------------------------- */

            Route::get('/', array(
                'as'   => 'frontend.store', 
                'uses' => 'Store\PostsController@index'
            ));

            /* Список продуктов по категории
            --------------------------------------------------------------- */

            Route::get('/categories/{slug}', array(
                'as'   => 'frontend.store.categories.view', 
                'uses' => 'Store\CategoriesController@view'
            ));

            /* Просмотр продукта 
            --------------------------------------------------------------- */

            Route::get('/products/{slug}', array(
                'as'   => 'frontend.store.products.view', 
                'uses' => 'Store\ProductsController@view'
            ));

        });

        /*
        |---------------------------------------------------------------------
        | Комментарии
        |---------------------------------------------------------------------
        */

        Route::post('/comments', array(
            'as'   => 'frontend.comments.save',
            'uses' => 'CommentsController@save'
        ));

        /*
        |---------------------------------------------------------------------
        | Контактная форма
        |---------------------------------------------------------------------
        */

        Route::group(array('prefix' => 'contacts'), function() 
        {

            Route::get('/', array(
                'as'   => 'frontend.contacts',
                'uses' => 'ContactsController@index'
            ));

            Route::post('/', array(
                'as'   => 'frontend.contacts.send',
                'uses' => 'ContactsController@send'
            ));

        });

    });
 
});