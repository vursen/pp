<?php
/*
|--------------------------------------------------------------------------
| Backend Layouts Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Request;

use Layouts;

class LayoutsController extends Controller {

	/**
	 * Список шаблонов
	 *
	 * @return Response
	 */

	public function index()
	{
		
		return Layouts::all();

	}

	/**
	 * Форма создания нового шаблона
	 *
	 * @return Response
	 */

	public function create()
	{

		return View::make('backend.layouts.create');

	}

	/**
	 * Форма редактирования шаблона
	 *
	 * @param  int  $layoutId
	 * @return Response
	 */

	public function edit($layoutId)
	{
		
		return Layouts::findOrFail($layoutId);

	}

	/**
	 * Создание или сохранение шаблона
	 *
	 * @param mixed $layoutId
	 * @return Response
	 */

	public function save($layoutId = null)
	{
		
		$layout       = $layoutId ? Layouts::findOrFail($layoutId) : new Layouts();
        $layout->name = Input::get('name');
        $layout->path = Input::get('path');
        
        if (!$layout->save()) {
            return Response::json(array('error' => $layout->errors->first()), 500);
        }

        return $layout;

	}

	
	/**
	 * Удаление шаблона
	 *
	 * @param  int  $layoutId
	 * @return Response
	 */
	
	public function delete($layoutId)
	{
		
		Layouts::findOrFail($layoutId)->delete();
        return Response::json(true);

	}

}