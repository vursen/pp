<?php
/*
|--------------------------------------------------------------------------
| Backend Settings Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Setting;
use Hash;
use Validator;
use URL;

class SettingsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Форма с настройками
     * 
     * @return Response
     */

    public function index()
    {


        return Response::json($this->getSettings());

    }

    /**
     * Сохранение настроек
     * 
     * @return Response
     */

    public function save()
    {

        $validator = Validator::make(Input::all(), array(
            'backend.path'        => 'required|regex:/^[a-zA-Z0-9\-\/_]+$/',
            'backend.password'    => 'min:4|max:20',
            'base.email'          => 'required|email',
            'blog.posts_per_page' => 'required|numeric'
        ), array(
            'backend.password.min'         => 'Пароль должен содержать не менее 4 символов.',
            'backend.password.max'         => 'Пароль должен содержать не более 20 символов.',
            'backend.path.required'        => 'Не заполнено поле "Путь к панели".',
            'backend.path.regex'           => 'Некорректно заполнено поле "Путь к панели".',
            'base.email.required'          => 'Не заполнено поле "Контактный E-mail".',
            'base.email.email'             => 'Некорректно заполнено поле "Контактный E-mail".',
            'blog.posts_per_page.required' => 'Не заполнено поле "Кол-во записей на странице"',
            'blog.posts_per_page.numeric'  => 'Некорректно заполнено поле "Кол-во записей на странице"',
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()->first()), 500);
        }

        /* Основные настройки 
        --------------------------------------------------------------------------------- */

        Setting::set('base.email', Input::get('base.email')); // Контактный E-mail

        /* Настройки панели
        ----------------------------------------------------------------------------------*/

        if (Input::has('backend.password')) {
            Setting::set('backend.password', Hash::make(Input::get('backend.password'))); // Пароль от панели
        }

        Setting::set('backend.path', trim(Input::get('backend.path'), '/')); // Путь к панели

        /* Настройки блога 
        --------------------------------------------------------------------------------- */

        Setting::set('blog.posts_per_page', Input::get('blog.posts_per_page')); // Кол-во записей на странице

        return Response::json($this->getSettings());

    }

    public function getSettings() {

        $settings = Setting::get();
        array_forget($settings, 'backend.password');

        return $settings;

    }

}