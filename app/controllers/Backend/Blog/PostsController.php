<?php
/*
|--------------------------------------------------------------------------
| Backend Blog posts Controller
|--------------------------------------------------------------------------
*/

namespace Backend\Blog;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Carbon\Carbon;
use Request;

use BlogPosts;
use BlogCategories;
use Tags;

class PostsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список записей
     * 
     * @return Response
     */

    public function index()
    {

        return BlogPosts::with('category')->get(array('id', 'title', 'added_on', 'category_id', 'slug'));

    }

    /**
     * Редактирование записи
     * 
     * @param int $postId
     * @return Response
     */

    public function edit($postId) 
    {

        return BlogPosts::with(array('category', 'tags', 'comments'))->findOrFail($postId);

    }

    /**
     * Создание или сохранение записи
     * 
     * @param mixed $postId
     * @return Response
     */

    public function save($postId = null)
    {

        /* Создание/изменение записи
        ------------------------------------------------------------------- */

        $post                    = $postId ? BlogPosts::findOrFail($postId) : new BlogPosts();

        $post->title             = Input::get('title');
        $post->content           = Input::get('content');

        $post->preview_img       = Input::get('preview_img');
        $post->preview_text      = Input::get('preview_text');

        $post->keywords          = Input::get('keywords');
        $post->description       = Input::get('description');

        $post->comments_disabled = (Input::get('comments_disabled') == 'yes') ? true : false;
        $post->category_id       = Input::get('category_id');

        if (Input::get('added_on')) {

            if (!preg_match('/^[0-9]{2}\.[0-9]{2}\.[0-9]{2}$/', Input::get('added_on'))) {
                return Response::json(array('error' => 'Не верно заполнено поле "Дата"'), 500);
            }

            $post->added_on = Carbon::createFromFormat('d.m.y', Input::get('added_on'))->toDateTimeString();

        } else {
            $post->added_on = Carbon::now()->toDateTimeString();
        }

        if (!$post->save()) {
            return Response::json(array('error' => $post->errors->first()), 500);
        }

        /* Добавление/удаление тегов
        ------------------------------------------------------------------- */

        $post->tags()->detach();

        if (Input::has('tags')) {

            foreach (Input::get('tags') as $tag) {

                $post->tags()->attach(Tags::firstOrCreate(array('name' => trim($tag['name']))));

            }
            
        }

        return $post->load('tags', 'category');
    }

    /**
     * Удаление записи
     * 
     * @param int $postId
     * @return Redirect
     */

    public function delete($postId)
    {

        BlogPosts::findOrFail($postId)->delete();
        return Response::json(true);

    }

}