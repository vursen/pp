<?php
/*
|--------------------------------------------------------------------------
| Backend Blog categories Controller
|--------------------------------------------------------------------------
*/

namespace Backend\Blog;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Localization;

use BlogCategories;
use Layouts;

class CategoriesController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список категорий 
     * 
     * @return Response
     */

    public function index()
    {

        return BlogCategories::all();

    }

    /**
     * Редактирования категории
     * 
     * @param int $categoryId
     * @return Response
     */

    public function edit($categoryId)
    {

        return BlogCategories::findOrFail($categoryId);

    }

    /**
     * Создание или сохранение категории
     * 
     * @param mixed $categoryId
     * @return Response
     */

    public function save($categoryId = null)
    {

        $category            = $categoryId ? BlogCategories::findOrFail($categoryId) : new BlogCategories();
        $category->name      = Input::get('name');
        $category->lang      = Input::get('lang');
        $category->layout_id = Input::get('layout_id');

        if (!$category->save()) {
            return Response::json(array('error' => $category->errors->first()), 500);
        }
        

        return $category;
    }

    /**
     * Удаление категории
     * 
     * @param int $categoryId
     * @return Redirect
     */

    public function delete($categoryId)
    {

        BlogCategories::findOrFail($categoryId)->delete();
        return Response::json(true);

    }

}