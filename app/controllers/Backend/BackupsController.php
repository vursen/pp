<?php
/*
|--------------------------------------------------------------------------
| Backend Backups Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Validator;
use URL;
use Carbon\Carbon;
use File;

class BackupsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список бэкапов
     * 
     * @return Response
     */

    public function index()
    {

        return View::make('backend.backups.index')->with(array(
            //'backups' => File::files(app_path() . '/backups')
        ));

    }

    /**
     * Создание бэкапа
     * 
     * @return Response
     */

    public function create()
    {

        $path = app_path() . '/backups/backup_' . Carbon::now()->timestamp . '.zip';

        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make($path)
               ->folder('database')
               ->add(app_path() . '/database/production.sqlite')
               ->close();

        return Response::download($path);

    }

}