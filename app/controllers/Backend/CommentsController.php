<?php
/*
|--------------------------------------------------------------------------
| Backend Сomments Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use Redirect;

use Comments;
use BlogPosts;

class CommentsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список комментарий
     * 
     * @param int $commentId
     * @return Response
     */

    public function index() 
    {

        if (Input::has('post_id')) {

            $comments = BlogPosts::with('comments')->findOrFail(Input::get('post_id'))->comments;

        }

        return $comments;

    }

    /**
     * Удаление комментария
     * 
     * @param int $commentId
     * @return Response
     */

    public function delete($commentId) 
    {

        Comments::findOrFail($commentId)->delete();
        return Response::json(true);

    }

}