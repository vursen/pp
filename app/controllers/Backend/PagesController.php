<?php
/*
|--------------------------------------------------------------------------
| Backend Pagess Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use URL;
use Redirect;
use Localization;
use Setting;
use Request;

use Layouts;
use Pages;

class PagesController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список страниц
     * 
     * @return Response
     */

    public function index()
    {

        if (Request::ajax()) {

            return Pages::all(array('id', 'title', 'lang', 'created_at', 'slug'));

        } else {

            return View::make('backend.pages.index');

        }

    }

    /**
     * Форма для создания страницы
     * 
     * @return Response
     */

    public function create()
    {   
        
        return View::make('backend.pages.create')->with(array(
            'layouts' => Layouts::all()->lists('name', 'id'),
            'langs'   => Localization::getLangs()
        ));

    }

    /**
     * Форма для редактирования страницы
     * 
     * @param int $pageId
     * @return Response
     */

    public function edit($pageId) 
    {

        if (Request::ajax()) {

            return Pages::findOrFail($pageId);

        } else {

            return View::make('backend.pages.edit')->with(array(
                'page'    => Pages::find($pageId),
                'layouts' => Layouts::all()->lists('name', 'id'),
                'langs'   => Localization::getLangs()
            ));

        }

    }

    /**
     * Создание или сохранение страницы
     * 
     * @param mixed $pageId
     * @return Response
     */

    public function save($pageId = null)
    {

        $page              = $pageId ? Pages::findOrFail($pageId) : new Pages();
        $page->title       = Input::get('title');
        $page->content     = Input::get('content');
        $page->layout_id   = Input::get('layout_id');
        $page->keywords    = Input::get('keywords');
        $page->description = Input::get('description');
        $page->lang        = Input::get('lang');
        
        if (!$page->save()) {
            return Response::json(array('error' => $page->errors->first()), 500);
        }

        if (Input::get('is_index') == 'yes') {
            Setting::set('pages.index.' . $page->lang, $page->id);
        }

        return $page;
    }

    /**
     * Удаление страницы
     * 
     * @param int $pageId
     * @return Redirect
     */

    public function delete($pageId)
    {

        Pages::findOrFail($pageId)->delete();

        return Response::json(true);

    }

}