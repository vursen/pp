<?php
/*
|--------------------------------------------------------------------------
| Backend Auth Controller
|--------------------------------------------------------------------------
*/

namespace Backend;

use Controller;
use View;
use Input;
use Response;
use Session;
use Redirect;

class AuthController extends Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * Форма авторизации
     * 
     * @return Response
     */

    public function index()
    {

        return View::make('backend.auth.login');

    }

    /**
     * Вход в систему
     * 
     * @return Response
     */

    public function login()
    {
        /*Hash::check(Input::get('password'), Setting::get('base.password'))*/
        
        if (Input::get('password') == '12345') {

            Session::put('backend.logged', true);
            return Response::json(array('redirect' => route('backend.index')));

        } else {

            return Response::json(array('error' => 'Неверный пароль.'), 500);

        }

    }

    /**
     * Выход из системы
     * 
     * @return Redirect
     */

    public function logout() 
    {

        Session::put('backend.logged', false);
        return Redirect::to('/');

    }

}