<?php
/*
|--------------------------------------------------------------------------
| Backend Store categories Controller
|--------------------------------------------------------------------------
*/

namespace Backend\Store;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Localization;

use StoreCategories;

class CategoriesController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список категорий 
     * 
     * @return Response
     */

    public function index()
    {

        return StoreCategories::all();

    }

    /**
     * Редактирование категории
     * 
     * @param int $categoryId
     * @return Response
     */

    public function edit($categoryId)
    {

        return StoreCategories::findOrFail($categoryId);

    }

    /**
     * Создание или сохранение категории
     * 
     * @param mixed $categoryId
     * @return Response
     */

    public function save($categoryId = null)
    {

        $category            = $categoryId ? StoreCategories::findOrFail($categoryId) : new StoreCategories();
        $category->name      = Input::get('name');
        $category->lang      = Input::get('lang');

        if (!$category->save()) {
            return Response::json(array('error' => $category->errors->first()), 500);
        }
        

        return $category;
    }

    /**
     * Удаление категории
     * 
     * @param int $categoryId
     * @return Redirect
     */

    public function delete($categoryId)
    {

        StoreCategories::findOrFail($categoryId)->delete();
        return Response::json(true);

    }

}