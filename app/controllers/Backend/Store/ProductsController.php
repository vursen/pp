<?php
/*
|--------------------------------------------------------------------------
| Backend Store products Controller
|--------------------------------------------------------------------------
*/

namespace Backend\Store;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Carbon\Carbon;
use Request;

use StoreProducts;
use StoreCategories;
use Tags;

class ProductsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список продуктов
     * 
     * @return Response
     */

    public function index()
    {

        return StoreProducts::with('category')->get(array('id', 'name', 'category_id', 'slug', 'price'));

    }

    /**
     * Редактирования продукта
     * 
     * @param int $productId
     * @return Response
     */

    public function edit($productId) 
    {

        $product = StoreProducts::with(array('category', 'tags', 'comments'))->find($productId);

        return $product;

    }

    /**
     * Создание или сохранение продукта
     * 
     * @param mixed $productId
     * @return Response
     */

    public function save($productId = null)
    {

        /* Создание/изменение продукта
        ------------------------------------------------------------------- */

        $product                    = $productId ? StoreProducts::findOrFail($productId) : new StoreProducts();

        $product->name              = Input::get('name');
        $product->price             = Input::get('price');
        $product->images            = Input::get('images');
        $product->content           = Input::get('content');

        $product->keywords          = Input::get('keywords');
        $product->description       = Input::get('description');

        $product->comments_disabled = (Input::get('comments_disabled') == 'yes') ? true : false;
        $product->category_id       = Input::get('category_id');

        if (!$product->save()) {
            return Response::json(array('error' => $product->errors->first()), 500);
        }

        /* Добавление/удаление тегов
        ------------------------------------------------------------------- */

        $product->tags()->detach();

        if (Input::has('tags')) {

            foreach (Input::get('tags') as $tag) {

                $product->tags()->attach(Tags::firstOrCreate(array('name' => trim($tag['name']))));

            }
            
        }

        return $product->load('tags', 'category', 'comments');
    }

    /**
     * Удаление продукта
     * 
     * @param int $productId
     * @return Redirect
     */

    public function delete($productId)
    {

        StoreProducts::findOrFail($productId)->delete();
        return Response::json(true);

    }

}