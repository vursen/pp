<?php
/*
|--------------------------------------------------------------------------
| Frontend Image Controller
|--------------------------------------------------------------------------
*/

namespace Frontend;

use Controller;
use View;
use Input;
use Response;
use Image;
use Validator;
use App;

class ImageController extends Controller {

    public function getImage() 
    {

        $validator = Validator::make(Input::all(), array(
            'path' => array('required', 'regex:/^[\/0-9A-Za-z\-_]+\.(jpg|png|jpeg|gif)$/'),
            'size' => array('required', 'regex:/^[0-9]+x[0-9]+$/')
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()->first()), 500);
        }

        $size = explode('x', Input::get('size'));
        $path = public_path() . '/' . trim(Input::get('path'), '/');

        return Image::cache(function($image) use ($size, $path) {

            return $image->make($path)
                         ->grab($size[0], $size[1]);

        }, 100000, true)->response();

    }

}