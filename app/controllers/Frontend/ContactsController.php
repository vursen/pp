<?php
/*
|--------------------------------------------------------------------------
| Frontend Contacts Controller
|--------------------------------------------------------------------------
*/

namespace Frontend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Validator;
use Captcha;
use Mail;
use Setting;

class ContactsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }
    
    public function index() 
    {

        return View::make('frontend.contacts.index');

    }

    public function send()
    {

        if (!Captcha::check(Input::get('captcha'))) {
            return Response::json(array('error' => trans('captcha.error')), 500);
        }

        $validator = Validator::make(Input::all(), array(
            'name'    => 'required',
            'email'   => 'required|email',
            'content' => 'required',
            'file'    => 'mimes:doc,docx,txt,rtf,jpg,gif,png,jpeg,pdf'
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()->first()), 500);
        }

        $data = array(
            'name' => e(Input::get('name')), 
            'email' => e(Input::get('email')), 
            'content' => e(Input::get('content'))
        );

        $file = Input::hasFile('file') ? Input::file('file') : false;

        //dd($file, Input::file('file'));

        Mail::send('frontend.contacts.email', $data, function($message) use ($file)
        {

            $message->to(Setting::get('base.email'))->subject('Новое сообщение!');

            if ($file) {
                $message->attach($file->getRealPath(), array('as' => $file->getClientOriginalName(), 'mime' => $file->getMimeType()));
            }

        });

        return Response::json(array('message' => trans('contacts.form.success')));

    }

}