<?php
/*
|--------------------------------------------------------------------------
| Frontend Blog posts Controller
|--------------------------------------------------------------------------
*/

namespace Frontend\Blog;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Paginator;
use DB;

use BlogCategories;
use BlogPosts;
use Tags;
use Comments;
use Setting;

class PostsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    } 

    /**
     * Список всех записей
     * 
     * @return Response
     */

    public function index()
    {

        $posts = BlogPosts::with('comments', 'tags')->lang()->sortByDate()->paginate(Setting::get('blog.posts_per_page'));

        return View::make('frontend.blog.index')->nest('posts_rendered', 'frontend.blog.posts.list', array('posts' => $posts));

    }

    /**
     * Список записей по тегу
     * 
     * @return Response
     */

    public function viewTag($slug) 
    {

        $tag   = Tags::slug($slug)->firstOrFail();
        $posts = $tag->posts()
                     ->with('comments', 'tags')
                     ->lang()
                     ->sortByDate()
                     ->paginate(Setting::get('blog.posts_per_page'));

        return View::make('frontend.blog.index')->with(array(

            'heading' => $tag->name
            
        ))->nest('posts_rendered', 'frontend.blog.posts.list', array('posts' => $posts));

    }

    /**
     * Список записей по категории
     * 
     * @return Response
     */

    public function viewCategory($slug)
    {   

        $category = BlogCategories::with('layout')->lang()->slug($slug)->firstOrFail();
        $posts    = $category->posts()
                             ->with('comments', 'tags')
                             ->sortByDate()
                             ->paginate(Setting::get('blog.posts_per_page'));

        return View::make('frontend.blog.index')->with(array(

            'heading' => $category->name,
            'layout'  => $category->layout

        ))->nest('posts_rendered', 'frontend.blog.posts.list', array('posts' => $posts));

    }

    /**
     * Поиск записей по названию или содержимому
     * 
     * @return Response
     */

    public function search()
    {   

        $query = Input::get('query'); //preg_replace('/[^\w\-_\s]+/u', '', Input::get('query'));
        
        $posts = BlogPosts::with('tags', 'comments')
                    ->where('title', 'LIKE', '%'. $query . '%')
                    ->orWhere('keywords', 'LIKE', '%'. $query . '%')
                    ->paginate(Setting::get('blog.posts_per_page'));

        return View::make('frontend.blog.index')->with(array(
            'heading' => 'Найдено по запросу: ' . $query
        ))->nest('posts_rendered', 'frontend.blog.posts.list', array('posts' => $posts));

    }

    /**
     * Вывод записи
     * 
     * @ return Response
     */

    public function viewPost($slug)
    {   

        $post = BlogPosts::with('comments', 'category', 'category.layout', 'tags')
                    ->lang()
                    ->slug($slug)
                    ->firstOrFail();

        return View::make('frontend.blog.posts.view')->with(array(
            'post'   => $post,
            'layout' => $post->category->layout
        ));

    }

}