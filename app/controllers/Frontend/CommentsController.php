<?php
/*
|--------------------------------------------------------------------------
| Frontend Сomments Controller
|--------------------------------------------------------------------------
*/

namespace Frontend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Captcha;
use Validator;
use Mail;
use Setting;

use Comments;
use StoreProducts;
use BlogPosts;
use Pages;

class CommentsController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Создание комментария
     * 
     * @return Response
     */

    public function save() 
    {

        if (!Captcha::check(Input::get('captcha'))) {
            return Response::json(array('error' => trans('captcha.error')), 500);
        }

        $validator = Validator::make(Input::all(), array(
            'name'    => 'required',
            'email'   => 'required|email',
            'content' => 'required'
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()->first()), 500);
        }

        if (Input::has('post_id')) {

            $commentable = BlogPosts::findOrFail(Input::get('post_id'));

        } elseif (Input::has('page_id')) {

            $commentable = Pages::findOrFail(Input::get('page_id'));
            
        } elseif (Input::has('product_id')) {

            $commentable = StoreProducts::findOrFail(Input::get('product_id'));
            
        }

        $comment          = new Comments();
        $comment->name    = e(Input::get('name'));
        $comment->email   = e(Input::get('email'));
        $comment->content = e(Input::get('content'));

        if (!$comment->save()) {
            return Response::json(array('error' => $comment->errors->first()), 500);
        }

        $commentable->comments()->save($comment);

        /*Mail::send('frontend.blog.email.comments', compact('commentable', 'comment'), function($message)
        {

            $message->to(Setting::get('base.email'))->subject('Новый комменарий!');

        });*/

        return Response::json(array('reload' => true));

    }

}