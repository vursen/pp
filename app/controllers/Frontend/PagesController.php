<?php
/*
|--------------------------------------------------------------------------
| Frontend Pagess Controller
|--------------------------------------------------------------------------
*/

namespace Frontend;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use Setting;
use App;

use Pages;
use BlogPosts;

class PagesController extends Controller {

    public function index() 
    {

        $posts = BlogPosts::with('tags', 'comments')
                    ->lang()
                    ->sortByDate()
                    ->paginate(Setting::get('blog.posts_per_page'));

        $page  = Pages::findOrFail(Setting::get('pages.index.' . App::getLocale()));

        return View::make('frontend.index')->with(array(
            
            'page' => $page,

        ))->nest('posts_rendered', 'frontend.blog.posts.list', array('posts' => $posts));

    }

    public function view($slug) 
    {

        return View::make('frontend.pages.view')->with(array(
            'page' => Pages::lang()->slug($slug)->firstOrFail()
        ));

    }

}