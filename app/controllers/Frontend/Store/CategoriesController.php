<?php
/*
|--------------------------------------------------------------------------
| Frontend Store categories Controller
|--------------------------------------------------------------------------
*/

namespace Frontend\Store;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Localization;

use StoreCategories;

class CategoriesController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список категорий 
     * 
     * @return Response
     */

    public function index()
    {

        return StoreCategories::all();

    }

    /**
     * Вывод списка продуктов категории
     * 
     * @param int $slug
     * @return Response
     */

    public function view($slug)
    {

        return StoreCategories::lang()->slug($slug)->firstOrFail();

    }

}