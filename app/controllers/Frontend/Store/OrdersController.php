<?php
/*
|--------------------------------------------------------------------------
| Frontend Store products Controller
|--------------------------------------------------------------------------
*/

namespace Frontend\Store;

use Controller;
use View;
use Input;
use Response;
use Redirect;
use URL;
use Carbon\Carbon;
use Request;

use StoreOrders;
use StoreCategories;
use Tags;

class OrdersController extends Controller {

    public function __construct()
    {

        $this->beforeFilter('csrf', array('on' => 'post'));

    }

    /**
     * Список записей
     * 
     * @return Response
     */

    public function index()
    {

        return StoreOrders::with('category')->get(array('id', 'title', 'added_on', 'category_id', 'slug'));

    }

    /**
     * Редактирования записи
     * 
     * @param int $productId
     * @return Response
     */

    public function edit($productId) 
    {

        $product = StoreOrders::with(array('category', 'tags'))->find($productId);

        return $product;

    }

    /**
     * Создание или сохранение продукта
     * 
     * @param mixed $productId
     * @return Response
     */

    public function save($productId = null)
    {

        /* Создание/изменение записи
        ------------------------------------------------------------------- */

        $product                    = $productId ? StoreOrders::findOrFail($productId) : new StoreOrders();

        $product->name             = Input::get('name');
        $product->content           = Input::get('content');

        $product->keywords          = Input::get('keywords');
        $product->description       = Input::get('description');

        $product->comments_disabled = (Input::get('comments_disabled') == 'yes') ? true : false;
        $product->category_id       = Input::get('category_id');

        if (!$product->save()) {
            return Response::json(array('error' => $product->errors->first()), 500);
        }

        /* Добавление/удаление тегов
        ------------------------------------------------------------------- */

        $product->tags()->detach();

        if (Input::has('tags')) {

            foreach (Input::get('tags') as $tag) {

                $product->tags()->attach(Tags::firstOrCreate(array('name' => trim($tag['name']))));

            }
            
        }

        return $product->load('tags', 'category');
    }

    /**
     * Удаление записи
     * 
     * @param int $productId
     * @return Redirect
     */

    public function delete($productId)
    {

        StoreOrders::findOrFail($productId)->delete();
        return Response::json(true);

    }

}