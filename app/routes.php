<?php
/*
|-------------------------------------------------------------------------
| Определение языка
|-------------------------------------------------------------------------
*/

$language = Request::segment(1);

if(in_array($language, array_keys(Config::get('localization.languages')))) {
    
    App::setLocale($language);

} else {

    $language = '';

}

/*
|-------------------------------------------------------------------------
| Регулярные выражения для параметров
|-------------------------------------------------------------------------
*/

Route::pattern('postId', '[0-9]+');
Route::pattern('pageId', '[0-9]+');
Route::pattern('categoryId', '[0-9]+');
Route::pattern('productId', '[0-9]+');
Route::pattern('orderId', '[0-9]+');

include app_path() . '/routes/frontend.php';
include app_path() . '/routes/backend.php';