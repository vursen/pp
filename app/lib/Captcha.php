<?php

class Captcha {
    
    public static function generate() {

        $number1 = rand(1, 10);
        $number2 = rand(1, 10);

        $question = $number1 . '+' . $number2;
        $answer   = $number1 + $number2;

        Session::put('captcha', array(
            'question' => $question,
            'answer'   => $answer
        ));

        return $question;

    }

    public static function check($answer) {

        return Session::get('captcha.answer') === (int)$answer;

    }

}

?>