<?php
class Localization {
    
    static function url($lang, $path, $extra = array(), $secure) {

        return self::_buildUrl($lang, URL::to($path, $extra, $secure));

    }

    static function route($lang, $name, $parameters = array(), $absolute = true, Route $route = null) {

        return self::_buildUrl($lang, URL::route($name, $parameters, $absolute, $route), $absolute);

    }

    static private function _buildUrl($lang, $url, $absolute = true) {

        $parseUrl         = parse_url($url);
        $parseUrl['path'] = explode('/', trim($parseUrl['path'], '/'));
        
        if (!array_key_exists($parseUrl['path'][0], self::getLangs())) {

            array_unshift($parseUrl['path'], $lang);
            $url = $absolute ? URL::to('/') . '/' . implode('/', $parseUrl['path']) : '/' . implode('/', $parseUrl['path']);

        }

        return $url;

    }

    static function getLangName($lang) {

        return Config::get('localization.languages.' . $lang);

    }

    static function getLangs() {

        return Config::get('localization.languages');

    }

}