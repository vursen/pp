@extends('frontend.layouts.' . $page->layout->path)

@section('title', $page->title)
@section('meta_keywords', $page->keywords)
@section('meta_description', $page->description)

@section('content')
<h1 class="page-heading">{{ $page->title }}</h1>

{{ $page->rendered }}
@stop