@extends('frontend.layouts.main')
@section('body')

    <!-- Header - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-header">
        <div class="container">

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-header__top clearfix">
                <div class="b-header__clock">
                    @include("frontend.partials.clock")
                </div>

                <div class="b-header__top__logo">
                    <a href="{{ url('/ru') }}">HermitageLine</a>
                </div>

                <div class="b-header__menu b-top-menu">
                    <div class="b-top-menu__big">
                        {{ Pages::title('Верхнее меню')->firstOrFail()->content }}
                    </div>

                    <div class="b-top-menu__small"></div>

                    <a href="#" class="b-top-menu__pull">Меню</a>
                </div>
            </div><!-- /.b-header__top.clearfix -->

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-header__main">

                <div class="b-header__main__logo">
                    <a href="{{ url('/ru') }}"><img src="{{ asset('img/b-header__main__logo.png') }}" alt="" /></a>
                </div><!-- /.b-header__main__logo-->

            </div><!-- /.b-header__main -->
            
            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        </div><!-- /.container -->
    </div><!-- /.b-header -->

    <!-- Main - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-main container">
        <div class="clearfix">
            <div class="col col--2-3 b-main__icons">
                <a href="#" class="icon icon__fb"></a>
                <a href="#" class="icon icon__tw"></a>
                <a href="#" class="icon icon__vk"></a>
                <a href="{{ url('/ru') }}" class="icon icon__ru"></a>
                <a href="{{ url('/en') }}" class="icon icon__en"></a>
            </div>

            {{ Form::open(array('route' => 'frontend.blog.search', 'class' => 'b-search col col--1-3')) }}
                <input type="text" name="query" class="b-search__input"> 

                <button type="submit" class="icon icon__search"></button>
            {{ Form::close() }}
        </div>

        <div class="b-main__quote">
            {{ Pages::title('Цитаты')->firstOrFail()->content }}
        </div>

        <!-- Контент - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <div class="b-content col col--2-3">
            @yield('content')
        </div><!-- /.b-content -->
        
        <!-- Сайдбар - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <div class="b-sidebar col col--1-3">

            <!-- Сайдбар / Спонсоры  - - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">

                {{ Pages::title('Спонсоры')->firstOrFail()->content }}

            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / День за днем - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">

                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/category-day-by-day.png') }}" alt="" />
                    <div>День за днем</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.partials.sidebar-category', array('count' => 5, 'category_name' => 'День за днем', 'preview_width' => 100, 'preview_height' => 100))

            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / Фото - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">

                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/category-photo.png') }}" alt="" />
                    <div>Фото</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.partials.sidebar-category', array('count' => 5, 'category_name' => 'Фото', 'preview_width' => 100, 'preview_height' => 100))

            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / Видео - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">

                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/category-video.png') }}" alt="" />
                    <div>Видео</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.partials.sidebar-category', array('count' => 5, 'category_name' => 'Видео', 'preview_width' => 100, 'preview_height' => 100))

            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / Календарь событий - - - - - - - - - - - - - - - -->

            {{--<div class="b-sidebar__section">

                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/category-events.png') }}" alt="" />
                    <div>Календарь событий</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.partials.sidebar-category', array('count' => 5, 'category_name' => 'Календарь событий', 'preview_width' => 100, 'preview_height' => 100))

            </div><!-- /.b-sidebar__section -->--}}
        </div><!-- /.b-sidebar -->
    </div><!-- /.b-main.container -->

    <!-- Footer - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-footer">
        <div class="container">
            <div class="col col--2-3 b-footer__text">
                <p>Все права защищены. При перепечатке материалов данного сайта ссылка обязательна </p>
                <p>Copyright 2012 HermitageLine</p>
            </div>

            <div class="col col--1-3">
                <a href="#" class="icon icon__fb"></a>
                <a href="#" class="icon icon__tw"></a>
                <a href="#" class="icon icon__vk"></a>
            </div>
        </div><!-- /.container -->
    </div><!-- /.b-footer -->
@stop