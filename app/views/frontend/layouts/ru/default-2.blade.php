@extends('frontend.layouts.main')
@section('body')

    <!-- Header - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-header">
        <div class="container">

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-header__top clearfix">
                <div class="b-header__clock">
                    @include("frontend.layouts.ru.partials.clock")
                </div>

                <div class="b-header__top__logo">
                    <a href="{{ url('/ru') }}">HermitageLine</a>
                </div>

                <div class="b-header__menu b-top-menu">
                    <div class="b-top-menu__big">
                        {{ Pages::title('меню эрмитаж')->firstOrFail()->content }}
                    </div>

                    <div class="b-top-menu__small"></div>

                    <a href="#" class="b-top-menu__pull">Меню</a>
                </div>
            </div><!-- /.b-header__top.clearfix -->

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-header__main">

                <div class="b-header__main__logo">
                    <a href="{{ url('/ru') }}"><img src="{{ asset('img/b-header__main__logo.png') }}" alt="" /></a>
                </div><!-- /.b-header__main__logo-->

            </div><!-- /.b-header__main -->
            
            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        </div><!-- /.container -->
    </div><!-- /.b-header -->

    <!-- Main - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-main container">
        <div class="b-main__top clearfix">

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-main__top__icons">
                <div class="b-social">

                </div>

                <div class="b-locales">

                </div>
            </div>
            
            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

            {{ Form::open(array('route' => 'frontend.blog.search', 'class' => 'b-search b-main__search')) }}
                <input type="text" name="query" class="b-search__input"> 

                <button type="submit" class="b-search__btn"></button>
            {{ Form::close() }}

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        </div>

        <div class="b-content">

            <div class="b-content__slider"> @include("frontend.layouts.ru.partials.slider")</div>

            <div class="b-content__main">            
                @yield('content')
            </div>

        </div><!-- /.b-content -->
        
        <!-- Сайдбар - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <div class="b-sidebar">

            <!-- Сайдбар / Спонсоры  - - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">
                {{ Pages::title('Спонсоры')->firstOrFail()->content }}
            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / День за днем - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">
                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/den_za_dnem.png') }}" alt="" />

                    <div>День за днем</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.layouts.ru.partials.sidebar-category', array('count' => 5, 'category_name' => 'День за днем', 'preview_width' => 60, 'preview_height' => 60))
            </div><!-- /.b-sidebar__section -->

            <!-- Сайдбар / Фото - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-sidebar__section">
                <div class="b-sidebar__section__heading">
                    <img src="{{ asset('img/den_za_dnem.png') }}" alt="" />

                    <div>Фото</div>  
                </div><!-- /.b-siderbar__section__heading -->

                @include('frontend.layouts.ru.partials.sidebar-category', array('count' => 5, 'category_name' => 'Фото', 'preview_width' => 60, 'preview_height' => 60))
            </div><!-- /.b-sidebar__section -->

            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        </div><!-- /.b-sidebar -->
    </div><!-- /.b-main.container -->

    <!-- Footer - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="b-footer">
        <div class="container">
            <div class="b-footer__text">
                <p>Все права защищены. При перепечатке материалов данного сайта ссылка обязательна </p>

                <p>Copyright 2012 HermitageLine</p>
            </div><!-- /.b-footer__text -->

            <div class="b-footer__social b-social">
                <a href="#" class="b-social__icon b-social__icon-vk-"></a>

                <a href="#" class="b-social__icon b-social__icon--fb"></a>

                <a href="#" class="b-social__icon b-social__icon--tw"></a>
            </div><!-- /.b-footer__social.b-social -->
        </div><!-- /.container -->
    </div><!-- /.b-footer -->
@stop

{{--    
        
       <!-- Соц. сети - - - - - - - - - - - - - - - - - - - - -->

        <div class="social">
            <div class="kontakt_button">
                <div class="RollOver">
                    <a href="#"><img class="hover" alt="" src="{{ asset('img/kontakt_button-2.png') }}"><span><img alt="" src="{{ asset('img/kontakt_button.png') }}"></span></a>
                </div>
            </div>

            <div class="face_button">
                <div class="RollOver">
                    <a href="#"><img class="hover" alt="" src="{{ asset('img/face_button-2.png') }}"><span><img alt="" src="{{ asset('img/face_button.png') }}"></span></a>
                </div>
            </div>

            <div class="tvit_button">
                <div class="RollOver">
                    <a href="#"><img class="hover" alt="" src="{{ asset('img/tvit_button-2.png') }}"><span><img alt="" src="{{ asset('img/tvit_button.png') }}"></span></a>
                </div>
            </div>

            <div class="rus_button">
                <div class="RollOver">
                    <a href="{{ url('/ru') }}"><img class="hover" alt="" src="{{ asset('img/ru_button-2.jpg') }}"><span><img alt="" src="{{ asset('img/ru_button.jpg') }}"></span></a>
                </div>
            </div>

            <div class="engl_button">
                <div class="RollOver">
                    <a href="{{ url('/en') }}"><img class="hover" alt="" src="{{ asset('img/en_button-2.jpg') }}"><span><img alt="" src="{{ asset('img/en_button.jpg') }}"></span></a>
                </div>
            </div>            
        </div>
    </div>

    <!-- Цитаты - - - - - - - - - - - - - - - - - - - - - - - -->

    <div class="citata">{{ Pages::title('Цитаты')->firstOrFail()->content }}</div>

    <!-- Контент - - - - - - - - - - - - - - - - - - - - - - - -->
    --}}