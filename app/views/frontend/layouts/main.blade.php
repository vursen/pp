<!DOCTYPE html>
<html>
<head>
    <title> @yield('title') </title>
    <meta content="@yield('meta_keywords')" name="keywords" />
    <meta content="@yield('meta_description')" name="description" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    @section('styles')
        {{ HTML::style('css/reset.css') }}

        {{ HTML::style('css/base.css') }}

        {{ HTML::style('css/main.css') }}

        {{ HTML::style('css/ckplugins.css') }}

        {{ HTML::style('css/responsive.css') }}
        
        {{ HTML::style('css/global_line.css') }}
        
        {{ HTML::style('css/content.css') }}

        {{ HTML::style('js/vendor/galleria.classic/galleria.classic.css') }} 

        {{ HTML::style('js/vendor/jquery.scrollerota/css/jquery.scrollerota.css') }}

        {{ HTML::style('js/vendor/jquery.thumbs/css/jquery.thumbs.css') }} 

        {{ HTML::style('js/vendor/jquery.fancybox/css/jquery.fancybox.css') }}

        {{ HTML::style('js/vendor/jquery.sweet_thumbnails/css/jquery.sweet_thumbnails.css') }}

        {{ HTML::style('js/vendor/jquery.countdown/css/jquery.countdown.css') }} 
    @show
</head>
<body>
    @yield('body')

    @section('scripts')  
        {{ HTML::script('js/vendor/jquery/jquery.min.js') }}

        {{ HTML::script('js/vendor/jquery.form/jquery.form.min.js') }}

        {{ HTML::script('js/vendor/galleria.classic/galleria-1.3.5.min.js') }}

        {{ HTML::script('js/vendor/galleria.classic/galleria.classic.min.js') }}

        {{ HTML::script('js/vendor/jquery.fancybox/jquery.fancybox.pack.js') }}

        {{ HTML::script('js/vendor/jquery.scrollerota/jquery.scrollerota.min.js') }}

        {{ HTML::script('js/vendor/jquery.sweet_thumbnails/jquery.sweet_thumbnails.js') }}

        {{ HTML::script('js/vendor/jquery.thumbs/jquery.thumbs.js') }}

        {{ HTML::script('js/vendor/jquery.fitvids/jquery.fitvids.js') }}

        {{ HTML::script('js/vendor/jquery.countdown/jquery.plugin.min.js') }}

        {{ HTML::script('js/vendor/jquery.countdown/jquery.countdown.min.js') }}

        {{ HTML::script('js/vendor/jquery.countdown/jquery.countdown-ru.js') }}

        {{ HTML::script('js/menu.js') }}

        {{ HTML::script('js/main.js') }}
        
        {{ HTML::script('js/ckplugins.js') }}
    @show
</body>
</html>