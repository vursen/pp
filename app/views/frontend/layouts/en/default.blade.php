<!DOCTYPE html>
<html>
<head>
    <title> @yield('title', 'Home page') </title>
    <meta content="@yield('meta_keywords')" name="keywords" />
    <meta content="@yield('meta_description')" name="description" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    
    {{ HTML::style('css/gorizont_rez_menu.css') }}

    {{ HTML::style('css/global.css') }}

    {{ HTML::style('css/content.css') }}

    {{ HTML::style('css/scroller.css') }}

    {{ HTML::style('css/jquery.thumbs.css') }} 

    {{ HTML::style('js/vendor/fancybox/jquery.fancybox.css') }}

    {{ HTML::style('js/vendor/sweet_thumbnails/css/style.css') }}

    {{ HTML::style('js/vendor/galleria/galleria.classic.css') }} 

</head>
<body>

<div class="bac_top_menu"></div>

<div class="wrapper_0">
    <div class="top_menu">
        {{ Page::lang()->title('Top menu')->firstOrFail()->content }}
    </div>

    <!-- - - - - Header Wrap - - - - - - - - - - - - - - -->

    <div class="header_wrapp">
    
        <div class="vagon"><img alt="" src="{{ asset('img/vagon.png') }}"></div>
        <div class="logo_header"><a href="/"><img alt="" src="{{ asset('img/logo.png') }}"></a></div>
        
        <div class="rus_button">
            <div class="RollOver">
                <a href="{{ url('/ru') }}"><img class="hover" alt="" src="{{ asset('img/Rus-2.jpg') }}"><span><img alt="" src="{{ asset('img/Rus-1.png') }}"></span></a>
            </div>
        </div>
        
        <div class="engl_button">
            <div class="RollOver">
                <a href="{{ url('/en') }}"><img class="hover" alt="" src="{{ asset('img/Engl-2.jpg') }}"><span><img alt="" src="{{ asset('img/Engl-1.png') }}"></span></a>
            </div>
        </div>

        <div class="akcii">
            <div class="RollOver">
                <a href="/"><img class="hover" alt="" src="{{ asset('img/akcii-2.png') }}"><span><img alt="" src="{{ asset('img/akcii-1.png') }}"></span></a>
            </div>
        </div>

        <div class="right_in_header">Отдел реализации<br>8 (3462) 21-40-00<br>8 (3462) 21-40-11</div>

        <div id="Bookmark"><a id="uid" name="bookmark"></a></div>

    </div>

    <!-- - - - - Wrapper - - - - - - - - - - - - - - - - -->

    <div class="wrapper">
        <div id="middle">
            <div id="container">
            
                <div id="l-sb_content_r-sb">
                    <div id="page_block">
                        @yield('content')
                    </div>
                </div>
            </div>

            <div id="sideLeft">
                <div class="left_sitebar">
                    <div class="left_sitebar_1"><div class="rubrika">Menu</div></div>
                    <div class="left_sitebar_2">

                        <div class="left_menu">
                            {{ Page::lang()->title('Left menu')->firstOrFail()->content }}
                        </div>

                    </div><!-- /.left-sitebar -->
                </div>
            </div>

            <div id="sideRight">
                <div class="right_sitebar">
                    <div class="right_sitebar_1"><div class="rubrika">Last news</div></div>
                    <div class="right_sitebar_2">
                        
                        <!-- - - Блок последних новостей - - - - - - - - - - -->

                        <ul class="latest-posts">
                            @foreach(BlogPost::with('comments')->lang()->sortByDate()->take(5)->get() as $post)
                            <li class="latest-posts__item">
                                
                                @if ($post->preview_img)
                                <img class="latest-posts__image-preview" src="{{ URL::to("/image/?path={$post->preview_img}&width=40&height=40", false) }}">
                                @endif

                                <div class="latest-posts__link">
                                    <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.view', $post->slug) }}" target='_parent'>{{ $post->title }}</a> 
                                    <b>({{ $post->comments->count() }})</b>
                                </div>
                                <div class="latest-posts__date">{{ $post->created_at }}</div>
                            </li>
                            @endforeach

                            <li><a href="{{ Localization::route(App::getLocale(), 'frontend.blog') }}" class="latest-posts__more">All news</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="footer_back"></div>

        <div class="footer">
            <div><a href="#bookmark"><img class="bookmark" src="{{ asset('img/na-verh-stranici.png') }}" alt="" border="0"></a></div>
            <div class="footer_logo"><img src="{{ asset('img/Logo-bottom.png') }}" alt="" border="0"></div>
            <div class="footer_copyrite">© PandP - Content Management System<br></div>
        </div>
    </div>
</div>

@section('scripts')
    
{{ HTML::script('js/vendor/jquery.js') }}

{{ HTML::script('js/vendor/jquery-ui.min.js') }}

{{ HTML::script('js/vendor/fancybox/jquery.fancybox.pack.js') }}

{{ HTML::script('js/vendor/galleria/galleria-1.3.5.min.js') }}

{{ HTML::script('js/vendor/galleria/galleria.classic.min.js') }}

{{ HTML::script('js/gorizont_rez_menu.js') }}

{{ HTML::script('js/vendor/jquery.form.min.js') }}

{{ HTML::script('js/main.js') }}

@show

</body>
</html>
