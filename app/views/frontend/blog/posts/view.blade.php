@extends('frontend.layouts.' . (isset($layout) ? $layout->path : App::getLocale() . '.default'))

@section('title', $post->title)
@section('meta_keywords', $post->keywords)
@section('meta_description', $post->description)

@section('content')
    <h1 class="page-heading">{{ $post->category->name }} <a href="{{ URL::previous() }}" class="back-link">Назад</a></h1>

    <div class="b-post">

        <div class="b-post__info">

            <!-- Заголовок - - - - - - - - - - - - - - - - - - - - -->

            <h2 class="b-post__heading">{{ $post->title }}</h2>

            <!-- Дата - - - - - - - - - - - - - - - - - - - - - - -->

            <div class="b-post__date">{{ $post->added_on }}</div>

            <!-- Теги - - - - - - - - - - - - - - - - - - - - - - -->

            @if ($post->tags->count())    
                <ul class="b-tags b-post__tags">
                    @foreach($post->tags as $tag)
                        <li class="b-tags__item">
                            {{ HTML::link(Localization::route(App::getLocale(), 'frontend.blog.tags.view', $tag->slug), $tag->name) }}
                        </li>
                    @endforeach
                </ul>
            @endif

        </div><!-- /.b-post__info -->

        <!-- Контент - - - - - - - - - - - - - - - - - - - - -->

        <div class="b-post__content">{{ $post->content }}</div>
    </div>

    @unless ($post->comments_disabled)
        <div class="b-comments">
            @foreach($post->comments as $comment)
                <div class="b-comments__item">
                    <div class="b-comments__info clearfix">
                        <span class="b-comments__date">{{ $comment->created_at }}</span>

                        <span class="b-comments__author">{{ $comment->name }}</span>
                    </div>

                    <div class="b-comments__content">{{ $comment->content }}</div>
                </div>
            @endforeach
        </div><!-- /.b-comments -->

        @include('frontend.partials.comment-form',  array('commentable_type' => 'post', 'commentable_id' => $post->id))
    @endunless
@stop