@if ($posts->count())
    @foreach($posts as $post)
        <div class="b-post clearfix">
            <div class="b-post__info">

                <!-- Заголовок - - - - - - - - - - - - - - - - - - - - -->

                <h2 class="b-post__heading">
                    {{ HTML::link(Localization::route(App::getLocale(), 'frontend.blog.posts.view', $post->slug), $post->title) }}
                </h2>

                <!-- Дата - - - - - - - - - - - - - - - - - - - - - - -->

                <div class="b-post__date">{{ $post->added_on }}</div>

                <!-- Теги - - - - - - - - - - - - - - - - - - - - - - -->
                
                @if ($post->tags->count())    
                    <ul class="b-tags b-post__tags">
                        @foreach($post->tags as $tag)
                            <li class="b-tags__item">
                                {{ HTML::link(Localization::route(App::getLocale(), 'frontend.blog.tags.view', $tag->slug), $tag->name) }}
                            </li>
                        @endforeach
                    </ul>
                @endif

            </div><!-- /.b-post__info -->

            <!-- Превью - - - - - - - - - - - - - - - - - - - - - -->

            @if ($post->preview_img) 
                <img src="{{ URL::to('/image?path=' . $post->preview_img .'&amp;size=200x200', false) }}" class="b-post__image img img--bordered">
            @endif

            <!-- Анонс - - - - - - - - - - - - - - - - - - - - - -->
            
            <div class="b-post__content">{{ nl2br($post->preview_text) }}</div>
            
            <!-- Кнопка далее - - - - - - - - - - - - - - - - - -->

            <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.posts.view', $post->slug) }}" class="btn btn--more btn--medium">
                {{ Lang::get('blog.posts.more') }}
            </a>

            <!-- Кнопка комментировать - - - - - - - - - - - - -->
            
            @unless($post->comments_disabled)
                <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.posts.view', $post->slug) }}" class="btn btn--light btn--medium">
                    {{ Lang::get('blog.posts.comments.count', array('count' => $post->comments->count())) }}
                </a>
            @endunless

            <!-- - - - - - - - - - - - - - - - - - - - - - - - -->
        </div><!-- /.b-post.clearfix -->
    @endforeach

    {{ $posts->links() }}
@else
    Записи не найдены.
@endif