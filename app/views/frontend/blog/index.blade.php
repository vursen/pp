@extends('frontend.layouts.' . (isset($layout) ? $layout->path : App::getLocale() . '.default'))

@section('content')
    <h1 class="page-heading">{{ isset($heading) ? $heading : Lang::get('blog.heading') }}</h1>

    {{ $posts_rendered }}
@stop