<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новый комментарий</title>
</head>
<body>
    <h2>Новый комментарий в записи <a href="{{ route('frontend.blog.view', $post->slug) }}">{{ $post->title }}</a></h2>

    <p><b>Имя</b> {{ $comment->name }}</p>

    <p><b>E-mail</b> {{ $comment->email }}</p>

    <p><b>Сообщение</b></p>

    <p>{{ $comment->content }}</p>
</body>
</html>