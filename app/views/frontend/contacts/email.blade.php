<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новое сообщение</title>
</head>
<body>
    <h2>Новое сообщение</h2>
    <p><b>Имя</b> {{ $name }}</p>
    <p><b>E-mail</b> {{ $email }}</p>
    <p><b>Сообщение</b></p>
    <p>{{ $content }}</p>
</body>
</html>