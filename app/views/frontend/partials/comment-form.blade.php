{{ Form::open(array('route' => 'frontend.comments.save', 'class' => 'form js-ajax-form')) }}
                    
    <div class="form__heading">{{ trans('comments.form.heading') }}</div>

    <label class="form-label">
        {{ trans('comments.form.fields.name') }}
        <input type="text" name="name" class="form-input" />
    </label>

    <label class="form-label">
        {{ trans('comments.form.fields.email') }}
        <input type="text" name="email" class="form-input" />
    </label>

    <label class="form-label">
        {{ trans('comments.form.fields.content') }}
        <textarea name="content" class="form-textarea"></textarea>
    </label>

    <label class="form-label">
        {{ trans('captcha.question', array('number' => Captcha::generate())) }}
        <input type="text" name="captcha" class="form-input" />
    </label>

    <input type="hidden" name="{{ $commentable_type . '_id' }}" value="{{ $commentable_id }}">

    <label class="form-label">
        <button type="submit" class="form__button btn btn--dark btn--medium">{{ trans('comments.form.button') }}</button>
    </label>

{{ Form::close() }}