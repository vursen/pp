<div id="Date"></div>
<ul class="b-clock">
    <li id="hours"></li>
    <li id="point">:</li>
    <li id="min"></li>
    <li id="point">:</li>
    <li id="sec"></li>
</ul>

@section('styles')
    @parent

    <style>
    .b-clock  li { display:inline; font-size:22px; text-align:center; font-family:Arial, sans-serif; }
    #Date { font-family: Arial, sans-serif; font-size:12px; text-align:center;}
    #point { position:relative; -moz-animation:mymove 1s ease infinite; -webkit-animation:mymove 1s ease infinite; padding-left:6px; padding-right:6px; }
    </style>
@stop

@section('scripts')
    @parent

    <script>
    $(document).ready(function() {

        var monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        var dayNames   = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
        var newDate    = new Date();

        newDate.setDate(newDate.getDate()); 

        $('#Date').html(dayNames[newDate.getDay()] + ' ' + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

        setInterval( function() {
            var seconds = new Date().getSeconds();
            $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
        },1000);
            
        setInterval( function() {
            var minutes = new Date().getMinutes();
            $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
        },1000);
            
        setInterval( function() {
            var hours = new Date().getHours();
            $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
        }, 1000);
        
    }); 
    </script>
@stop