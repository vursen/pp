<?php 
$category = BlogCategories::with(array('posts' => function($query) use ($count) {

    return $query->sortByDate()->take($count);

}), 'posts.comments')->name($category_name)->firstOrFail();
?>

<div class="b-recent-posts">
    @foreach($category->posts as $post)
        <div class="b-post-preview clearfix">
            
            <!-- Превью - - - - - - - - - - - - - - - - - - -->

            @if ($post->preview_img)
                <img class="b-post-preview__image" src="{{ URL::to("/image?path={$post->preview_img}&size={$preview_width}x{$preview_height}", false) }}">
            @endif

            <!-- Дата - - - - - - - - - - - - - - - - - - - -->

            <div class="b-post-preview__date">{{ $post->added_on }}</div>
    
            <!-- Заголовок и кол-во комментарий - - - - - - -->

            <div class="b-post-preview__heading">
                <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.posts.view', $post->slug) }}" target='_parent'>{{ $post->title }}</a> 
                <b>({{ $post->comments->count() }})</b>
            </div>

        </div><!-- /.b-recent-posts__item.b-post-preview -->
    @endforeach

    <a href="{{ route('frontend.blog.categories.view', $category->slug) }}" class="b-recent-posts__more">Все записи</a>
</div>
