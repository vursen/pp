<?php 
$category = BlogCategories::with(array('posts' => function($query) use ($count) {

    return $query->sortByDate(true)->take($count);

}), 'posts.comments')->name($category_name)->firstOrFail();
?>

<ul class="latest-posts">
    @foreach($category->posts as $post)
        <li class="latest-posts__item">
            
            <!-- Превью - - - - - - - - - - - - - - - - - - -->

            @if ($post->preview_img)
                <img class="latest-posts__image-preview" src="{{ URL::to("/image/?path={$post->preview_img}&size={$preview_width}x{$preview_height}", false) }}">
            @endif

            <!-- Дата - - - - - - - - - - - - - - - - - - - -->

            <div class="latest-posts__date">{{ $post->added_on }}</div>
    
            <!-- Заголовок и кол-во комментарий - - - - - - -->

            <div class="latest-posts__link">
                <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.posts.view', $post->slug) }}" target='_parent'>
                    {{ $post->title }}
                </a> 
                <b>({{ $post->comments->count() }})</b>
            </div>

        </li><!-- /.latest-posts__item -->

        <div class="clear" style="border-bottom:solid 1px #d7d7d7;padding: 3px 0 0 0;"></div>
    @endforeach

    <li>
        <a href="{{ route('frontend.blog.category', $category->slug) }}" class="latest-posts__more">Все события</a>
    </li>
</ul>
