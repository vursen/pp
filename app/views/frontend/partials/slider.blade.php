<ul class="bxslider">
    <li>
        <a href="{{ route('frontend.blog.categories.view', 'dnevniki-kotov') }}">
            <img  title="" src="{{ asset('uploads/slider/1.jpg') }}">
        </a>
    </li>
 
    <li><img title="Царское Село. Камеронова галерея" src="{{ asset('uploads/slider/2.jpg') }}"></li>
 
    <li><img title="Царское Село. Старый мост" src="{{ asset('uploads/slider/3.jpg') }}"></li>
 
    <li><img title="Смольный собор" src="{{ asset('uploads/slider/4.jpg') }}"></li>
</ul>

@section('styles')
    @parent
    
    {{ HTML::style('js/vendor/jquery.bxslider/css/jquery.bxslider.css') }}
@stop

@section('scripts')
    @parent

    {{ HTML::script('js/vendor/jquery.bxslider/jquery.bxslider.js') }}

    <script>
    $(document).ready(function() {

        $('.bxslider').bxSlider({
            autoControls: true,
            auto: true,
            mode: 'fade',
            captions: true
        });

    });
    </script>
@stop

