{{ Form::open(array('route' => array('frontend.contacts.send'), 'class' => 'form js-ajax-form', 'files' => true)) }}
    
    <h2 class="form__heading">{{ trans('contacts.form.heading') }}</h2>

    <label class="form-label">
        *{{ trans('contacts.form.fields.name') }}
        <input type="text" name="name" class="form-input" />
    </label>

    <label class="form-label">
        *{{ trans('contacts.form.fields.email') }}
        <input type="text" name="email" class="form-input" />
    </label>

    <label class="form-label">
        *{{ trans('contacts.form.fields.content') }}
        <textarea name="content" class="form-textarea"></textarea>
    </label>

    <label class="form-label">
        {{ trans('contacts.form.fields.file') }} (doc, docx, txt, rtf, jpeg, gif, png, pdf)
        <input type="file" name="file" class="form-file" />
    </label>

    <label class="form-label">
        *{{ trans('captcha.question', array('number' => Captcha::generate())) }}
        <input type="text" name="captcha" class="form-input" />
    </label>

    <label class="form-label">
        <button type="submit" class="form__button btn btn--dark btn--medium">{{ trans('contacts.form.button') }}</button>
    </label>

{{ Form::close() }}