@if ($posts->count())
    @foreach($posts as $post)
        
        <!-- Заголовок - - - - - - - - - - - - - - - - - - - - -->

        <h2 class="blog-title">
            {{ HTML::link(Localization::route(App::getLocale(), 'frontend.blog.view', $post->slug), $post->title) }}
        </h2>

        <!-- Дата - - - - - - - - - - - - - - - - - - - - - - -->

        <div class="blog-date">{{ $post->added_on }}</div>

        <!-- Теги - - - - - - - - - - - - - - - - - - - - - - -->

        <div class="blog-tags">
            @foreach($post->tags as $tag)
                {{ HTML::link(Localization::route(App::getLocale(), 'frontend.blog.tag', $tag->slug), $tag->name) }}
            @endforeach
        </div>

        <!-- Превью - - - - - - - - - - - - - - - - - - - - - -->

        @if ($post->preview_img) 
            <div class="blog-images"><img src="{{ URL::to("/image/?path={$post->preview_img}&size=200x200", false) }}"></div>
        @endif

        <!-- Анонс - - - - - - - - - - - - - - - - - - - - - -->
        
        <div class="blog-content">{{ nl2br($post->preview_text) }}</div>
        
        <!-- Кнопка далее - - - - - - - - - - - - - - - - - -->

        <div class="blog-more-button">
            <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.view', $post->slug) }}">
                {{ Lang::get('blog.posts.more') }}
            </a>
        </div>

        <!-- Кнопка комментировать - - - - - - - - - - - - -->
        
        @unless($post->comments_disabled)
            <div class="blog-comments-button">
                <a href="{{ Localization::route(App::getLocale(), 'frontend.blog.view', $post->slug) }}">
                    {{ Lang::get('blog.posts.comments.count', array('count' => $post->comments->count())) }}
                </a>
            </div>
        @endunless

        <div class="clear"></div>

    @endforeach

    <ul class="blog-pagination">{{ $posts->links() }}</ul>

@else
    Записи не найдены.
@endif
