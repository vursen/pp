@extends('frontend.layouts.' . App::getLocale() . '.store')

@section('title', $product->name)
@section('meta_keywords', $product->keywords)
@section('meta_description', $product->description)

@section('content')
    <div class="b-product clearfix">
        <h1 class="page-heading">{{ $product->name }}</h1>

        <ul class="b-product__images clearfix">
            @foreach($product->images as $image)
                <li class="b-product__images__item">
                    <img src="{{ URL::to('/image?path=' . $image. '&size=135x135', false) }} " alt="">
                </li>
            @endforeach
        </ul>

        <div class="b-product__info">
            <div class="b-product__info__item">
                Категория: {{ HTML::link(Localization::route(App::getLocale(), 'frontend.store.categories.view', $product->category->slug), $product->category->name) }}
            </div>

            @if ($product->tags->count())    
                <ul class="b-tags b-product__tags">
                    @foreach($product->tags as $tag)
                        <li class="b-tags__item">
                            {{ HTML::link(Localization::route(App::getLocale(), 'frontend.store.tags.view', $tag->slug), $tag->name) }}
                        </li>
                    @endforeach
                </ul>
            @endif

            <div class="b-product__content">{{ $product->content }}</div>

            <div class="b-product__info__item b-product__price">Стоимость: {{ $product->price }}р</div>            

            <a href="#" class="btn btn--medium btn--dark b-product__add-backet">Добавить в корзину</a>
        </div>
    </div>

    @unless ($product->comments_disabled)
        <div class="b-comments">
            @foreach($product->comments as $comment)
                <div class="b-comments__item ">
                    <div class="b-comments__info clearfix">
                        <span class="b-comments__date">{{ $comment->created_at }}</span>

                        <span class="b-comments__author">{{ $comment->name }}</span>
                    </div>

                    <div class="b-comments__content">{{ $comment->content }}</div>
                </div>
            @endforeach
        </div>

        @include('frontend.partials.comment-form',  array('commentable_type' => 'product', 'commentable_id' => $product->id))
    @endunless
@stop