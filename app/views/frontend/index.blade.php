@extends('frontend.layouts.' . $page->layout->path)

@section('title', $page->title)
@section('meta_keywords', $page->keywords)
@section('meta_description', $page->description)

@section('content')
	<div class="b-slider"> @include("frontend.partials.slider")</div>

    <h1 class="page-heading">{{ Lang::get('blog.heading') }}</h1>

    {{ $posts_rendered }}
@stop