@extends('backend.layouts.main')

@section('content')
<section class="b-auth-form">
    <h1 class="heading--md">Войти в систему</h1>

    {{ Form::open(array('route' => 'backend.auth.login.post', 'class' => 'form')) }}
        
        <input type="password" name="password" placeholder="Пароль" class="form-input">
        <button type="submit" class="btn--success btn--md">Войти в систему</button>

    {{ Form::close() }}
    

</section>
@stop

@section('scripts')
@parent
<script>
$(document).ready(function() {

    $authForm = $('.b-auth-form form');
    $authForm.submit(function(e) {

        $.post($authForm.attr('action'), $authForm.serialize())
            .done(function(response) {

                if (response.redirect) {
                    document.location.href = response.redirect;
                }
      
            })
            .error(function(response) {

                var response = $.parseJSON(response.responseText);
                alert(response.error ? response.error : 'Неизвестная ошибка.');

            });
        e.preventDefault();

    });
});
</script>
@stop