<!doctype html>
<html lang="ru" ng-app="app">
<head>
    <meta charset="UTF-8">
    <title>P&amp;P 2014</title>

    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'> -->

    {{ HTML::style('backend/css/main.css') }}
</head>
<body ng-controller="MainCtrl">

@if (Session::get('backend.logged'))

    <!-- - - Flash - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --> 

    <flash:messages class="b-flash" ng-show="messages"></flash:messages>

    <!-- - - Header - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --> 

    <header class="b-header">
        <div class="b-header__top">
            <ul class="b-header__menu">
                <!--<li class="b-header__menu__item" ng-class="itemIsActive('/')"><a href="#/">Главная</a></li>-->

                <li class="b-header__menu__item" ng-class="itemIsActive('/pages')"><a href="#/pages">Страницы</a></li>

                <li class="b-header__menu__item" ng-class="itemIsActive('/blog')"><a href="#/blog">Блог</a></li>

                <li class="b-header__menu__item" ng-class="itemIsActive('/store')"><a href="#/store">Магазин</a></li>

                <li class="b-header__menu__item" ng-class="itemIsActive('/layouts')"><a href="#/layouts">Шаблоны</a></li>
                
                <li class="b-header__menu__item" ng-class="itemIsActive('/backups')"><a href="#/backups">Резервное копирование</a></li>

                <li class="b-header__menu__item"><a href="" file-manager>Файловый менеджер</a></li>

                <li class="b-header__menu__item" ng-class="itemIsActive('/settings')"><a href="#/settings">Настройки</a></li>
<!--             </ul>
        </div>

        <div class="b-header__bottom">
            <ul class="b-header__menu"> -->
                <li class="b-header__menu__item"><a href="{{ url('/') }}" target="_blank">Перейти на сайт</a></li>

                <li class="b-header__menu__item"><a href="{{ route('backend.auth.logout') }}">Выйти</a></li>
            </ul>
        </div>
    </header>
@endif

<!-- - - Content - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->    

<section class="b-content" ng-view> @yield('content') </section>

<!-- - - Footer - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<footer class="b-footer">
    <div class="b-footer__copy">&copy; P&amp;P 2014</div>

    <div class="b-footer__links">
        <a href="{{ url('/') }}" class="link" target="_blank">Перейти на сайт</a>
    </div>
</footer>

<!-- - - Scripts - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

@section('scripts') 

    {{ HTML::script('backend/js/vendor/jquery/jquery.min.js') }}
    {{ HTML::script('backend/js/vendor/pace/pace.min.js') }}

    <script>
    Pace.options.ajax.trackMethods = ['POST', 'GET'];
    Pace.options.restartOnRequestAfter = 200;
    </script>

    @if (Session::get('backend.logged'))

        {{ HTML::script('backend/js/vendor/jquery/jquery-ui.min.js') }}
        {{ HTML::script('backend/js/vendor/ckeditor/ckeditor.js') }}
        {{ HTML::script('backend/js/vendor/angular-core.min.js') }}
        {{ HTML::script('backend/js/vendor/angular-modules.min.js') }}
        {{ HTML::script('backend/js/app.min.js') }}

        <script>
        app.constant('config', {
            backendAssetsUrl: "{{ asset('backend'); }}",
            backendUrl: "{{ route('backend.index') }}",
            frontendUrl: "{{ route('frontend.index') }}",
            langs: {{ json_encode(Localization::getLangs()) }}
        });
        </script>

        {{ HTML::style('backend/js/vendor/jquery.colorpicker/colorPicker.css') }}
        {{ HTML::style('backend/js/vendor/jquery.fancybox/jquery.fancybox.css') }}

        {{ HTML::script('backend/js/vendor/jquery.pin/jquery.pin.js') }}
        {{ HTML::script('backend/js/vendor/jquery.fancybox/jquery.fancybox.pack.js') }}
        {{ HTML::script('backend/js/vendor/jquery.colorpicker/jquery.colorPicker.js') }}
        {{ HTML::script('backend/js/vendor/jquery.sortable/jquery.dragsort-0.5.1.min.js') }}

    @endif
@show
</body>
</html>