<?php
    $max_pages = 10;

    $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);

    $trans     = $environment->getTranslator();

    $items     = array_chunk(range(1, $paginator->getLastPage()), $max_pages);
?>

<ul class="b-pagination">
@foreach($items as $key => $item)

    @if(in_array($paginator->getCurrentPage(), $item))
        
        @if(isset($items[$key - 1]))
            <li class="b-pagination__item">
                <a href="{{ $paginator->getUrl(end($items[$key - 1])) }}">Предыдущие {{ $max_pages }}</a>
            </li>
        @endif

        @foreach($item as $page)
            <li class="b-pagination__item {{ $paginator->getCurrentPage() == $page ? 'active' : '' }}">
                <a href="{{ $paginator->getUrl($page) }}">{{ $page }}</a>
            </li>
        @endforeach

        @if(isset($items[$key + 1]))
            <li class="b-pagination__item">
                <a href="{{ $paginator->getUrl(reset($items[$key + 1])) }}">Следующие {{ $max_pages }}</a>
            </li>
        @endif

    @endif

@endforeach
</ul>