<?php
return array(
    'form' => array(
        'heading' => 'Добавить комментарий',
        'fields'  => array(

            'name'    => 'Имя',
            'email'   => 'E-mail (Не публикуется)',
            'content' => 'Комментарий'

        ),
        'button' => 'Отправить'
    )
);