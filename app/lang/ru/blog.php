<?php 

return array(

    'heading' => 'Блог',

    'posts' => array(

        'more'     => 'Подробнее', 
        'comments' => array(

            'count' => 'Комментарий :count'

        )

    ),

);