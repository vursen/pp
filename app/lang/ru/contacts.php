<?php 

return array(

    'heading'  => 'Контактная форма',

    'form' => array(
        'heading' => 'Контактная форма',
        'fields'  => array(

            'name'    => 'Имя',
            'email'   => 'E-mail',
            'content' => 'Содержимое',
            'file'    => 'Файл'

        ),
        'button' => 'Отправить',
        'success' => 'Спасибо за ваше сообщение!'
    )
);