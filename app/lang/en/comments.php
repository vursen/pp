<?php
return array(
    'form' => array(
        'heading' => 'Add comment',
        'fields'  => array(

            'name'    => 'Name',
            'email'   => 'E-mail',
            'content' => 'Comment'

        ),
        'button' => 'Send'
    )
);