<?php 

return array(

    'heading'  => 'Contact form',

    'form' => array(
        'heading' => 'Contact form',
        'fields'  => array(

            'name'    => 'Name',
            'email'   => 'E-mail',
            'content' => 'Content',
            'file'    => 'File'

        ),
        'button' => 'Send',
        'success' => 'Thank you for your message!'
    )
);