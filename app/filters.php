<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{

	/*$language = Request::segment(1);

    if(in_array($language, array_keys(Config::get('app.languages')))) {

        App::setLocale($language);
        //Session::put('language', $language);

    } elseif (!Session::has('language')) {

        Session::set('language', App::getLocale());

    } else {

        App::setLocale(Session::get('language'));

    }*/

});
/*$DBLog = DB::getQueryLog();

    if ($DBLog) {
        dd($DBLog);
    }*/

App::after(function($request, $response)
{
    
    return $response->withCookie(Cookie::make('_token', csrf_token(), 30));

});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('admin.auth', function()
{

	if (!Session::get('backend.logged')) {

        return Request::ajax() ? Response::json(array('error' => 'Ошибка доступа. Вы должны войти в систему.'), 401) : Redirect::route('backend.auth.login');

    }
    
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token') && Session::token() != Cookie::get('_token'))
    {
        return Request::ajax() ? Response::json(array('error' => 'Неверный токен.'), 500) : Response::make('Неверный токен.', 500);
    }
});