<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('LayoutsSeeder');
        $this->call('PagesSeeder');
        $this->call('BlogSeeder');
        $this->call('StoreSeeder');

	}

}

/* Магазин
------------------------------------------------------------ */

class StoreSeeder extends Seeder {

    public function run()
    {

        $faker = Faker\Factory::create('ru_RU');
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        StoreCategories::truncate();
        StoreProducts::truncate();

        for ($i = 0; $i < 5; $i++) {

            $category = StoreCategories::create(array(
                'name' => $faker->sentence(2),
                'lang' => 'ru'
            ));

            for ($j = 0; $j < 5; $j++) {

                StoreProducts::create(array(
                    'name'        => 'Продукт ' . $category->id . '-' . $j,
                    'category_id' => $category->id,
                    'content'     => $faker->text,
                    'price'       => $faker->randomFloat()
                ));

            }

        }
    }

}

/* Блог
------------------------------------------------------------ */

class BlogSeeder extends Seeder {

    public function run()
    {
        
        BlogCategories::truncate();
        BlogPosts::truncate();

        $faker = Faker\Factory::create('ru_RU');
        $faker->addProvider(new Faker\Provider\Base($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $categories[] = BlogCategories::create(array(
            'name'      => 'Видео',
            'lang'      => 'ru',
            'layout_id' => 1 
        ));

        $categories[] = BlogCategories::create(array(
            'name'      => 'Фото',
            'lang'      => 'ru',
            'layout_id' => 1 
        ));

        $categories[] = BlogCategories::create(array(
            'name'      => 'День за днем',
            'lang'      => 'ru',
            'layout_id' => 1 
        ));

        $categories[] = BlogCategories::create(array(
            'name'      => 'Дневники котов',
            'lang'      => 'ru',
            'layout_id' => 1 
        ));

        $categories[] = BlogCategories::create(array(
            'name'      => 'Календарь событий',
            'lang'      => 'ru',
            'layout_id' => 1 
        ));        

        for ($j = 0; $j < 10; $j++) {

            $category = $faker->randomElement($categories);

            BlogPosts::create(array(
                'title'         => 'Запись ' . $category->id . '-' . $j,
                'category_id'   => $category->id,
                'content'       => $faker->text,
                'preview_text'  => $faker->text
            ));

        }


    }

}

/* Страницы
------------------------------------------------------------ */

class PagesSeeder extends Seeder {

    public function run()
    {

        Pages::truncate();

        $indexPage = Pages::create(array(
            'title'        => 'Главная',
            'content'      => 'Главная страницы',
            'lang'         => 'ru',
            'layout_id'    => 1,
        ));

        Setting::set('pages.index.ru', $indexPage->id);

        Pages::create(array(
            'title'        => 'Верхнее меню',
            'content'      => 'меню',
            'lang'         => 'ru',
            'layout_id'    => 1 
        ));

        Pages::create(array(
            'title'        => 'Спонсоры',
            'content'      => 'Спонсоры',
            'lang'         => 'ru',
            'layout_id'    => 1 
        ));

        Pages::create(array(
            'title'        => 'Цитаты',
            'content'      => 'Цитаты',
            'lang'         => 'ru',
            'layout_id'    => 1 
        ));

    }

}

/* Шаблоны
------------------------------------------------------------ */

class LayoutsSeeder extends Seeder {

    public function run()
    {
        
        Layouts::truncate();

        $layout = Layouts::create(array(
            'name' => 'Основной (Русский)', 
            'path' => 'ru.default'
        ));

    }

}

