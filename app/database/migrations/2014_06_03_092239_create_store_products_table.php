<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name');
			$table->text('content');
			$table->text('images')->nullable();
			$table->text('slug')->nullable();
			$table->float('price')->nullable();

			$table->text('keywords')->nullable();
			$table->text('description')->nullable();

			$table->boolean('comments_disabled')->default(0);
			$table->integer('category_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_products');
	}

}
