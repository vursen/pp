<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_posts', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('slug')->nullable();
			$table->text('title')->nullable();
			$table->text('content')->nullable();
			$table->text('preview_img')->nullable();
			$table->text('preview_text')->nullable();

			$table->text('keywords')->nullable();
			$table->text('description')->nullable();

			$table->boolean('enabled')->default(1);
			$table->integer('category_id')->unsigned()->nullable();
			$table->boolean('comments_disabled')->default(0);

			$table->timestamp('added_on')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_posts');
	}

}
