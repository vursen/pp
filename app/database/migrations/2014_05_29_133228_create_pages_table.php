<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('slug')->nullable();
			$table->text('title')->nullable();
			$table->text('content')->nullable();
			$table->text('lang')->nullable();

			$table->text('keywords')->nullable();
			$table->text('description')->nullable();
			
			$table->integer('layout_id')->unsigned()->nullable();
			$table->boolean('enabled')->default(1);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
