<?php

class Tags extends BaseModel {

    protected $table = 'tags';

    public $timestamps = false;

    public $fillable = array('name');

    static $rules = array(
        'name' => 'required',
    );

    static $messages = array(
        'name.required' => 'Не заполнено поле "Название".',
    );

    public static $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );

    /**
     * Записи
     */

    public function posts()
    {

        return $this->morphedByMany('BlogPosts', 'taggable', 'taggables', 'taggable_id', 'tag_id');

    }

    /*public function posts()
    {

        return $this->belongsToMany('BlogPosts', 'blog_posts_tags', 'tag_id', 'post_id');

    }*/

    /**
     * Фильтр по slug
     */

    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }


}