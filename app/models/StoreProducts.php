<?php
class StoreProducts extends BaseModel {

    protected $table = 'store_products';

    public $appends = array('count_comments' );
    
    static $rules = array(
        'name'             => 'required|unique:store_products,name,:id',
        'content'          => 'required',
        'price'            => 'required|numeric',
        'category_id'      => 'required|exists:store_categories,id',
        'comments_disable' => 'numeric',
    );

    static $messages = array(
        'name.required'              => 'Не заполнено поле Название.',
        'name.unique'                => 'Продукт с таким названием уже существует.',
        'tags.required'              => 'Не заполнено поле Теги.',
        'content.required'           => 'Не заполнено поле Описание.',
        'category_id.required'       => 'Не указана категория продукта.',
        'category_id.exists'         => 'Категория не существует.',
        'comments_disabled.numeric'  => 'Не верно заполнено поле Отключить комментарии',
        'price.required'             => 'Не заполнено поле Стоимость.',
        'price.numeric'              => 'Не верно заполнено поле Стоимость.'
    );

    public static $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );

    /**
     * Категория
     */

    public function category() 
    {

        return $this->belongsTo('StoreCategories', 'category_id');

    }

    /**
     * Комментарии
     */

    public function comments() 
    {

        return $this->morphMany('Comments', 'commentable');

    }

    /**
     * Теги
     */

    public function tags()
    {

        return $this->morphToMany('Tags', 'taggable', 'taggables', 'tag_id', 'taggable_id');

    }

    /**
     * Фильтр по языку
     */

    public function scopeLang($query, $lang = null) 
    {

        return $query->whereExists(function($query) use ($lang)
        {
            $query->select(DB::raw(1))
                  ->from('store_categories')
                  ->whereRaw('store_products.category_id = store_categories.id')
                  ->where('store_categories.lang', isset($lang) ? $lang : App::getLocale());
        });

    }

    /**
     * Фильтр по slug
     */
    
    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }

    public function getCountCommentsAttribute() 
    {

        return $this->comments()->count();

    }

    public function getImagesAttribute() 
    {

        return isset($this->attributes['images']) ? json_decode($this->attributes['images']) : array();

    }

    public function setImagesAttribute($images)
    {

        $this->attributes['images'] = json_encode($images);

    }


}