<?php

class Pages extends BaseModel {

    protected $table = 'pages';

    protected $appends = array('is_index');

    /**
     * Правила для валидации
     * 
     * @var array
     */

    static $rules = array(
        'title'       => 'required|unique:pages,title,:id',
        'content'     => 'required',
        'layout_id'   => 'required|exists:layouts,id',
        'lang'        => 'required',
/*        'keywords'    => '',
        'description' => ''*/
    );

    /**
     * Сообщения об ошибках
     * 
     * @var array
     */

    static $messages = array(
        'title.required'     => 'Не заполнено поле "Название".',
        'title.unique'       => 'Страница с таким названием уже существует.',
        'content.required'   => 'Не заполнено поле "Контент".',
        'lang.required'      => 'Не указан язык.',
        'layout_id.required' => 'Не указан шаблон.',
        'layout_id.exists'   => 'Шаблон не существует.'
    );

    public static $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    /**
     * Шаблон
     */

    public function layout() 
    {

        return $this->belongsTo('Layouts');

    }

    /**
     * Фильтр по языку
     */

    public function scopeLang($query, $lang = null) 
    {

        return $lang ? $query->where('lang', $lang) : $query->where('lang', App::getLocale());

    }

    /**
     * Фильтр по заголовку
     */

    public function scopeTitle($query, $title) 
    {

        return $query->where('title', $title);

    }

    /**
     * Фильтр по slug
     */

    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }

    public function getIsIndexAttribute() {

        return Setting::get('pages.index.' . $this->attributes['lang']) == $this->attributes['id'];

    }

    /**
     * Рендеринг контента
     */
    
    /*public function getRenderedAttribute() 
    {

        $content = $this->attributes['content'];

        $content = preg_replace_callback('/@page\((.+)\)/s', function($page) {

            $page = Page::title($page[1])->first();

            return $page ? $page->rendered : '';

        }, $content);

        $content = preg_replace_callback('/@contact_form/', function($page) {

            return (string)View::make('frontend.contacts.form');

        }, $content);

        return $content;

    }*/
}