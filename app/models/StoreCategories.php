<?php

class StoreCategories extends BaseModel {

    protected $table = 'store_categories';

    public $timestamps = false;

    public $appends = array('count_products');

    static $rules = array(
        'name' => 'required|unique:store_categories,name,:id',
        'lang' => 'required',
    );

    static $messages = array(
        'name.required' => 'Не заполнено поле Название.',
        'name.unique'   => 'Категория с таким названием уже существует.',
        'lang.required' => 'Не указан язык категории.',
    );

    public static $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );

    /**
     * Продукты
     */
    
    public function products()
    {

        return $this->hasMany('StoreProducts', 'category_id');

    }

    /**
     * Фильтр по языку
     */

    public function scopeLang($query, $lang = null) 
    {

        return $lang ? $query->where('lang', $lang) : $query->where('lang', App::getLocale());

    }

    /**
     * Фильтр по slug
     */

    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }

    /**
     * Фильтр по названию
     */

    public function scopeName($query, $name) 
    {

        return $query->where('name', $name);

    }

    /**
     * Кол-во продуктов
     */

    public function getCountProductsAttribute() 
    {

        return $this->products()->count();

    }

}