<?php

class BlogCategories extends BaseModel {

    protected $table = 'blog_categories';

    public $appends = array('count_posts');

    public $timestamps = false;

    static $rules = array(
        'name'      => 'required|unique:blog_categories,name,:id',
        'lang'      => 'required',
        'layout_id' => 'required|exists:layouts,id'
    );

    static $messages = array(
        'name.required'      => 'Не заполнено поле Название.',
        'name.unique'        => 'Категория с таким названием уже существует.',
        'lang.required'      => 'Не указан язык категории.',
        'layout_id.required' => 'Не указан шаблон.',
        'layout_id.exists'   => 'Шаблон не существует.'
    );

    public static $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );

    /**
     * Записи
     */
    
    public function posts()
    {

        return $this->hasMany('BlogPosts', 'category_id');

    }

    /**
     * Шаблон
     */

    public function layout()
    {

        return $this->belongsTo('Layouts', 'layout_id');

    }

    /**
     * Фильтр по языку
     */

    public function scopeLang($query, $lang = null) 
    {

        return $lang ? $query->where('lang', $lang) : $query->where('lang', App::getLocale());

    }

    /**
     * Фильтр по slug
     */

    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }

    /**
     * Фильтр по названию
     */

    public function scopeName($query, $name) 
    {

        return $query->where('name', $name);

    }

    /**
     * Кол-во записей
     */

    public function getCountPostsAttribute() 
    {

        return $this->posts()->count();

    }

}