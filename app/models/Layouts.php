<?php

class Layouts extends BaseModel {

    protected $table = 'layouts';

    /**
     * Правила валидации
     * 
     * @var array
     */

    static $rules = array(
        'name' => 'required|unique:layouts,name,:id',
        'path' => 'required|unique:layouts,path,:id'
    );

    /**
     * Сообщения об ошибках валидации
     * 
     * @var array
     */

    static $messages = array(
        'name.required' => 'Не заполнено поле "Название".',
        'name.unique'   => 'Шаблон с таким названием уже существует.',
        'path.required' => 'Не заполнено поле "Путь к шаблону".',
        'path.unique'   => 'Шаблон с таким путем уже существует.',
    );

    /**
     * Страницы
     */

    public function pages() 
    {

        return $this->hasMany('Pages');

    }

}