<?php
class BaseModel extends Eloquent {

    /**
    * Listen for save event
    *
    * @return void
    */

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {

            return $model->validate();

        });

        static::updating(function($model)
        {

            return $model->validate();

        });
    }

    /**
    * Validates current attributes against rules
    *
    * @return boolean
    */

    public function validate()
    {

        $rules = property_exists($this, 'rules') ? static::$rules : array();
        $messages = property_exists($this, 'messages') ? static::$messages : array();

        if (!empty($rules)) {

            $replace = ($this->getKey() > 0) ? $this->getKey() : null;

            foreach ($rules as $key => $rule) {
                $rules[$key] = str_replace(':id', $replace, $rule);
            }

            $validation = Validator::make($this->attributes, $rules, $messages);
            if ($validation->fails()) {

                $this->errors = $validation->errors();

                return false;

            }
        }


        return true;
    }

    /**
     * Рендеринг контента
     */
    
    public function getRenderedAttribute() 
    {
        if (!isset($this->attributes['content'])) {
            return false;
        }

        $content = $this->attributes['content'];

        $content = preg_replace_callback('/@page\((.+)\)/s', function($page) {

            $page = Pages::title($page[1])->first();

            return $page ? $page->rendered : '';

        }, $content);

        $content = preg_replace_callback('/@contact_form/', function($page) {

            return (string)View::make('frontend.partials.contact-form');

        }, $content);

        return $content;

    }

}