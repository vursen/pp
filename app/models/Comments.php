<?php

class Comments extends BaseModel {

    protected $table = 'comments';

    /**
     * Запись
     */

    public function commentable()
    {

        return $this->morphTo();

    }

    /**
     * Сортировка по дате
     */

    public function scopeSortByDate($query) {

        return $query->orderBy('created_at', 'DESC');

    }

}