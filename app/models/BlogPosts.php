<?php
use Carbon\Carbon;

class BlogPosts extends BaseModel {

    protected $table = 'blog_posts';

    protected $appends = array('added_on');
    
    protected $fillable = array(); 

    public $timestamps = false;

    static $rules = array(
        'title'            => 'required|unique:blog_posts,title,:id',
        'content'          => 'required',
        'preview_text'     => 'required',
        'category_id'      => 'required|exists:blog_categories,id',
        'comments_disable' => 'numeric',
        'added_on'         => ''
    );

    static $messages = array(
        'title.required'        => 'Не заполнено поле "Название".',
        'title.unique'          => 'Запись с таким названием уже существует.',
        'tags.required'         => 'Не заполнено поле "Теги".',
        'content.required'      => 'Не заполнено поле "Контент".',
        'preview_text.required' => 'Не заполнено поле "Анонс".',
        'category_id.required'  => 'Не указана категория записи.',
        'category_id.exists'    => 'Категория не существует.',
        'comments_disabled'     => 'Не верно заполнено поле "Отключить комментарии"',
        'added_on.required'     => 'Не заполнено поле "Дата".',
        'added_on.regex'        => 'Не верно заполнено поле "Дата".'
    );

    public static $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    /**
     * Категория
     */

    public function category() 
    {

        return $this->belongsTo('BlogCategories', 'category_id');

    }

    /**
     * Комментарии
     */

    public function comments() 
    {

        return $this->morphMany('Comments', 'commentable');

    }

    /**
     * Теги
     */

    public function tags()
    {

        return $this->morphToMany('Tags', 'taggable', 'taggables', 'tag_id', 'taggable_id');

    }

    /**
     * Фильтр по языку
     */

    public function scopeLang($query, $lang = null) 
    {

        return $query->whereExists(function($query) use ($lang)
        {
            $query->select(DB::raw(1))
                  ->from('blog_categories')
                  ->whereRaw('blog_posts.category_id = blog_categories.id')
                  ->where('blog_categories.lang', isset($lang) ? $lang : App::getLocale());
        });

    }

    /**
     * Фильтр по slug
     */
    
    public function scopeSlug($query, $slug) 
    {

        return $query->where('slug', $slug);

    }

    /**
     * Сортировка по дате
     */

    public function scopeSortByDate($query, $reverse = false) 
    {

        return $query->orderBy('added_on', ($reverse ? 'ABC' : 'DESC'));

    }

    public function getAddedOnAttribute()
    {
        return with(new Carbon($this->attributes['added_on']))->format('d.m.y');

    }


/*
    public function getShortContentAttribute() 
    {

        if (preg_match('/^(.+)###more###/s', $this->attributes['content'], $m)) {

            return $m[1];

        }
        return $this->attributes['content'];

    }*/

    /*public function getContentAttribute() 
    {

        //return str_replace('###more###', '', $this->attributes['content']);

    }*/

}