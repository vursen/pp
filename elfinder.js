function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;

    return (match && match.length > 1) ? match[1] : '' ;
}

// Documentation for client options:
// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
$().ready(function() {
    var funcNum = getUrlParam('CKEditorFuncNum');

    $('#elfinder').elfinder({
        url : '<?= URL::action('Barryvdh\Elfinder\ElfinderController@showConnector') ?>',  // connector URL (REQUIRED)
        getFileCallback: function(file) { // editor callback
            
            if (window.fileCallback) {
                window.fileCallback(file);
                window.close();
            }

            if (funcNum) {
                window.opener.CKEDITOR.tools.callFunction(funcNum, file.url);
                window.close();
            }
            
        }
    });
});